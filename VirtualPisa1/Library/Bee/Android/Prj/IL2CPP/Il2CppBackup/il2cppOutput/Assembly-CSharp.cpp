﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<TMPro.TMP_TextInfo>
struct Action_1_tB93AB717F9D419A1BEC832FF76E74EAA32184CC1;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180;
// System.Func`3<System.Int32,System.String,TMPro.TMP_FontAsset>
struct Func_3_tC721DF8CDD07ED66A4833A19A2ED2302608C906C;
// System.Func`3<System.Int32,System.String,TMPro.TMP_SpriteAsset>
struct Func_3_t6F6D9932638EA1A5A45303C6626C818C25D164E5;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t89B39292AD45371F7FDCB295AAE956D33588BC6E;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t830EC096236A3CEC7189DFA6E0B2E74C5C97780B;
// TMPro.TMP_TextProcessingStack`1<System.Int32>[]
struct TMP_TextProcessingStack_1U5BU5D_t08293E0BB072311BB96170F351D1083BCA97B9B2;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.Color32[]
struct Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259;
// System.Decimal[]
struct DecimalU5BU5D_t93BA0C88FA80728F73B792EE1A5199D0C060B615;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t2A406B5BAB0DD0F06E7F1773DB062E4AF98067BA;
// TMPro.HighlightState[]
struct HighlightStateU5BU5D_tA878A0AF1F4F52882ACD29515AADC277EE135622;
// TMPro.HorizontalAlignmentOptions[]
struct HorizontalAlignmentOptionsU5BU5D_t4D185662282BFB910D8B9A8199E91578E9422658;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.Material[]
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t7491D335AB3E3E13CE9C0F5E931F396F6A02E1F2;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_t5816316EFD8F59DBC30B9F88E15828C564E47B6D;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t297D56FCF66DAA99D8FEA7C30F9F3926902C5B99;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2F65E8C42F268DFF33BB1392D94BCF5B5087308A;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// System.UInt32[]
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// TMPro.WordWrapState[]
struct WordWrapStateU5BU5D_t473D59C9DBCC949CE72EF1EB471CBA152A6CEAC9;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_t67F27D09F8EB28D2C42DFF16FE60054F157012F5;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074;
// UnityEngine.UI.Button
struct Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// CubeRotate
struct CubeRotate_t28274E131964C53FEEBDE5D076CFAFF181D3C45F;
// UnityEngine.UI.Dropdown
struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89;
// UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// Growth_Menu
struct Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// TMPro.ITextPreprocessor
struct ITextPreprocessor_tDBB49C8B68D7B80E8D233B9D9666C43981EFAAB9;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.UI.InputField
struct InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// System.String
struct String_t;
// TMPro.TMP_Character
struct TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2E0F016A61CA343E3222FF51E7CF0E53F9F256E4;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39;
// TMPro.TMP_Style
struct TMP_Style_tA9E5B1B35EBFE24EF980CEA03251B638282E120C;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t70C71699F5CB2D855C361DBB78A44C901236C859;
// TMPro.TMP_Text
struct TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9;
// TMPro.TMP_TextElement
struct TMP_TextElement_t262A55214F712D4274485ABE5676E5254B84D0A5;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D;
// Task_Door_manager
struct Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.UI.Toggle
struct Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_tF2E6FE7D4B17BDBF82462715CFB57C4FDE0A2A2C;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_tA8CE0AAB4B0C872B843E7973637037D17682BA01;
// moduli
struct moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955;
// perehodMain
struct perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088;
// perehodmodul1
struct perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D;
// perehodmodul2
struct perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA;
// perehodmodul4
struct perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA;
// task1
struct task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F;
// task10
struct task10_tDE5028C32B8C330404B0B6888C2780FAC891472F;
// task11
struct task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4;
// task12
struct task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330;
// task15
struct task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA;
// task16
struct task16_t56A0437BE43073063DF2885A7A931FFF46DD8331;
// task17
struct task17_t31BD90159BED97C8AF144D4987B7CD17FB655180;
// task18
struct task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB;
// task19
struct task19_t98E5806FE743E604F715DFA8F534387E08B569CC;
// task2
struct task2_t2C5D69F0A329B1B40F552703669EA12C355019C4;
// task20
struct task20_t3D21D377B9C69943268EEB49715FC68A77487D1C;
// task21
struct task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3;
// task23
struct task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42;
// task24
struct task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C;
// task4
struct task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C;
// task5
struct task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023;
// task6
struct task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED;
// task7
struct task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67;
// task8
struct task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061;
// task9
struct task9_tC73010C90655823D17F5BABBF0D4C0751EF88867;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t8EA72E90B3BD1392FB3B3EF167D5121C23569E4C;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t53255477D0A9C6980AB48693A520EFBC94DFFB96;
// Growth_Menu/<stopwatch>d__89
struct U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381;
// UnityEngine.UI.InputField/EndEditEvent
struct EndEditEvent_t946A962BA13CF60BB0BE7AD091DA041FD788E655;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_tE4829F88300B0E0E0D1B78B453AF25FC1AA55E2F;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t48916A4E9C9FD6204401FF0808C2B7A93D73418B;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t1E0F5A2AB28D0DB55AE18E8DA99147D86492DD5D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;
// Task_Door_manager/<VisiblePeople>d__29
struct U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404;
// Task_Door_manager/<VisibleRotate>d__31
struct U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t88B31268F9D6D1882E4F921B14704FB9F7047F02;
// task1/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721;
// task10/<stopwatch>d__30
struct U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0;
// task11/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE;
// task12/<stopwatch>d__29
struct U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484;
// task15/<stopwatch>d__27
struct U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5;
// task16/<stopwatch>d__36
struct U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114;
// task17/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E;
// task18/<stopwatch>d__18
struct U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C;
// task19/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB;
// task2/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6;
// task20/<stopwatch>d__22
struct U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4;
// task21/<stopwatch>d__18
struct U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377;
// task23/<VisiblePeople>d__38
struct U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761;
// task23/<VisibleRotate>d__29
struct U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1;
// task23/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4;
// task24/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A;
// task4/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581;
// task5/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170;
// task6/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947;
// task7/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA;
// task8/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038;
// task9/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9;

IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719;
IL2CPP_EXTERN_C String_t* _stringLiteral04E9EBE1F00A77E549A2B09F449656118F2B3197;
IL2CPP_EXTERN_C String_t* _stringLiteral05232D54BF656F639590A6B59985605128B3D584;
IL2CPP_EXTERN_C String_t* _stringLiteral074DECB0B6A85A8FE24AB2C92BF43E49056C81C8;
IL2CPP_EXTERN_C String_t* _stringLiteral1D953F5E34759B5103309A24C047B28F03C5DA76;
IL2CPP_EXTERN_C String_t* _stringLiteral1D98514DA44F57EE99E5E0779EC025797FC2563D;
IL2CPP_EXTERN_C String_t* _stringLiteral1DC9F6BC39F00E092B0BEEEEE189243DA24E1C20;
IL2CPP_EXTERN_C String_t* _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE;
IL2CPP_EXTERN_C String_t* _stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40;
IL2CPP_EXTERN_C String_t* _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2;
IL2CPP_EXTERN_C String_t* _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9;
IL2CPP_EXTERN_C String_t* _stringLiteral2B0579ACFE6DC9DC857FCADED1DDADDC6CBBB23D;
IL2CPP_EXTERN_C String_t* _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD;
IL2CPP_EXTERN_C String_t* _stringLiteral394FD3AEF9694249DD5036EC8A636CCAC0BBEFB4;
IL2CPP_EXTERN_C String_t* _stringLiteral3960B836F80A1121745A7FF318E16CA0377383D6;
IL2CPP_EXTERN_C String_t* _stringLiteral3BADA17CF62AE1A9C33E0086BFF271B2563C57B9;
IL2CPP_EXTERN_C String_t* _stringLiteral3F82580E4FA37F2BAB42B1DFE3D0FFF5E289B93C;
IL2CPP_EXTERN_C String_t* _stringLiteral412AC43995D234B24609E29CBD8D8CFA87AA8D0A;
IL2CPP_EXTERN_C String_t* _stringLiteral4C3E1FD83C61F9FB95D4043B9CFE9517650E0CC4;
IL2CPP_EXTERN_C String_t* _stringLiteral521FB55423CC068B92B5E8453A36DF0D6CBDD93F;
IL2CPP_EXTERN_C String_t* _stringLiteral532DCA297F5B95A1298F57EEF603E768B19F3427;
IL2CPP_EXTERN_C String_t* _stringLiteral5479C1C5ECE0533E4A55EE2AF88A08A9E7D1DC56;
IL2CPP_EXTERN_C String_t* _stringLiteral556DDA907570BAA94FD8305D5E082166623C2DE7;
IL2CPP_EXTERN_C String_t* _stringLiteral57C4A3908A7E58343F767097E3A76C282D0888F9;
IL2CPP_EXTERN_C String_t* _stringLiteral68ABB36301BCDDA4EEB3BF7DCA2DFD108BCE35FB;
IL2CPP_EXTERN_C String_t* _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6;
IL2CPP_EXTERN_C String_t* _stringLiteral6F09A47709D08F77E21625A3482A3A0A98579DE1;
IL2CPP_EXTERN_C String_t* _stringLiteral700C1596CA301B3AA61533700CEED311DC1053BC;
IL2CPP_EXTERN_C String_t* _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E;
IL2CPP_EXTERN_C String_t* _stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9;
IL2CPP_EXTERN_C String_t* _stringLiteral74F373A07613B0985D213534426C8F7DCE864244;
IL2CPP_EXTERN_C String_t* _stringLiteral75388A0FCDF389F8530BFB9F20E55AB8AD7F95F9;
IL2CPP_EXTERN_C String_t* _stringLiteral7643D699232FD7DBE557F071B1A3A37AEDD1E2DE;
IL2CPP_EXTERN_C String_t* _stringLiteral83A08B95DA45FB82D73D93620C3D06753D73B05E;
IL2CPP_EXTERN_C String_t* _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D;
IL2CPP_EXTERN_C String_t* _stringLiteral8F0E69BF00A53CC4161A360099669FB996FD6447;
IL2CPP_EXTERN_C String_t* _stringLiteral908087AF1B00E5A6097DB84E1CA2DD8DDD7EE064;
IL2CPP_EXTERN_C String_t* _stringLiteral92F8F26D4A784498D26E165EAA2AF96F6ED97F5E;
IL2CPP_EXTERN_C String_t* _stringLiteral93DD1EB39AAB8608AB51651588484F9FDBA9932F;
IL2CPP_EXTERN_C String_t* _stringLiteral94AA14A52763F3CD8FFB480C9EF5EBD529D39C24;
IL2CPP_EXTERN_C String_t* _stringLiteralA16D90C9F2FF27AD42EBD31D0D6A2DFEF1664E81;
IL2CPP_EXTERN_C String_t* _stringLiteralA6A6AB007C12E7673D8479299FDC94A4D003EBF9;
IL2CPP_EXTERN_C String_t* _stringLiteralA941ABAA66D5C286D010F3318C47E9E1C1EF5EB9;
IL2CPP_EXTERN_C String_t* _stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79;
IL2CPP_EXTERN_C String_t* _stringLiteralAF684003298C71D4D13496BF2119604A329CDC4B;
IL2CPP_EXTERN_C String_t* _stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE;
IL2CPP_EXTERN_C String_t* _stringLiteralB4E68654B7D1C85BED3057BC5DB05DF42FB53BC5;
IL2CPP_EXTERN_C String_t* _stringLiteralBA6489F9FF78EA53C6B7657F31F26E6124C0AC1D;
IL2CPP_EXTERN_C String_t* _stringLiteralC12E4A2478C8542173734DE349D50AF90C34898B;
IL2CPP_EXTERN_C String_t* _stringLiteralC44C020D752222A424F7F9E300FD9D237C95A6CE;
IL2CPP_EXTERN_C String_t* _stringLiteralC46E3DE1D683271C065DF2DFD5E2E87DA73D086E;
IL2CPP_EXTERN_C String_t* _stringLiteralD4E05ABC1E12488C6A70B26CAD1AEFE636D14B23;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE18044B8B947F5B124682EBE52BE2E13C5E422D8;
IL2CPP_EXTERN_C String_t* _stringLiteralE3D246D5CC5B87BFCC0C03F310D4D97A8C3C477B;
IL2CPP_EXTERN_C String_t* _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3;
IL2CPP_EXTERN_C String_t* _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141;
IL2CPP_EXTERN_C String_t* _stringLiteralF34425C641201FE6B55C8FCBE7A668330A6033AC;
IL2CPP_EXTERN_C String_t* _stringLiteralF41DDB4D64CC42FB36F2B7E0929F808D5BC1C499;
IL2CPP_EXTERN_C String_t* _stringLiteralF63DF0A24E87ED583133D4C27862F85ABB36A8F5;
IL2CPP_EXTERN_C String_t* _stringLiteralF77DC4857B149AEE7F1D08A173014BCAED8EA20F;
IL2CPP_EXTERN_C String_t* _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE;
IL2CPP_EXTERN_C String_t* _stringLiteralF859CAA74FC4BD45E85F9AFF276F614AE47D3DA2;
IL2CPP_EXTERN_C String_t* _stringLiteralF85C8F48FAFA3666F9F535246EC370456D1976BC;
IL2CPP_EXTERN_C const RuntimeMethod* U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_Reset_mF3D061165A076653B3044E1EEC3FE34959CDBBF8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_Reset_m4246B29506244980BE7A9236CCDF52291E841A50_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_Reset_m7CBDF83FC94C799459737385FF0C2E1CFFC5876D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_Reset_m3DEA0136564A7FBE1C91EF8659AD71DEA872B57C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m0282CAB192F651508DDBBFFA11D8BD5B9B2FCBA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m4A765E673B465AB278707B6FF492C7595152324E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__22_System_Collections_IEnumerator_Reset_m3ECCA74D5197F50D38F45FEB9DF44710AA48C65D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m3DD586B5B64D48A3E26DE70E99D865E74BC6B68E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m7687E13C63EB80EA50D717667B6A601259F2C817_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m87A3860173B5A6BF3C96B25BF9FD502FBC02953F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_mBC582C5957C3035D3103ECE4C8A6EA16D5FDBB30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m2E2D4AA52F089E0BD63A0C14067D282CAB53530B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m81FC0300E7EA90226FEF616DB832873969C16A32_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m896AFD90834C83785294BC43E7A161D3966D3DC6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m93082B657640111769C616B95E39BA4DEB00CA36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_mBCC6462A07E23707896D3C34E923E1ED21704053_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__27_System_Collections_IEnumerator_Reset_mBA30D813137118067E8CCF6E4B421B60764FAB4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m0EABDE0C4FBC3676A84E8E420FF9CBE70274ED20_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m740F6D1D83CC0F796D56F666CA6391FE4AE06534_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m96D72C34051F787294468EB7E08BA0C90BE22675_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mF94B5912F0DE9CE7169D2D881F14DE8DAB775B8D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__29_System_Collections_IEnumerator_Reset_m1E65CC4AC36D6F6B3E4A565402C9059B28CFCC56_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__30_System_Collections_IEnumerator_Reset_m5E7FA5648CD08E9E609369D53888336719995879_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__36_System_Collections_IEnumerator_Reset_m7DD2F20B5E3227FC44D10C623A4A8AD250F13DD4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CstopwatchU3Ed__89_System_Collections_IEnumerator_Reset_m7021A424310BDF3F5810B457A25C99FCC22AC91C_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};
struct Il2CppArrayBounds;

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// Growth_Menu/<stopwatch>d__89
struct U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381  : public RuntimeObject
{
	// System.Int32 Growth_Menu/<stopwatch>d__89::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Growth_Menu/<stopwatch>d__89::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Growth_Menu Growth_Menu/<stopwatch>d__89::<>4__this
	Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* ___U3CU3E4__this_2;
};

// Task_Door_manager/<VisiblePeople>d__29
struct U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404  : public RuntimeObject
{
	// System.Int32 Task_Door_manager/<VisiblePeople>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Task_Door_manager/<VisiblePeople>d__29::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Task_Door_manager Task_Door_manager/<VisiblePeople>d__29::<>4__this
	Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* ___U3CU3E4__this_2;
};

// Task_Door_manager/<VisibleRotate>d__31
struct U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7  : public RuntimeObject
{
	// System.Int32 Task_Door_manager/<VisibleRotate>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Task_Door_manager/<VisibleRotate>d__31::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Task_Door_manager Task_Door_manager/<VisibleRotate>d__31::<>4__this
	Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* ___U3CU3E4__this_2;
	// System.Int32 Task_Door_manager/<VisibleRotate>d__31::<i>5__2
	int32_t ___U3CiU3E5__2_3;
};

// task1/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721  : public RuntimeObject
{
	// System.Int32 task1/<stopwatch>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task1/<stopwatch>d__25::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task1 task1/<stopwatch>d__25::<>4__this
	task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* ___U3CU3E4__this_2;
};

// task10/<stopwatch>d__30
struct U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0  : public RuntimeObject
{
	// System.Int32 task10/<stopwatch>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task10/<stopwatch>d__30::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task10 task10/<stopwatch>d__30::<>4__this
	task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* ___U3CU3E4__this_2;
};

// task11/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE  : public RuntimeObject
{
	// System.Int32 task11/<stopwatch>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task11/<stopwatch>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task11 task11/<stopwatch>d__28::<>4__this
	task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* ___U3CU3E4__this_2;
};

// task12/<stopwatch>d__29
struct U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484  : public RuntimeObject
{
	// System.Int32 task12/<stopwatch>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task12/<stopwatch>d__29::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task12 task12/<stopwatch>d__29::<>4__this
	task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* ___U3CU3E4__this_2;
};

// task15/<stopwatch>d__27
struct U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5  : public RuntimeObject
{
	// System.Int32 task15/<stopwatch>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task15/<stopwatch>d__27::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task15 task15/<stopwatch>d__27::<>4__this
	task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* ___U3CU3E4__this_2;
};

// task16/<stopwatch>d__36
struct U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114  : public RuntimeObject
{
	// System.Int32 task16/<stopwatch>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task16/<stopwatch>d__36::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task16 task16/<stopwatch>d__36::<>4__this
	task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* ___U3CU3E4__this_2;
};

// task17/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E  : public RuntimeObject
{
	// System.Int32 task17/<stopwatch>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task17/<stopwatch>d__25::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task17 task17/<stopwatch>d__25::<>4__this
	task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* ___U3CU3E4__this_2;
};

// task18/<stopwatch>d__18
struct U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C  : public RuntimeObject
{
	// System.Int32 task18/<stopwatch>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task18/<stopwatch>d__18::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task18 task18/<stopwatch>d__18::<>4__this
	task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* ___U3CU3E4__this_2;
};

// task19/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB  : public RuntimeObject
{
	// System.Int32 task19/<stopwatch>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task19/<stopwatch>d__25::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task19 task19/<stopwatch>d__25::<>4__this
	task19_t98E5806FE743E604F715DFA8F534387E08B569CC* ___U3CU3E4__this_2;
};

// task2/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6  : public RuntimeObject
{
	// System.Int32 task2/<stopwatch>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task2/<stopwatch>d__23::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task2 task2/<stopwatch>d__23::<>4__this
	task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* ___U3CU3E4__this_2;
};

// task20/<stopwatch>d__22
struct U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4  : public RuntimeObject
{
	// System.Int32 task20/<stopwatch>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task20/<stopwatch>d__22::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task20 task20/<stopwatch>d__22::<>4__this
	task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* ___U3CU3E4__this_2;
};

// task21/<stopwatch>d__18
struct U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377  : public RuntimeObject
{
	// System.Int32 task21/<stopwatch>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task21/<stopwatch>d__18::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task21 task21/<stopwatch>d__18::<>4__this
	task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* ___U3CU3E4__this_2;
};

// task23/<VisiblePeople>d__38
struct U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761  : public RuntimeObject
{
	// System.Int32 task23/<VisiblePeople>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task23/<VisiblePeople>d__38::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task23 task23/<VisiblePeople>d__38::<>4__this
	task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* ___U3CU3E4__this_2;
};

// task23/<VisibleRotate>d__29
struct U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1  : public RuntimeObject
{
	// System.Int32 task23/<VisibleRotate>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task23/<VisibleRotate>d__29::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task23 task23/<VisibleRotate>d__29::<>4__this
	task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* ___U3CU3E4__this_2;
	// System.Int32 task23/<VisibleRotate>d__29::<i>5__2
	int32_t ___U3CiU3E5__2_3;
};

// task23/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4  : public RuntimeObject
{
	// System.Int32 task23/<stopwatch>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task23/<stopwatch>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task23 task23/<stopwatch>d__28::<>4__this
	task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* ___U3CU3E4__this_2;
};

// task24/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A  : public RuntimeObject
{
	// System.Int32 task24/<stopwatch>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task24/<stopwatch>d__23::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task24 task24/<stopwatch>d__23::<>4__this
	task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* ___U3CU3E4__this_2;
};

// task4/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581  : public RuntimeObject
{
	// System.Int32 task4/<stopwatch>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task4/<stopwatch>d__25::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task4 task4/<stopwatch>d__25::<>4__this
	task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* ___U3CU3E4__this_2;
};

// task5/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170  : public RuntimeObject
{
	// System.Int32 task5/<stopwatch>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task5/<stopwatch>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task5 task5/<stopwatch>d__28::<>4__this
	task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* ___U3CU3E4__this_2;
};

// task6/<stopwatch>d__25
struct U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947  : public RuntimeObject
{
	// System.Int32 task6/<stopwatch>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task6/<stopwatch>d__25::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task6 task6/<stopwatch>d__25::<>4__this
	task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* ___U3CU3E4__this_2;
};

// task7/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA  : public RuntimeObject
{
	// System.Int32 task7/<stopwatch>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task7/<stopwatch>d__23::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task7 task7/<stopwatch>d__23::<>4__this
	task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* ___U3CU3E4__this_2;
};

// task8/<stopwatch>d__23
struct U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038  : public RuntimeObject
{
	// System.Int32 task8/<stopwatch>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task8/<stopwatch>d__23::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task8 task8/<stopwatch>d__23::<>4__this
	task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* ___U3CU3E4__this_2;
};

// task9/<stopwatch>d__28
struct U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9  : public RuntimeObject
{
	// System.Int32 task9/<stopwatch>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object task9/<stopwatch>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// task9 task9/<stopwatch>d__28::<>4__this
	task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* ___U3CU3E4__this_2;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight>
struct TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	FontWeightU5BU5D_t2A406B5BAB0DD0F06E7F1773DB062E4AF98067BA* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions>
struct TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	HorizontalAlignmentOptionsU5BU5D_t4D185662282BFB910D8B9A8199E91578E9422658* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<System.Int32>
struct TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<System.Single>
struct TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	float ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient>
struct TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2F65E8C42F268DFF33BB1392D94BCF5B5087308A* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB* ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	// System.UInt64 System.DateTime::_dateData
	uint64_t ____dateData_46;
};

struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_StaticFields
{
	// System.Int32[] System.DateTime::s_daysToMonth365
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth365_30;
	// System.Int32[] System.DateTime::s_daysToMonth366
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth366_31;
	// System.DateTime System.DateTime::MinValue
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MinValue_32;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MaxValue_33;
	// System.DateTime System.DateTime::UnixEpoch
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___UnixEpoch_34;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// TMPro.MaterialReference
struct MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B 
{
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;
};
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___spriteAsset_2;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___spriteAsset_2;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C 
{
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD 
{
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_pinvoke
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_com
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};

// TMPro.TMP_FontStyleStack
struct TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC 
{
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;
};

// TMPro.TMP_Offset
struct TMP_Offset_t2262BE4E87D9662487777FF8FFE1B17B0E4438C6 
{
	// System.Single TMPro.TMP_Offset::m_Left
	float ___m_Left_0;
	// System.Single TMPro.TMP_Offset::m_Right
	float ___m_Right_1;
	// System.Single TMPro.TMP_Offset::m_Top
	float ___m_Top_2;
	// System.Single TMPro.TMP_Offset::m_Bottom
	float ___m_Bottom_3;
};

struct TMP_Offset_t2262BE4E87D9662487777FF8FFE1B17B0E4438C6_StaticFields
{
	// TMPro.TMP_Offset TMPro.TMP_Offset::k_ZeroOffset
	TMP_Offset_t2262BE4E87D9662487777FF8FFE1B17B0E4438C6 ___k_ZeroOffset_4;
};

// System.TimeSpan
struct TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A 
{
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;
};

struct TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A_StaticFields
{
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___MinValue_21;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	float ___m_Seconds_0;
};

// TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777 
{
	// TMPro.TMP_Character TMPro.TMP_Text/SpecialCharacter::character
	TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35* ___character_0;
	// TMPro.TMP_FontAsset TMPro.TMP_Text/SpecialCharacter::fontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	// UnityEngine.Material TMPro.TMP_Text/SpecialCharacter::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	// System.Int32 TMPro.TMP_Text/SpecialCharacter::materialIndex
	int32_t ___materialIndex_3;
};
// Native definition for P/Invoke marshalling of TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777_marshaled_pinvoke
{
	TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35* ___character_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	int32_t ___materialIndex_3;
};
// Native definition for COM marshalling of TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777_marshaled_com
{
	TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35* ___character_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	int32_t ___materialIndex_3;
};

// TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361 
{
	// System.UInt32[] TMPro.TMP_Text/TextBackingContainer::m_Array
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___m_Array_0;
	// System.Int32 TMPro.TMP_Text/TextBackingContainer::m_Count
	int32_t ___m_Count_1;
};
// Native definition for P/Invoke marshalling of TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ___m_Array_0;
	int32_t ___m_Count_1;
};
// Native definition for COM marshalling of TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ___m_Array_0;
	int32_t ___m_Count_1;
};

// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32>
struct TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference>
struct TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	MaterialReferenceU5BU5D_t7491D335AB3E3E13CE9C0F5E931F396F6A02E1F2* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 
{
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;
};

struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11_StaticFields
{
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___defaultColorBlock_7;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// TMPro.Extents
struct Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 
{
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___min_2;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___max_3;
};

struct Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8_StaticFields
{
	// TMPro.Extents TMPro.Extents::zero
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___zero_0;
	// TMPro.Extents TMPro.Extents::uninitialized
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___uninitialized_1;
};

// TMPro.HighlightState
struct HighlightState_tE4F50287E5E2E91D42AB77DEA281D88D3AD6A28B 
{
	// UnityEngine.Color32 TMPro.HighlightState::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_0;
	// TMPro.TMP_Offset TMPro.HighlightState::padding
	TMP_Offset_t2262BE4E87D9662487777FF8FFE1B17B0E4438C6 ___padding_1;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;
};

// TMPro.VertexGradient
struct VertexGradient_t2C057B53C0EA6E987C2B7BAB0305E686DA1C9A8F 
{
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___bottomRight_3;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState>
struct TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	HighlightStateU5BU5D_tA878A0AF1F4F52882ACD29515AADC277EE135622* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	HighlightState_tE4F50287E5E2E91D42AB77DEA281D88D3AD6A28B ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// TMPro.TMP_LineInfo
struct TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 
{
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___lineExtents_19;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// TMPro.WordWrapState
struct WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A 
{
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::startOfLineAscender
	float ___startOfLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_14;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_15;
	// System.Single TMPro.WordWrapState::pageAscender
	float ___pageAscender_16;
	// TMPro.HorizontalAlignmentOptions TMPro.WordWrapState::horizontalAlignment
	int32_t ___horizontalAlignment_17;
	// System.Single TMPro.WordWrapState::marginLeft
	float ___marginLeft_18;
	// System.Single TMPro.WordWrapState::marginRight
	float ___marginRight_19;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_20;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_21;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_22;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_23;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_24;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_25;
	// System.Int32 TMPro.WordWrapState::italicAngle
	int32_t ___italicAngle_26;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_27;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_28;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_29;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_30;
	// System.Boolean TMPro.WordWrapState::isDrivenLineSpacing
	bool ___isDrivenLineSpacing_31;
	// System.Single TMPro.WordWrapState::glyphHorizontalAdvanceAdjustment
	float ___glyphHorizontalAdvanceAdjustment_32;
	// System.Single TMPro.WordWrapState::cSpace
	float ___cSpace_33;
	// System.Single TMPro.WordWrapState::mSpace
	float ___mSpace_34;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___textInfo_35;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 ___lineInfo_36;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_37;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_38;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_39;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_40;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___basicStyleStack_41;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::italicAngleStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___italicAngleStack_42;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___colorStack_43;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___underlineColorStack_44;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___strikethroughColorStack_45;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___highlightColorStack_46;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState> TMPro.WordWrapState::highlightStateStack
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___highlightStateStack_47;
	// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___colorGradientStack_48;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___sizeStack_49;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___indentStack_50;
	// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___fontWeightStack_51;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___styleStack_52;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___baselineStack_53;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___actionStack_54;
	// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___materialReferenceStack_55;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___lineJustificationStack_56;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_57;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___currentFontAsset_58;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___currentSpriteAsset_59;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_60;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_61;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___meshExtents_62;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_63;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_64;
};
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___startOfLineAscender_13;
	float ___maxLineAscender_14;
	float ___maxLineDescender_15;
	float ___pageAscender_16;
	int32_t ___horizontalAlignment_17;
	float ___marginLeft_18;
	float ___marginRight_19;
	float ___xAdvance_20;
	float ___preferredWidth_21;
	float ___preferredHeight_22;
	float ___previousLineScale_23;
	int32_t ___wordCount_24;
	int32_t ___fontStyle_25;
	int32_t ___italicAngle_26;
	float ___fontScaleMultiplier_27;
	float ___currentFontSize_28;
	float ___baselineOffset_29;
	float ___lineOffset_30;
	int32_t ___isDrivenLineSpacing_31;
	float ___glyphHorizontalAdvanceAdjustment_32;
	float ___cSpace_33;
	float ___mSpace_34;
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___textInfo_35;
	TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 ___lineInfo_36;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_37;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_38;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_39;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_40;
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___basicStyleStack_41;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___italicAngleStack_42;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___colorStack_43;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___underlineColorStack_44;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___strikethroughColorStack_45;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___highlightColorStack_46;
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___highlightStateStack_47;
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___colorGradientStack_48;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___sizeStack_49;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___indentStack_50;
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___fontWeightStack_51;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___styleStack_52;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___baselineStack_53;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___actionStack_54;
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___materialReferenceStack_55;
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___lineJustificationStack_56;
	int32_t ___spriteAnimationID_57;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___currentFontAsset_58;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___currentSpriteAsset_59;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_60;
	int32_t ___currentMaterialIndex_61;
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___meshExtents_62;
	int32_t ___tagNoParsing_63;
	int32_t ___isNonBreakingSpace_64;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___startOfLineAscender_13;
	float ___maxLineAscender_14;
	float ___maxLineDescender_15;
	float ___pageAscender_16;
	int32_t ___horizontalAlignment_17;
	float ___marginLeft_18;
	float ___marginRight_19;
	float ___xAdvance_20;
	float ___preferredWidth_21;
	float ___preferredHeight_22;
	float ___previousLineScale_23;
	int32_t ___wordCount_24;
	int32_t ___fontStyle_25;
	int32_t ___italicAngle_26;
	float ___fontScaleMultiplier_27;
	float ___currentFontSize_28;
	float ___baselineOffset_29;
	float ___lineOffset_30;
	int32_t ___isDrivenLineSpacing_31;
	float ___glyphHorizontalAdvanceAdjustment_32;
	float ___cSpace_33;
	float ___mSpace_34;
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___textInfo_35;
	TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 ___lineInfo_36;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_37;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_38;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_39;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_40;
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___basicStyleStack_41;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___italicAngleStack_42;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___colorStack_43;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___underlineColorStack_44;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___strikethroughColorStack_45;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___highlightColorStack_46;
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___highlightStateStack_47;
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___colorGradientStack_48;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___sizeStack_49;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___indentStack_50;
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___fontWeightStack_51;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___styleStack_52;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___baselineStack_53;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___actionStack_54;
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___materialReferenceStack_55;
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___lineJustificationStack_56;
	int32_t ___spriteAnimationID_57;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___currentFontAsset_58;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___currentSpriteAsset_59;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_60;
	int32_t ___currentMaterialIndex_61;
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___meshExtents_62;
	int32_t ___tagNoParsing_63;
	int32_t ___isNonBreakingSpace_64;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.WordWrapState>
struct TMP_TextProcessingStack_1_t2DDA00FFC64AF6E3AFD475AB2086D16C34787E0F 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	WordWrapStateU5BU5D_t473D59C9DBCC949CE72EF1EB471CBA152A6CEAC9* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// CubeRotate
struct CubeRotate_t28274E131964C53FEEBDE5D076CFAFF181D3C45F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// Growth_Menu
struct Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 Growth_Menu::time_S
	int32_t ___time_S_4;
	// System.Int32 Growth_Menu::time_M
	int32_t ___time_M_5;
	// System.Int32 Growth_Menu::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean Growth_Menu::stop
	bool ___stop_7;
	// UnityEngine.GameObject Growth_Menu::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_8;
	// UnityEngine.UI.Text Growth_Menu::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_9;
	// UnityEngine.GameObject Growth_Menu::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_10;
	// UnityEngine.GameObject Growth_Menu::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject Growth_Menu::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject Growth_Menu::original1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original1_13;
	// UnityEngine.GameObject Growth_Menu::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// System.Int32 Growth_Menu::p1
	int32_t ___p1_15;
	// System.Int32 Growth_Menu::p2
	int32_t ___p2_16;
	// System.Int32 Growth_Menu::p3
	int32_t ___p3_17;
	// System.Int32 Growth_Menu::p4
	int32_t ___p4_18;
	// System.Int32 Growth_Menu::p5
	int32_t ___p5_19;
	// System.Int32 Growth_Menu::p6
	int32_t ___p6_20;
	// System.Int32 Growth_Menu::p7
	int32_t ___p7_21;
	// System.Int32 Growth_Menu::p8
	int32_t ___p8_22;
	// TMPro.TMP_Text Growth_Menu::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_23;
	// TMPro.TMP_Text Growth_Menu::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_24;
	// TMPro.TMP_Text Growth_Menu::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_25;
	// TMPro.TMP_Text Growth_Menu::pod4
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod4_26;
	// TMPro.TMP_Text Growth_Menu::pod5
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod5_27;
	// TMPro.TMP_Text Growth_Menu::pod6
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod6_28;
	// TMPro.TMP_Text Growth_Menu::pod7
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod7_29;
	// TMPro.TMP_Text Growth_Menu::pod8
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod8_30;
	// System.Int32 Growth_Menu::mistake
	int32_t ___mistake_31;
	// TMPro.TMP_Text Growth_Menu::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_32;
	// TMPro.TMP_Text Growth_Menu::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_33;
	// UnityEngine.UI.Button Growth_Menu::btnY1A
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY1A_34;
	// UnityEngine.UI.Button Growth_Menu::btnY2A
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY2A_35;
	// UnityEngine.UI.Button Growth_Menu::btnN3A
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN3A_36;
	// UnityEngine.UI.Button Growth_Menu::btnN4A
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN4A_37;
	// UnityEngine.UI.Button Growth_Menu::btnY1B
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY1B_38;
	// UnityEngine.UI.Button Growth_Menu::btnY2B
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY2B_39;
	// UnityEngine.UI.Button Growth_Menu::btnN3B
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN3B_40;
	// UnityEngine.UI.Button Growth_Menu::btnN4B
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN4B_41;
	// UnityEngine.UI.Button Growth_Menu::btnY1C
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY1C_42;
	// UnityEngine.UI.Button Growth_Menu::btnY2C
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY2C_43;
	// UnityEngine.UI.Button Growth_Menu::btnN3C
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN3C_44;
	// UnityEngine.UI.Button Growth_Menu::btnN4C
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN4C_45;
	// UnityEngine.UI.Button Growth_Menu::btnY1D
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY1D_46;
	// UnityEngine.UI.Button Growth_Menu::btnY2D
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnY2D_47;
	// UnityEngine.UI.Button Growth_Menu::btnN3D
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN3D_48;
	// UnityEngine.UI.Button Growth_Menu::btnN4D
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___btnN4D_49;
	// System.Boolean Growth_Menu::YesA
	bool ___YesA_50;
	// System.Boolean Growth_Menu::NoA
	bool ___NoA_51;
	// System.Boolean Growth_Menu::YesB
	bool ___YesB_52;
	// System.Boolean Growth_Menu::NoB
	bool ___NoB_53;
	// System.Boolean Growth_Menu::YesC
	bool ___YesC_54;
	// System.Boolean Growth_Menu::NoC
	bool ___NoC_55;
	// System.Boolean Growth_Menu::YesD
	bool ___YesD_56;
	// System.Boolean Growth_Menu::NoD
	bool ___NoD_57;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpA
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpA_58;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpA1
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpA1_59;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpB
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpB_60;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpB1
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpB1_61;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpC
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpC_62;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpC1
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpC1_63;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpD
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpD_64;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpD1
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpD1_65;
	// UnityEngine.UI.Button Growth_Menu::BtnHelp
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelp_66;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpAuse
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpAuse_67;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpA1use
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpA1use_68;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpBuse
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpBuse_69;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpB1use
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpB1use_70;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpCuse
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpCuse_71;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpC1use
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpC1use_72;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpDuse
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpDuse_73;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpD1use
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpD1use_74;
	// UnityEngine.UI.Button Growth_Menu::BtnHelpuse
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___BtnHelpuse_75;
	// UnityEngine.GameObject Growth_Menu::PanelA
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelA_76;
	// UnityEngine.GameObject Growth_Menu::PanelA1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelA1_77;
	// UnityEngine.GameObject Growth_Menu::PanelB
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelB_78;
	// UnityEngine.GameObject Growth_Menu::PanelB1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelB1_79;
	// UnityEngine.GameObject Growth_Menu::PanelC
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelC_80;
	// UnityEngine.GameObject Growth_Menu::PanelC1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelC1_81;
	// UnityEngine.GameObject Growth_Menu::PanelD
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelD_82;
	// UnityEngine.GameObject Growth_Menu::PanelD1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PanelD1_83;
	// System.Boolean Growth_Menu::HelpA
	bool ___HelpA_84;
	// System.Boolean Growth_Menu::HelpA1
	bool ___HelpA1_85;
	// System.Boolean Growth_Menu::HelpB
	bool ___HelpB_86;
	// System.Boolean Growth_Menu::HelpB1
	bool ___HelpB1_87;
	// System.Boolean Growth_Menu::HelpC
	bool ___HelpC_88;
	// System.Boolean Growth_Menu::HelpC1
	bool ___HelpC1_89;
	// System.Boolean Growth_Menu::HelpD
	bool ___HelpD_90;
	// System.Boolean Growth_Menu::Help
	bool ___Help_91;
};

// Task_Door_manager
struct Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject Task_Door_manager::DLGMHints
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___DLGMHints_4;
	// UnityEngine.GameObject Task_Door_manager::btnStatistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___btnStatistics_5;
	// UnityEngine.GameObject Task_Door_manager::DLGMStatistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___DLGMStatistics_6;
	// UnityEngine.GameObject Task_Door_manager::People
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___People_7;
	// UnityEngine.GameObject Task_Door_manager::ElevatorRotate
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ElevatorRotate_8;
	// UnityEngine.GameObject Task_Door_manager::hints_people
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hints_people_9;
	// UnityEngine.GameObject Task_Door_manager::hints_rotate
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hints_rotate_10;
	// UnityEngine.UI.Text Task_Door_manager::Placeholder
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Placeholder_11;
	// UnityEngine.UI.Text Task_Door_manager::Answer
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answer_12;
	// System.Int32 Task_Door_manager::time
	int32_t ___time_13;
	// System.Int32 Task_Door_manager::count_activePeople
	int32_t ___count_activePeople_14;
	// System.Int32 Task_Door_manager::count_activeRotate
	int32_t ___count_activeRotate_15;
	// System.Int32 Task_Door_manager::count_input_error
	int32_t ___count_input_error_16;
	// System.Int32 Task_Door_manager::time_start
	int32_t ___time_start_17;
	// TMPro.TMP_Text Task_Door_manager::text_peo
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___text_peo_18;
	// TMPro.TMP_Text Task_Door_manager::text_rot
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___text_rot_19;
	// TMPro.TMP_Text Task_Door_manager::text_error
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___text_error_20;
	// TMPro.TMP_Text Task_Door_manager::text_time
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___text_time_21;
	// UnityEngine.UI.Toggle Task_Door_manager::tog_people
	Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* ___tog_people_22;
	// UnityEngine.UI.Toggle Task_Door_manager::tog_rotate
	Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* ___tog_rotate_23;
	// UnityEngine.GameObject Task_Door_manager::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_24;
	// UnityEngine.GameObject Task_Door_manager::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_25;
	// UnityEngine.GameObject Task_Door_manager::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_26;
	// UnityEngine.GameObject Task_Door_manager::info
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___info_27;
	// System.Boolean Task_Door_manager::tog_people_active
	bool ___tog_people_active_28;
	// System.Boolean Task_Door_manager::tog_rotate_active
	bool ___tog_rotate_active_29;
	// System.Int32 Task_Door_manager::temp
	int32_t ___temp_30;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// moduli
struct moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject moduli::info
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___info_4;
	// System.Boolean moduli::textNotDisplayed
	bool ___textNotDisplayed_5;
	// System.Single moduli::timeToHideText
	float ___timeToHideText_6;
	// System.Boolean moduli::k
	bool ___k_7;
	// System.Boolean moduli::t
	bool ___t_8;
};

// perehodMain
struct perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// perehodmodul1
struct perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

struct perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_StaticFields
{
	// System.Boolean perehodmodul1::r
	bool ___r_4;
};

// perehodmodul2
struct perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

struct perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_StaticFields
{
	// System.Boolean perehodmodul2::r
	bool ___r_4;
};

// perehodmodul3
struct perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

struct perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_StaticFields
{
	// System.Boolean perehodmodul3::r
	bool ___r_4;
};

// perehodmodul4
struct perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

struct perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_StaticFields
{
	// System.Boolean perehodmodul4::r
	bool ___r_4;
};

// task1
struct task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task1::time_S
	int32_t ___time_S_4;
	// System.Int32 task1::time_M
	int32_t ___time_M_5;
	// System.Int32 task1::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task1::stop
	bool ___stop_7;
	// UnityEngine.GameObject task1::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task1::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task1::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task1::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task1::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task1::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task1::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// UnityEngine.GameObject task1::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_15;
	// UnityEngine.GameObject task1::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_16;
	// UnityEngine.GameObject task1::podskaz1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1_17;
	// UnityEngine.GameObject task1::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_18;
	// UnityEngine.GameObject task1::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task1::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// System.Int32 task1::p1
	int32_t ___p1_21;
	// System.Int32 task1::p2
	int32_t ___p2_22;
	// TMPro.TMP_Text task1::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_23;
	// TMPro.TMP_Text task1::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_24;
	// System.Int32 task1::mistake
	int32_t ___mistake_25;
	// TMPro.TMP_Text task1::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_26;
	// TMPro.TMP_Text task1::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_27;
};

// task10
struct task10_tDE5028C32B8C330404B0B6888C2780FAC891472F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task10::time_S
	int32_t ___time_S_4;
	// System.Int32 task10::time_M
	int32_t ___time_M_5;
	// System.Int32 task10::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task10::stop
	bool ___stop_7;
	// UnityEngine.GameObject task10::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task10::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task10::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task10::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task10::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task10::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task10::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// UnityEngine.GameObject task10::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_15;
	// UnityEngine.GameObject task10::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_16;
	// UnityEngine.GameObject task10::hint3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint3_17;
	// UnityEngine.GameObject task10::podskaz1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1_18;
	// UnityEngine.GameObject task10::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_19;
	// UnityEngine.GameObject task10::podskaz3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3_20;
	// UnityEngine.GameObject task10::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_21;
	// UnityEngine.GameObject task10::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_22;
	// UnityEngine.GameObject task10::podskaz3use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3use_23;
	// System.Int32 task10::p1
	int32_t ___p1_24;
	// System.Int32 task10::p2
	int32_t ___p2_25;
	// System.Int32 task10::p3
	int32_t ___p3_26;
	// TMPro.TMP_Text task10::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_27;
	// TMPro.TMP_Text task10::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_28;
	// TMPro.TMP_Text task10::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_29;
	// System.Int32 task10::mistake
	int32_t ___mistake_30;
	// TMPro.TMP_Text task10::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_31;
	// TMPro.TMP_Text task10::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_32;
};

// task11
struct task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task11::time_S
	int32_t ___time_S_4;
	// System.Int32 task11::time_M
	int32_t ___time_M_5;
	// System.Int32 task11::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task11::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task11::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task11::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task11::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task11::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task11::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task11::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task11::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task11::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task11::hint3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint3_16;
	// UnityEngine.GameObject task11::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_17;
	// UnityEngine.GameObject task11::podskaz3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3_18;
	// UnityEngine.GameObject task11::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task11::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// UnityEngine.GameObject task11::podskaz3use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3use_21;
	// System.Int32 task11::p1
	int32_t ___p1_22;
	// System.Int32 task11::p2
	int32_t ___p2_23;
	// System.Int32 task11::p3
	int32_t ___p3_24;
	// TMPro.TMP_Text task11::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_25;
	// TMPro.TMP_Text task11::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_26;
	// TMPro.TMP_Text task11::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_27;
	// System.Int32 task11::mistake
	int32_t ___mistake_28;
	// TMPro.TMP_Text task11::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_29;
	// TMPro.TMP_Text task11::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_30;
};

// task12
struct task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task12::time_S
	int32_t ___time_S_4;
	// System.Int32 task12::time_M
	int32_t ___time_M_5;
	// System.Int32 task12::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task12::stop
	bool ___stop_7;
	// UnityEngine.GameObject task12::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task12::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task12::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task12::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task12::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task12::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task12::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// UnityEngine.GameObject task12::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_15;
	// UnityEngine.GameObject task12::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_16;
	// UnityEngine.GameObject task12::podskaz1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1_17;
	// UnityEngine.GameObject task12::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_18;
	// UnityEngine.GameObject task12::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task12::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// System.Int32 task12::p1
	int32_t ___p1_21;
	// System.Int32 task12::p2
	int32_t ___p2_22;
	// System.Int32 task12::p3
	int32_t ___p3_23;
	// TMPro.TMP_Text task12::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_24;
	// TMPro.TMP_Text task12::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_25;
	// TMPro.TMP_Text task12::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_26;
	// System.Int32 task12::mistake
	int32_t ___mistake_27;
	// TMPro.TMP_Text task12::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_28;
	// TMPro.TMP_Text task12::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_29;
	// UnityEngine.GameObject task12::usl1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl1_30;
	// UnityEngine.GameObject task12::usl2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl2_31;
};

// task15
struct task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task15::time_S
	int32_t ___time_S_4;
	// System.Int32 task15::time_M
	int32_t ___time_M_5;
	// System.Int32 task15::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task15::stop
	bool ___stop_7;
	// UnityEngine.GameObject task15::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task15::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task15::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task15::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task15::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task15::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task15::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// UnityEngine.GameObject task15::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_15;
	// UnityEngine.GameObject task15::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_16;
	// UnityEngine.GameObject task15::podskaz1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1_17;
	// UnityEngine.GameObject task15::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_18;
	// UnityEngine.GameObject task15::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task15::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// System.Int32 task15::p1
	int32_t ___p1_21;
	// System.Int32 task15::p2
	int32_t ___p2_22;
	// TMPro.TMP_Text task15::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_23;
	// TMPro.TMP_Text task15::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_24;
	// System.Int32 task15::mistake
	int32_t ___mistake_25;
	// TMPro.TMP_Text task15::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_26;
	// TMPro.TMP_Text task15::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_27;
	// UnityEngine.GameObject task15::usl1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl1_28;
	// UnityEngine.GameObject task15::usl2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl2_29;
};

// task16
struct task16_t56A0437BE43073063DF2885A7A931FFF46DD8331  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task16::time_S
	int32_t ___time_S_4;
	// System.Int32 task16::time_M
	int32_t ___time_M_5;
	// System.Int32 task16::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task16::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task16::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task16::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task16::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task16::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task16::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task16::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task16::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task16::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task16::hint3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint3_16;
	// UnityEngine.GameObject task16::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_17;
	// UnityEngine.GameObject task16::podskaz3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3_18;
	// UnityEngine.GameObject task16::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task16::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// UnityEngine.GameObject task16::podskaz3use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3use_21;
	// System.Int32 task16::p1
	int32_t ___p1_22;
	// System.Int32 task16::p2
	int32_t ___p2_23;
	// System.Int32 task16::p3
	int32_t ___p3_24;
	// TMPro.TMP_Text task16::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_25;
	// TMPro.TMP_Text task16::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_26;
	// TMPro.TMP_Text task16::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_27;
	// System.Int32 task16::mistake
	int32_t ___mistake_28;
	// TMPro.TMP_Text task16::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_29;
	// TMPro.TMP_Text task16::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_30;
	// UnityEngine.GameObject task16::usl1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl1_31;
	// UnityEngine.GameObject task16::usl2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl2_32;
	// UnityEngine.UI.Dropdown task16::dropdown1
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown1_33;
	// UnityEngine.UI.Dropdown task16::dropdown2
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown2_34;
	// UnityEngine.UI.Text task16::TextCount
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___TextCount_35;
	// UnityEngine.UI.InputField task16::inputField
	InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* ___inputField_36;
	// System.String task16::countStr
	String_t* ___countStr_37;
	// System.Single task16::count
	float ___count_38;
};

// task17
struct task17_t31BD90159BED97C8AF144D4987B7CD17FB655180  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task17::time_S
	int32_t ___time_S_4;
	// System.Int32 task17::time_M
	int32_t ___time_M_5;
	// System.Int32 task17::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task17::stop
	bool ___stop_7;
	// UnityEngine.GameObject task17::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task17::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task17::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task17::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task17::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task17::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task17::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// UnityEngine.GameObject task17::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_15;
	// UnityEngine.GameObject task17::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_16;
	// UnityEngine.GameObject task17::podskaz1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1_17;
	// UnityEngine.GameObject task17::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_18;
	// UnityEngine.GameObject task17::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task17::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// System.Int32 task17::p1
	int32_t ___p1_21;
	// System.Int32 task17::p2
	int32_t ___p2_22;
	// TMPro.TMP_Text task17::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_23;
	// TMPro.TMP_Text task17::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_24;
	// System.Int32 task17::mistake
	int32_t ___mistake_25;
	// TMPro.TMP_Text task17::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_26;
	// TMPro.TMP_Text task17::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_27;
};

// task18
struct task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task18::time_S
	int32_t ___time_S_4;
	// System.Int32 task18::time_M
	int32_t ___time_M_5;
	// System.Int32 task18::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task18::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task18::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task18::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task18::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task18::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task18::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task18::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task18::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task18::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_15;
	// System.Int32 task18::p1
	int32_t ___p1_16;
	// TMPro.TMP_Text task18::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_17;
	// System.Int32 task18::mistake
	int32_t ___mistake_18;
	// TMPro.TMP_Text task18::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_19;
	// TMPro.TMP_Text task18::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_20;
};

// task19
struct task19_t98E5806FE743E604F715DFA8F534387E08B569CC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task19::time_S
	int32_t ___time_S_4;
	// System.Int32 task19::time_M
	int32_t ___time_M_5;
	// System.Int32 task19::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task19::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task19::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task19::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task19::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task19::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task19::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task19::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task19::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task19::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task19::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_16;
	// UnityEngine.GameObject task19::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_17;
	// UnityEngine.GameObject task19::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_18;
	// System.Int32 task19::p1
	int32_t ___p1_19;
	// System.Int32 task19::p2
	int32_t ___p2_20;
	// TMPro.TMP_Text task19::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_21;
	// TMPro.TMP_Text task19::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_22;
	// System.Int32 task19::mistake
	int32_t ___mistake_23;
	// TMPro.TMP_Text task19::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_24;
	// TMPro.TMP_Text task19::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_25;
	// UnityEngine.GameObject task19::usl1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl1_26;
	// UnityEngine.GameObject task19::usl2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl2_27;
};

// task2
struct task2_t2C5D69F0A329B1B40F552703669EA12C355019C4  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task2::time_S
	int32_t ___time_S_4;
	// System.Int32 task2::time_M
	int32_t ___time_M_5;
	// System.Int32 task2::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task2::stop
	bool ___stop_7;
	// UnityEngine.GameObject task2::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task2::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task2::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task2::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task2::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task2::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task2::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// System.Int32 task2::p1
	int32_t ___p1_15;
	// System.Int32 task2::p2
	int32_t ___p2_16;
	// UnityEngine.GameObject task2::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_17;
	// UnityEngine.GameObject task2::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_18;
	// TMPro.TMP_Text task2::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_19;
	// TMPro.TMP_Text task2::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_20;
	// System.Int32 task2::mistake
	int32_t ___mistake_21;
	// TMPro.TMP_Text task2::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_22;
	// TMPro.TMP_Text task2::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_23;
	// UnityEngine.GameObject task2::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_24;
	// UnityEngine.GameObject task2::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_25;
};

// task20
struct task20_t3D21D377B9C69943268EEB49715FC68A77487D1C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task20::time_S
	int32_t ___time_S_4;
	// System.Int32 task20::time_M
	int32_t ___time_M_5;
	// System.Int32 task20::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task20::stop
	bool ___stop_7;
	// UnityEngine.GameObject task20::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task20::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task20::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task20::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task20::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task20::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task20::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// UnityEngine.GameObject task20::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_15;
	// UnityEngine.GameObject task20::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_16;
	// UnityEngine.GameObject task20::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_17;
	// System.Int32 task20::p1
	int32_t ___p1_18;
	// System.Int32 task20::p2
	int32_t ___p2_19;
	// TMPro.TMP_Text task20::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_20;
	// TMPro.TMP_Text task20::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_21;
	// System.Int32 task20::mistake
	int32_t ___mistake_22;
	// TMPro.TMP_Text task20::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_23;
	// TMPro.TMP_Text task20::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_24;
};

// task21
struct task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task21::time_S
	int32_t ___time_S_4;
	// System.Int32 task21::time_M
	int32_t ___time_M_5;
	// System.Int32 task21::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task21::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task21::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task21::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task21::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task21::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task21::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task21::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task21::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task21::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_15;
	// System.Int32 task21::p1
	int32_t ___p1_16;
	// TMPro.TMP_Text task21::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_17;
	// System.Int32 task21::mistake
	int32_t ___mistake_18;
	// TMPro.TMP_Text task21::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_19;
	// TMPro.TMP_Text task21::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_20;
};

// task23
struct task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task23::time_S
	int32_t ___time_S_4;
	// System.Int32 task23::time_M
	int32_t ___time_M_5;
	// System.Int32 task23::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task23::stop
	bool ___stop_7;
	// UnityEngine.GameObject task23::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_8;
	// UnityEngine.UI.Text task23::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_9;
	// UnityEngine.GameObject task23::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_10;
	// UnityEngine.UI.Text task23::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_11;
	// UnityEngine.GameObject task23::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_12;
	// UnityEngine.GameObject task23::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_13;
	// UnityEngine.GameObject task23::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_14;
	// System.Int32 task23::mistake
	int32_t ___mistake_15;
	// UnityEngine.UI.Text task23::texttime
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___texttime_16;
	// UnityEngine.UI.Text task23::textoshibka
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textoshibka_17;
	// UnityEngine.GameObject task23::Textpodskaz
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___Textpodskaz_18;
	// UnityEngine.UI.Toggle task23::tog_people
	Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* ___tog_people_19;
	// UnityEngine.UI.Toggle task23::tog_rotate
	Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* ___tog_rotate_20;
	// System.Boolean task23::tog_people_active
	bool ___tog_people_active_21;
	// System.Boolean task23::tog_rotate_active
	bool ___tog_rotate_active_22;
	// UnityEngine.GameObject task23::People
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___People_23;
	// UnityEngine.GameObject task23::ElevatorRotate
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ElevatorRotate_24;
	// UnityEngine.GameObject task23::hints_people
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hints_people_25;
	// UnityEngine.GameObject task23::hints_rotate
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hints_rotate_26;
	// System.Int32 task23::count_activePeople
	int32_t ___count_activePeople_27;
	// System.Int32 task23::count_activeRotate
	int32_t ___count_activeRotate_28;
	// UnityEngine.UI.Text task23::text_peo
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___text_peo_29;
	// UnityEngine.UI.Text task23::text_rot
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___text_rot_30;
};

// task24
struct task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task24::time_S
	int32_t ___time_S_4;
	// System.Int32 task24::time_M
	int32_t ___time_M_5;
	// System.Int32 task24::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task24::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task24::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task24::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task24::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task24::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task24::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task24::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task24::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task24::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task24::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_16;
	// UnityEngine.GameObject task24::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_17;
	// UnityEngine.GameObject task24::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_18;
	// System.Int32 task24::p1
	int32_t ___p1_19;
	// System.Int32 task24::p2
	int32_t ___p2_20;
	// TMPro.TMP_Text task24::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_21;
	// TMPro.TMP_Text task24::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_22;
	// System.Int32 task24::mistake
	int32_t ___mistake_23;
	// TMPro.TMP_Text task24::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_24;
	// TMPro.TMP_Text task24::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_25;
};

// task4
struct task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task4::time_S
	int32_t ___time_S_4;
	// System.Int32 task4::time_M
	int32_t ___time_M_5;
	// System.Int32 task4::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task4::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task4::Answ1
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ1_8;
	// UnityEngine.UI.Text task4::Answ2
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ2_9;
	// UnityEngine.UI.Text task4::Answ3
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ3_10;
	// UnityEngine.UI.Text task4::Answ4
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ4_11;
	// UnityEngine.GameObject task4::ar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ar_12;
	// UnityEngine.GameObject task4::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_13;
	// UnityEngine.UI.Text task4::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_14;
	// UnityEngine.GameObject task4::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_15;
	// UnityEngine.GameObject task4::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_16;
	// UnityEngine.GameObject task4::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_17;
	// UnityEngine.GameObject task4::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_18;
	// UnityEngine.GameObject task4::podskaz1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1_19;
	// UnityEngine.GameObject task4::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_20;
	// System.Int32 task4::p1
	int32_t ___p1_21;
	// TMPro.TMP_Text task4::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_22;
	// System.Int32 task4::mistake
	int32_t ___mistake_23;
	// TMPro.TMP_Text task4::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_24;
	// TMPro.TMP_Text task4::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_25;
	// UnityEngine.GameObject task4::usl1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl1_26;
	// UnityEngine.GameObject task4::usl2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl2_27;
};

// task5
struct task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task5::time_S
	int32_t ___time_S_4;
	// System.Int32 task5::time_M
	int32_t ___time_M_5;
	// System.Int32 task5::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task5::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task5::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task5::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task5::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task5::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task5::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task5::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task5::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task5::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task5::hint3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint3_16;
	// UnityEngine.GameObject task5::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_17;
	// UnityEngine.GameObject task5::podskaz3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3_18;
	// UnityEngine.GameObject task5::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task5::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// UnityEngine.GameObject task5::podskaz3use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3use_21;
	// System.Int32 task5::p1
	int32_t ___p1_22;
	// System.Int32 task5::p2
	int32_t ___p2_23;
	// System.Int32 task5::p3
	int32_t ___p3_24;
	// TMPro.TMP_Text task5::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_25;
	// TMPro.TMP_Text task5::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_26;
	// TMPro.TMP_Text task5::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_27;
	// System.Int32 task5::mistake
	int32_t ___mistake_28;
	// TMPro.TMP_Text task5::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_29;
	// TMPro.TMP_Text task5::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_30;
};

// task6
struct task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task6::time_S
	int32_t ___time_S_4;
	// System.Int32 task6::time_M
	int32_t ___time_M_5;
	// System.Int32 task6::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task6::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task6::Answ1
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ1_8;
	// UnityEngine.UI.Text task6::Answ2
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ2_9;
	// UnityEngine.UI.Text task6::Answ3
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ3_10;
	// UnityEngine.UI.Text task6::Answ4
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ4_11;
	// UnityEngine.UI.Text task6::Answ5
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ5_12;
	// UnityEngine.UI.Text task6::Answ6
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ6_13;
	// UnityEngine.GameObject task6::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_14;
	// UnityEngine.UI.Text task6::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_15;
	// UnityEngine.GameObject task6::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_16;
	// UnityEngine.GameObject task6::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_17;
	// UnityEngine.GameObject task6::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_18;
	// UnityEngine.GameObject task6::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_19;
	// UnityEngine.GameObject task6::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_20;
	// System.Int32 task6::p1
	int32_t ___p1_21;
	// TMPro.TMP_Text task6::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_22;
	// System.Int32 task6::mistake
	int32_t ___mistake_23;
	// TMPro.TMP_Text task6::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_24;
	// TMPro.TMP_Text task6::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_25;
	// UnityEngine.GameObject task6::usl1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl1_26;
	// UnityEngine.GameObject task6::usl2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___usl2_27;
};

// task7
struct task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task7::time_S
	int32_t ___time_S_4;
	// System.Int32 task7::time_M
	int32_t ___time_M_5;
	// System.Int32 task7::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task7::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task7::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task7::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task7::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task7::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task7::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task7::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task7::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task7::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task7::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_16;
	// UnityEngine.GameObject task7::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_17;
	// UnityEngine.GameObject task7::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_18;
	// System.Int32 task7::p1
	int32_t ___p1_19;
	// System.Int32 task7::p2
	int32_t ___p2_20;
	// TMPro.TMP_Text task7::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_21;
	// TMPro.TMP_Text task7::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_22;
	// System.Int32 task7::mistake
	int32_t ___mistake_23;
	// TMPro.TMP_Text task7::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_24;
	// TMPro.TMP_Text task7::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_25;
};

// task8
struct task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task8::time_S
	int32_t ___time_S_4;
	// System.Int32 task8::time_M
	int32_t ___time_M_5;
	// System.Int32 task8::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task8::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task8::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task8::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task8::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task8::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task8::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task8::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task8::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task8::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task8::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_16;
	// UnityEngine.GameObject task8::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_17;
	// UnityEngine.GameObject task8::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_18;
	// System.Int32 task8::p1
	int32_t ___p1_19;
	// System.Int32 task8::p2
	int32_t ___p2_20;
	// TMPro.TMP_Text task8::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_21;
	// TMPro.TMP_Text task8::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_22;
	// System.Int32 task8::mistake
	int32_t ___mistake_23;
	// TMPro.TMP_Text task8::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_24;
	// TMPro.TMP_Text task8::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_25;
};

// task9
struct task9_tC73010C90655823D17F5BABBF0D4C0751EF88867  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 task9::time_S
	int32_t ___time_S_4;
	// System.Int32 task9::time_M
	int32_t ___time_M_5;
	// System.Int32 task9::time_Ch
	int32_t ___time_Ch_6;
	// System.Boolean task9::stop
	bool ___stop_7;
	// UnityEngine.UI.Text task9::Answ
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___Answ_8;
	// UnityEngine.GameObject task9::ok
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ok_9;
	// UnityEngine.UI.Text task9::prov
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___prov_10;
	// UnityEngine.GameObject task9::stat
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___stat_11;
	// UnityEngine.GameObject task9::original
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original_12;
	// UnityEngine.GameObject task9::statistics
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___statistics_13;
	// UnityEngine.GameObject task9::hint1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint1_14;
	// UnityEngine.GameObject task9::hint2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint2_15;
	// UnityEngine.GameObject task9::hint3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hint3_16;
	// UnityEngine.GameObject task9::podskaz2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2_17;
	// UnityEngine.GameObject task9::podskaz3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3_18;
	// UnityEngine.GameObject task9::podskaz1use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz1use_19;
	// UnityEngine.GameObject task9::podskaz2use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz2use_20;
	// UnityEngine.GameObject task9::podskaz3use
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___podskaz3use_21;
	// System.Int32 task9::p1
	int32_t ___p1_22;
	// System.Int32 task9::p2
	int32_t ___p2_23;
	// System.Int32 task9::p3
	int32_t ___p3_24;
	// TMPro.TMP_Text task9::pod1
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod1_25;
	// TMPro.TMP_Text task9::pod2
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod2_26;
	// TMPro.TMP_Text task9::pod3
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___pod3_27;
	// System.Int32 task9::mistake
	int32_t ___mistake_28;
	// TMPro.TMP_Text task9::texttime
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___texttime_29;
	// TMPro.TMP_Text task9::textoshibka
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___textoshibka_30;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;
};

struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_21;
};

// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074* ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35* ___m_CanvasGroupCache_19;
};

struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712_StaticFields
{
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
};

// UnityEngine.UI.Button
struct Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t8EA72E90B3BD1392FB3B3EF167D5121C23569E4C* ___m_OnClick_20;
};

// UnityEngine.UI.Dropdown
struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_Template_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_CaptionText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_CaptionImage_22;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_ItemText_23;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_ItemImage_24;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_25;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t53255477D0A9C6980AB48693A520EFBC94DFFB96* ___m_Options_26;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* ___m_OnValueChanged_27;
	// System.Single UnityEngine.UI.Dropdown::m_AlphaFadeSpeed
	float ___m_AlphaFadeSpeed_28;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_Dropdown_29;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_Blocker_30;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t89B39292AD45371F7FDCB295AAE956D33588BC6E* ___m_Items_31;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t830EC096236A3CEC7189DFA6E0B2E74C5C97780B* ___m_AlphaTweenRunner_32;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_33;
};

struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_StaticFields
{
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* ___s_NoOptionData_35;
};

// UnityEngine.UI.InputField
struct InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A* ___m_Keyboard_20;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_TextComponent_24;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___m_Placeholder_25;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_26;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_27;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_28;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_29;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_30;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_31;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_32;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_33;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnSubmit
	SubmitEvent_t1E0F5A2AB28D0DB55AE18E8DA99147D86492DD5D* ___m_OnSubmit_34;
	// UnityEngine.UI.InputField/EndEditEvent UnityEngine.UI.InputField::m_OnDidEndEdit
	EndEditEvent_t946A962BA13CF60BB0BE7AD091DA041FD788E655* ___m_OnDidEndEdit_35;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_tE4829F88300B0E0E0D1B78B453AF25FC1AA55E2F* ___m_OnValueChanged_36;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t48916A4E9C9FD6204401FF0808C2B7A93D73418B* ___m_OnValidateInput_37;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_CaretColor_38;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_39;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectionColor_40;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_41;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_42;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_43;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_44;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateOnSelect
	bool ___m_ShouldActivateOnSelect_45;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_46;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_47;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___caretRectTrans_48;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_CursorVerts_49;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_InputTextCache_50;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CachedInputRenderer_51;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_52;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_Mesh_53;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_54;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_55;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_56;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_57;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_60;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* ___m_BlinkCoroutine_61;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_62;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_63;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_64;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* ___m_DragCoroutine_65;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_66;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_67;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_68;
	// UnityEngine.WaitForSecondsRealtime UnityEngine.UI.InputField::m_WaitForSecondsRealtime
	WaitForSecondsRealtime_tA8CE0AAB4B0C872B843E7973637037D17682BA01* ___m_WaitForSecondsRealtime_69;
	// System.Boolean UnityEngine.UI.InputField::m_TouchKeyboardAllowsInPlaceEditing
	bool ___m_TouchKeyboardAllowsInPlaceEditing_70;
	// System.Boolean UnityEngine.UI.InputField::m_IsCompositionActive
	bool ___m_IsCompositionActive_71;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB* ___m_ProcessingEvent_74;
};

struct InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_StaticFields
{
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___kSeparators_21;
	// System.Boolean UnityEngine.UI.InputField::s_IsQuestDeviceEvaluated
	bool ___s_IsQuestDeviceEvaluated_22;
	// System.Boolean UnityEngine.UI.InputField::s_IsQuestDevice
	bool ___s_IsQuestDevice_23;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_35;
};

// UnityEngine.UI.Toggle
struct Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_20;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___graphic_21;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_tF2E6FE7D4B17BDBF82462715CFB57C4FDE0A2A2C* ___m_Group_22;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t88B31268F9D6D1882E4F921B14704FB9F7047F02* ___onValueChanged_23;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_24;
};

// TMPro.TMP_Text
struct TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_36;
	// System.Boolean TMPro.TMP_Text::m_IsTextBackingStringDirty
	bool ___m_IsTextBackingStringDirty_37;
	// TMPro.ITextPreprocessor TMPro.TMP_Text::m_TextPreprocessor
	RuntimeObject* ___m_TextPreprocessor_38;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_39;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___m_fontAsset_40;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___m_currentFontAsset_41;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_42;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_sharedMaterial_43;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_currentMaterial_44;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_48;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___m_fontSharedMaterials_49;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_fontMaterial_50;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___m_fontMaterials_51;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_52;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_fontColor32_53;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_fontColor_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_underlineColor_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_strikethroughColor_57;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_58;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_59;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t2C057B53C0EA6E987C2B7BAB0305E686DA1C9A8F ___m_fontColorGradient_60;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB* ___m_fontColorGradientPreset_61;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___m_spriteAsset_62;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_63;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_64;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_spriteColor_65;
	// TMPro.TMP_StyleSheet TMPro.TMP_Text::m_StyleSheet
	TMP_StyleSheet_t70C71699F5CB2D855C361DBB78A44C901236C859* ___m_StyleSheet_66;
	// TMPro.TMP_Style TMPro.TMP_Text::m_TextStyle
	TMP_Style_tA9E5B1B35EBFE24EF980CEA03251B638282E120C* ___m_TextStyle_67;
	// System.Int32 TMPro.TMP_Text::m_TextStyleHashCode
	int32_t ___m_TextStyleHashCode_68;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_69;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_faceColor_70;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_outlineColor_71;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_72;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_73;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_74;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_75;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___m_sizeStack_76;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_77;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_78;
	// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___m_FontWeightStack_79;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_80;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_81;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_82;
	// System.Int32 TMPro.TMP_Text::m_AutoSizeIterationCount
	int32_t ___m_AutoSizeIterationCount_83;
	// System.Int32 TMPro.TMP_Text::m_AutoSizeMaxIterationCount
	int32_t ___m_AutoSizeMaxIterationCount_84;
	// System.Boolean TMPro.TMP_Text::m_IsAutoSizePointSizeSet
	bool ___m_IsAutoSizePointSizeSet_85;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_86;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_87;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_88;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_89;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___m_fontStyleStack_90;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_91;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_Text::m_HorizontalAlignment
	int32_t ___m_HorizontalAlignment_92;
	// TMPro.VerticalAlignmentOptions TMPro.TMP_Text::m_VerticalAlignment
	int32_t ___m_VerticalAlignment_93;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_94;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_95;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___m_lineJustificationStack_96;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_textContainerLocalCorners_97;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_98;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_99;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_100;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_101;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_102;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_103;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_104;
	// System.Boolean TMPro.TMP_Text::m_IsDrivenLineSpacing
	bool ___m_IsDrivenLineSpacing_105;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_106;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_107;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_108;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_109;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_110;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_111;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_112;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_113;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_114;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_115;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_116;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___m_linkedTextComponent_117;
	// TMPro.TMP_Text TMPro.TMP_Text::parentLinkedComponent
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___parentLinkedComponent_118;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_119;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_120;
	// System.Single TMPro.TMP_Text::m_GlyphHorizontalAdvanceAdjustment
	float ___m_GlyphHorizontalAdvanceAdjustment_121;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_122;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_123;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_124;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_125;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_126;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_127;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_128;
	// System.Boolean TMPro.TMP_Text::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_129;
	// System.Boolean TMPro.TMP_Text::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_130;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_131;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_132;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_133;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_134;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_135;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_136;
	// System.Boolean TMPro.TMP_Text::m_IsTextObjectScaleStatic
	bool ___m_IsTextObjectScaleStatic_137;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_138;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_139;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_140;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_141;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_142;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_143;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_144;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_145;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_margin_146;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_147;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_148;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_149;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_150;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_151;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___m_textInfo_152;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_153;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_154;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___m_transform_155;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_rectTransform_156;
	// UnityEngine.Vector2 TMPro.TMP_Text::m_PreviousRectTransformSize
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PreviousRectTransformSize_157;
	// UnityEngine.Vector2 TMPro.TMP_Text::m_PreviousPivotPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PreviousPivotPosition_158;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_159;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_160;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_mesh_161;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_162;
	// System.Action`1<TMPro.TMP_TextInfo> TMPro.TMP_Text::OnPreRenderText
	Action_1_tB93AB717F9D419A1BEC832FF76E74EAA32184CC1* ___OnPreRenderText_165;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2E0F016A61CA343E3222FF51E7CF0E53F9F256E4* ___m_spriteAnimator_166;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_167;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_168;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_169;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_170;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_171;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_172;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___m_LayoutElement_173;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_174;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_175;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_176;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_177;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_178;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_179;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_180;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_181;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_182;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_183;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_184;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_185;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_186;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_190;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_191;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___m_indentStack_192;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_193;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_194;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_FXMatrix_195;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_196;
	// TMPro.TMP_Text/UnicodeChar[] TMPro.TMP_Text::m_TextProcessingArray
	UnicodeCharU5BU5D_t67F27D09F8EB28D2C42DFF16FE60054F157012F5* ___m_TextProcessingArray_197;
	// System.Int32 TMPro.TMP_Text::m_InternalTextProcessingArraySize
	int32_t ___m_InternalTextProcessingArraySize_198;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t297D56FCF66DAA99D8FEA7C30F9F3926902C5B99* ___m_internalCharacterInfo_199;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_200;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_207;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_208;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_209;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_210;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_211;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_212;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_213;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_214;
	// System.Single TMPro.TMP_Text::m_PageAscender
	float ___m_PageAscender_215;
	// System.Single TMPro.TMP_Text::m_maxTextAscender
	float ___m_maxTextAscender_216;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_217;
	// System.Single TMPro.TMP_Text::m_ElementAscender
	float ___m_ElementAscender_218;
	// System.Single TMPro.TMP_Text::m_ElementDescender
	float ___m_ElementDescender_219;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_220;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_221;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_222;
	// System.Single TMPro.TMP_Text::m_startOfLineDescender
	float ___m_startOfLineDescender_223;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_224;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___m_meshExtents_225;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_htmlColor_226;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___m_colorStack_227;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___m_underlineColorStack_228;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___m_strikethroughColorStack_229;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState> TMPro.TMP_Text::m_HighlightStateStack
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___m_HighlightStateStack_230;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB* ___m_colorGradientPreset_231;
	// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___m_colorGradientStack_232;
	// System.Boolean TMPro.TMP_Text::m_colorGradientPresetIsTinted
	bool ___m_colorGradientPresetIsTinted_233;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_234;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_235;
	// TMPro.TMP_TextProcessingStack`1<System.Int32>[] TMPro.TMP_Text::m_TextStyleStacks
	TMP_TextProcessingStack_1U5BU5D_t08293E0BB072311BB96170F351D1083BCA97B9B2* ___m_TextStyleStacks_236;
	// System.Int32 TMPro.TMP_Text::m_TextStyleStackDepth
	int32_t ___m_TextStyleStackDepth_237;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.TMP_Text::m_ItalicAngleStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___m_ItalicAngleStack_238;
	// System.Int32 TMPro.TMP_Text::m_ItalicAngle
	int32_t ___m_ItalicAngle_239;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___m_actionStack_240;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_241;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_242;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___m_baselineOffsetStack_243;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_244;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_245;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t262A55214F712D4274485ABE5676E5254B84D0A5* ___m_cached_TextElement_246;
	// TMPro.TMP_Text/SpecialCharacter TMPro.TMP_Text::m_Ellipsis
	SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777 ___m_Ellipsis_247;
	// TMPro.TMP_Text/SpecialCharacter TMPro.TMP_Text::m_Underline
	SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777 ___m_Underline_248;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___m_defaultSpriteAsset_249;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___m_currentSpriteAsset_250;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_251;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_252;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_253;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_256;
	// TMPro.TMP_Text/TextBackingContainer TMPro.TMP_Text::m_TextBackingArray
	TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361 ___m_TextBackingArray_257;
	// System.Decimal[] TMPro.TMP_Text::k_Power
	DecimalU5BU5D_t93BA0C88FA80728F73B792EE1A5199D0C060B615* ___k_Power_258;
};

struct TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9_StaticFields
{
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t7491D335AB3E3E13CE9C0F5E931F396F6A02E1F2* ___m_materialReferences_45;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ___m_materialReferenceIndexLookup_46;
	// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___m_materialReferenceStack_47;
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_colorWhite_55;
	// System.Func`3<System.Int32,System.String,TMPro.TMP_FontAsset> TMPro.TMP_Text::OnFontAssetRequest
	Func_3_tC721DF8CDD07ED66A4833A19A2ED2302608C906C* ___OnFontAssetRequest_163;
	// System.Func`3<System.Int32,System.String,TMPro.TMP_SpriteAsset> TMPro.TMP_Text::OnSpriteAssetRequest
	Func_3_t6F6D9932638EA1A5A45303C6626C818C25D164E5* ___OnSpriteAssetRequest_164;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_htmlTag_187;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_t5816316EFD8F59DBC30B9F88E15828C564E47B6D* ___m_xmlAttribute_188;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___m_attributeParameterValues_189;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedWordWrapState_201;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedLineState_202;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedEllipsisState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedEllipsisState_203;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLastValidState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedLastValidState_204;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedSoftLineBreakState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedSoftLineBreakState_205;
	// TMPro.TMP_TextProcessingStack`1<TMPro.WordWrapState> TMPro.TMP_Text::m_EllipsisInsertionCandidateStack
	TMP_TextProcessingStack_1_t2DDA00FFC64AF6E3AFD475AB2086D16C34787E0F ___m_EllipsisInsertionCandidateStack_206;
	// Unity.Profiling.ProfilerMarker TMPro.TMP_Text::k_ParseTextMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_ParseTextMarker_254;
	// Unity.Profiling.ProfilerMarker TMPro.TMP_Text::k_InsertNewLineMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_InsertNewLineMarker_255;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___k_LargePositiveVector2_259;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___k_LargeNegativeVector2_260;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_261;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_262;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_263;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_264;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_42;
};

struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_40;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, String_t* ___methodName0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void task18/<stopwatch>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18__ctor_m9C98BAF99D3194A475B10F43E12EC1467D6EF023 (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method) ;
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5 (int32_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* __this, float ___seconds0, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void task1/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mF4FF86AF458829AF5663B0E71B4745CC95329BCD (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task4/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_m409BDAEB39E1686BFF71A2B46CFF8E553453EF8C (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task16/<stopwatch>d__36::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__36__ctor_mC91A3522F3CCBEAA4251614DF2EC589EA21268DA (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.UI.Dropdown::get_value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.UI.InputField::get_text()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline (InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* __this, const RuntimeMethod* method) ;
// System.Single System.Convert::ToSingle(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Convert_ToSingle_m312FA1741E893E5B9D93A208CD15D417B1E4C65C (String_t* ___value0, const RuntimeMethod* method) ;
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972 (float* __this, const RuntimeMethod* method) ;
// System.Void task9/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_mAF135CD78100517B8A047C1CC0CC7FBF7D721E99 (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task12/<stopwatch>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__29__ctor_m1B84C54EC9B518E9CECE83E837776F3276385838 (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E (String_t* ___sceneName0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_mE304382DB9A6455C2A474C8F364C7387F37E9281 (const RuntimeMethod* method) ;
// System.Void task21/<stopwatch>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18__ctor_mC5DF3C8476F781C48C8C06C1F8FE1C134F0AD286 (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task7/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m1665459298850446F9749CA3A9D77E111D5C1874 (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task24/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m4967F2F96916BB941EABA3D4B61599D03E007EC6 (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task11/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_mAD20EA5F58F53CE11B28B998BAF88A97D3995725 (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task23/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_m20ACD953B1BBACC38DDA747FCE0ED1E7EF173B7A (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task23/<VisibleRotate>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__29__ctor_m32FBBD56576C854E4E4A54DF1CB66A92DC28C367 (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void task23/<VisiblePeople>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__38__ctor_m561B71216B1355DF5FAD403B70C373D043A0E5F1 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Toggle_set_isOn_m61D6AB073668E87530A9F49D990A3B3631D2061F (Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* __this, bool ___value0, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D DateTime_get_Now_m636CB9651A9099D20BA1CF813A0C69637317325C (const RuntimeMethod* method) ;
// System.TimeSpan System.DateTime::get_TimeOfDay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A DateTime_get_TimeOfDay_mE8933E5F62C0369E4BA6AF928283A00CA9D54D04 (DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D* __this, const RuntimeMethod* method) ;
// System.Int32 System.TimeSpan::get_Hours()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TimeSpan_get_Hours_m770B4B777A816E051EFDA317C28DA9A4F39D6CFB (TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A* __this, const RuntimeMethod* method) ;
// System.Int32 System.TimeSpan::get_Minutes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TimeSpan_get_Minutes_m93E37D01CD6DA2DE5B35609D740D322E270B678F (TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A* __this, const RuntimeMethod* method) ;
// System.Int32 System.TimeSpan::get_Seconds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TimeSpan_get_Seconds_m8CA21613DC31BD025C5D30D41BAD0ED50827578B (TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A* __this, const RuntimeMethod* method) ;
// System.Void Task_Door_manager/<VisiblePeople>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__29__ctor_m4C06D63C1CC803EC17AC6A2252086760283F82AD (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void Task_Door_manager/<VisibleRotate>d__31::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__31__ctor_m34B001F2552F17A0BD062867405FE3807320C9C7 (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Void task20/<stopwatch>d__22::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__22__ctor_m6FF962E670F0354B1BF097A4F6D365ABD8A8CEF5 (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task15/<stopwatch>d__27::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__27__ctor_m2F5D907BB12AB98E504E9D63C163FFDE0FFF6838 (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3 (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865 (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m2A308205498AFEEA3DF784B1C86E4F7C126CA2EE (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___eulers0, const RuntimeMethod* method) ;
// System.Void task2/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m6A763E528C337619B93BA87550784E8CD7AEB70D (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task6/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mBE7488CED855751FE6A9873967B29D948A39278E (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task17/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mF47A706BF25A5698C8E4C2445AE0B63B9F7E7ABB (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task19/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mACB511D54A1CF1255DE5F80BD47A29E5DDA01A24 (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void Growth_Menu/<stopwatch>d__89::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__89__ctor_m501F56E1C4172B9FDD76399C097A2D1C961CE401 (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void task10/<stopwatch>d__30::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__30__ctor_m67EEB770039EA89A16B5BE37CD3B2B4A0E1380EE (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task8/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m5B9E8360E8BACEF61B8A182C111973FD1348B85A (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void task5/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_mD77B2A6660FF72176CB31E51282D3F5FB5D3E06B (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task18::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_Start_m1AAE1EFFA5315271B1197642276249E743BF3C70 (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_15;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task18::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task18_stopwatch_mEA8809D2BEF7BC2CE791218486EE678C51AC41BA (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* L_0 = (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__18__ctor_m9C98BAF99D3194A475B10F43E12EC1467D6EF023(L_0, 0, NULL);
		U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task18::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_answer_m30E40F26D9BAC45E527BE61F623C1F1D2A571C43 (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "8")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_18;
		__this->___mistake_18 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task18::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_showhint1_mB1C3D2D881AE78E4D5A55D7602937E52116D0D20 (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_15;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_16;
		__this->___p1_16 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task18::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_notshowhint1_m6E9DD3A71239F426A8383961B87C3D52F9F6453E (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task18::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_notshowver_mFD72CD1A5C4BB5913AAA58AAD9FC8401C5F516C5 (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task18::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_showstatistics_m5650CA72DE99959FAE28FB35712EF51F25181E0F (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_17;
		int32_t* L_5 = (&__this->___p1_16);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___textoshibka_20;
		int32_t* L_8 = (&__this->___mistake_18);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___texttime_19;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		int32_t* L_13 = (&__this->___time_Ch_6);
		String_t* L_14;
		L_14 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_13, NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_14);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_12;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = L_15;
		int32_t* L_17 = (&__this->___time_M_5);
		String_t* L_18;
		L_18 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_17, NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_18);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_19;
		int32_t* L_21 = (&__this->___time_S_4);
		String_t* L_22;
		L_22 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_21, NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_22);
		String_t* L_23;
		L_23 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_20, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_23);
		// }
		return;
	}
}
// System.Void task18::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18_notshowstatistics_mBD2FC50E4562BB95666567B0F226BEF0F9C5D5A9 (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task18::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task18__ctor_mEF834A6BC1981BA55DD06B3CD1A094F22EC4A6E2 (task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task18/<stopwatch>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18__ctor_m9C98BAF99D3194A475B10F43E12EC1467D6EF023 (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task18/<stopwatch>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18_System_IDisposable_Dispose_m03C6C59F8C3C0FF5A36144D3BDC693CFA69061C4 (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task18/<stopwatch>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__18_MoveNext_m42F5DFE4E1380ADEACAD6EEC6699B18FED7C83EC (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_5 = V_1;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_11 = V_1;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_17 = V_1;
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task18_tED77E2DDD34A73322928D524DCC6BE8F3B96F3AB* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task18/<stopwatch>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E33CCBBE0AD7F1388E75C63050C728C23B1329B (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task18/<stopwatch>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m4A765E673B465AB278707B6FF492C7595152324E (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m4A765E673B465AB278707B6FF492C7595152324E_RuntimeMethod_var)));
	}
}
// System.Object task18/<stopwatch>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__18_System_Collections_IEnumerator_get_Current_m3E57CA1A69F2ED7DA1C3B19DD22899A8C13ECB93 (U3CstopwatchU3Ed__18_t1BF98BD830471BC1377255B6AE33D1C070D0991C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task1::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_Start_m3CDDCAABF1E9FCDF93D124E6F74AB1B5DDC0A55A (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task1::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task1_stopwatch_mAC140E804336901EE2D25D9A2D6C6CBE8A96CB4E (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* L_0 = (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__25__ctor_mF4FF86AF458829AF5663B0E71B4745CC95329BCD(L_0, 0, NULL);
		U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task1::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_answer_m03F536155A966C9965198798BCCD295C25E3B687 (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68ABB36301BCDDA4EEB3BF7DCA2DFD108BCE35FB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "10:00")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral68ABB36301BCDDA4EEB3BF7DCA2DFD108BCE35FB, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_10;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_12;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_25;
		__this->___mistake_25 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_10;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_11;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_12;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task1::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_showhint1_m4C4905A5980A8F60285FD3937709A023A990762B (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_18;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_21;
		__this->___p1_21 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task1::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_notshowhint1_m58B495C81C4CF335F65500D9336CC32FA14DA61F (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task1::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_showhint2_m189F7CF98EA1C9A47263D3EE311530951C3B790F (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___podskaz2use_20;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___hint2_16;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_22;
		__this->___p2_22 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task1::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_notshowhint2_m745C6BE79B610C92FE18E83DCD2923D3F6A680ED (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task1::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_notshowver_mBD486C906F67837E25A8836358EB6D8691D6006C (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task1::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_showstatistics_m9425EFA1B3BFBE3369ADB102FE515E869153C96A (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_23;
		int32_t* L_5 = (&__this->___p1_21);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_24;
		int32_t* L_8 = (&__this->___p2_22);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_27;
		int32_t* L_11 = (&__this->___mistake_25);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_26;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task1::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_notshowstatistics_m97C72FEA1BDB8AB2B05F178ED137E45C89F06598 (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task1::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_showar_m3156D703A467152E915900D8F55103D2884093D0 (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task1::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1_notshowar_mF3D02CFDB8679942256A373796F833F63F30C749 (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task1__ctor_m6BB42C0D3B2B26637FD35AA11D1868EAB472A1A5 (task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task1/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mF4FF86AF458829AF5663B0E71B4745CC95329BCD (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task1/<stopwatch>d__25::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m58A3826412DA336685BCA3CD51C27470C8FD760F (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task1/<stopwatch>d__25::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__25_MoveNext_mB49BCAFD4060480C1F2071848D3918B43DFF9D95 (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_5 = V_1;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_11 = V_1;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_17 = V_1;
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task1_t7EEA3827DDE29089EED3E889F90F81D869E1600F* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task1/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m958693B16603CEF13E2250BFE3595E3EA6A9884F (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task1/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_mBCC6462A07E23707896D3C34E923E1ED21704053 (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_mBCC6462A07E23707896D3C34E923E1ED21704053_RuntimeMethod_var)));
	}
}
// System.Object task1/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_mB2C5972D09EA4D0BD26BB7F009188AC2D95626E0 (U3CstopwatchU3Ed__25_t37E75DF655C07FC1181F5651924FBC96F8171721* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task4::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_Start_m0B1D772E15AB95915391247131C17D5067FED8D8 (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 00;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_20;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task4::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task4_stopwatch_m95DDCF773F9417486F4A3B7DFD7F9304044095C9 (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* L_0 = (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__25__ctor_m409BDAEB39E1686BFF71A2B46CFF8E553453EF8C(L_0, 0, NULL);
		U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task4::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_answer_m4DE52F25C330730E1191012EC4F210C871D0D333 (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral05232D54BF656F639590A6B59985605128B3D584);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1D953F5E34759B5103309A24C047B28F03C5DA76);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1D98514DA44F57EE99E5E0779EC025797FC2563D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral394FD3AEF9694249DD5036EC8A636CCAC0BBEFB4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral532DCA297F5B95A1298F57EEF603E768B19F3427);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF41DDB4D64CC42FB36F2B7E0929F808D5BC1C499);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF77DC4857B149AEE7F1D08A173014BCAED8EA20F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF85C8F48FAFA3666F9F535246EC370456D1976BC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (((Answ1.text == "07:00") && (Answ2.text == "08:00") && (Answ3.text == "22:00") && (Answ4.text == "23:00")) ||
		//     ((Answ3.text == "7:30") && (Answ4.text == "9:00")) && (Answ1.text == "16:30") && (Answ2.text == "18:00"))
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ1_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral1D98514DA44F57EE99E5E0779EC025797FC2563D, NULL);
		if (!L_2)
		{
			goto IL_005c;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = __this->___Answ2_9;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		bool L_5;
		L_5 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_4, _stringLiteralF41DDB4D64CC42FB36F2B7E0929F808D5BC1C499, NULL);
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___Answ3_10;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
		bool L_8;
		L_8 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_7, _stringLiteral05232D54BF656F639590A6B59985605128B3D584, NULL);
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_9 = __this->___Answ4_11;
		NullCheck(L_9);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
		bool L_11;
		L_11 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_10, _stringLiteralF77DC4857B149AEE7F1D08A173014BCAED8EA20F, NULL);
		if (L_11)
		{
			goto IL_00b8;
		}
	}

IL_005c:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_12 = __this->___Answ3_10;
		NullCheck(L_12);
		String_t* L_13;
		L_13 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_12);
		bool L_14;
		L_14 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_13, _stringLiteral1D953F5E34759B5103309A24C047B28F03C5DA76, NULL);
		if (!L_14)
		{
			goto IL_00f2;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_15 = __this->___Answ4_11;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_15);
		bool L_17;
		L_17 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_16, _stringLiteral532DCA297F5B95A1298F57EEF603E768B19F3427, NULL);
		if (!L_17)
		{
			goto IL_00f2;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_18 = __this->___Answ1_8;
		NullCheck(L_18);
		String_t* L_19;
		L_19 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_18);
		bool L_20;
		L_20 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_19, _stringLiteralF85C8F48FAFA3666F9F535246EC370456D1976BC, NULL);
		if (!L_20)
		{
			goto IL_00f2;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___Answ2_9;
		NullCheck(L_21);
		String_t* L_22;
		L_22 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_21);
		bool L_23;
		L_23 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_22, _stringLiteral394FD3AEF9694249DD5036EC8A636CCAC0BBEFB4, NULL);
		if (!L_23)
		{
			goto IL_00f2;
		}
	}

IL_00b8:
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_24 = __this->___ok_13;
		NullCheck(L_24);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25;
		L_25 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_24, NULL);
		NullCheck(L_25);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_25, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_26 = __this->___prov_14;
		NullCheck(L_26);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_26, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = __this->___stat_15;
		NullCheck(L_27);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_27, NULL);
		NullCheck(L_28);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_28, (bool)1, NULL);
		return;
	}

IL_00f2:
	{
		// mistake++;
		int32_t L_29 = __this->___mistake_23;
		__this->___mistake_23 = ((int32_t)il2cpp_codegen_add(L_29, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30 = __this->___ok_13;
		NullCheck(L_30);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_31;
		L_31 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_30, NULL);
		NullCheck(L_31);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_31, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_32 = __this->___prov_14;
		NullCheck(L_32);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_32, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_33 = __this->___stat_15;
		NullCheck(L_33);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_34;
		L_34 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_33, NULL);
		NullCheck(L_34);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_34, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task4::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_notshowver_m1413B9D28D4E264B82631BFD301BF6D23E0BB28D (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task4::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_showstatistics_m1EE69CE4A686DEB76C973DD61926BCBAF48188BB (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_17;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_22;
		int32_t* L_5 = (&__this->___p1_21);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___textoshibka_25;
		int32_t* L_8 = (&__this->___mistake_23);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___texttime_24;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		int32_t* L_13 = (&__this->___time_Ch_6);
		String_t* L_14;
		L_14 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_13, NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_14);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_12;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = L_15;
		int32_t* L_17 = (&__this->___time_M_5);
		String_t* L_18;
		L_18 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_17, NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_18);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_19;
		int32_t* L_21 = (&__this->___time_S_4);
		String_t* L_22;
		L_22 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_21, NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_22);
		String_t* L_23;
		L_23 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_20, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_23);
		// }
		return;
	}
}
// System.Void task4::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_notshowstatistics_mB117841FA16A32F8CE605E9BC01FA86B31D364AD (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_17;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task4::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_showhint1_m41D33F7070217066E07F6B43AFF81A2B5A761EE1 (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_18;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_20;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_21;
		__this->___p1_21 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task4::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_notshowhint1_m5032296A1C5864BF54B9C840D5D2D3842E4697CD (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_18;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task4::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_showar_m37988850128214C0E8685CB89044779FD5FC01CC (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_12;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task4::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_notshowar_mB09DBBFE50531517979CAE5A298A275120CD462B (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_16;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_12;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task4::nextuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_nextuslzad_mD8368986A60FA50E94965FD5B3D67587A43928EC (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_26;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// usl2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_27;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task4::backuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4_backuslzad_m5DB41551643432B8608F84D9A0D0B9F8B7C0FAF7 (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_26;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// usl2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_27;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task4::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task4__ctor_mA47FE455EA13015141BBE3690DB9CC612842C58B (task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task4/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_m409BDAEB39E1686BFF71A2B46CFF8E553453EF8C (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task4/<stopwatch>d__25::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_mC9B982DB50D40262AEFB26036F95AD5864144C49 (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task4/<stopwatch>d__25::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__25_MoveNext_m85115A36EDAA0AC8AC668128539790BE3606EAE4 (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_5 = V_1;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 00;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_11 = V_1;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 00;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_17 = V_1;
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task4_t003E8F0D5FB5B707AF8354726325D8C00D7C165C* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task4/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE196CC004A5D105E636406B83FC98CF574D25241 (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task4/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m2E2D4AA52F089E0BD63A0C14067D282CAB53530B (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m2E2D4AA52F089E0BD63A0C14067D282CAB53530B_RuntimeMethod_var)));
	}
}
// System.Object task4/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m2B920A2072A94FABED0614800B71C1232E07D828 (U3CstopwatchU3Ed__25_tE86D71012C77EE15B567D534E5A68B2EDF579581* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task16::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_Start_m317742F3C73D2B7672E7812FFCFCE805D1A2CC6C (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// podskaz3use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___podskaz3use_21;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task16::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task16_stopwatch_m2A2DB95C40486C258C29A239F25E038055F058F7 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* L_0 = (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__36__ctor_mC91A3522F3CCBEAA4251614DF2EC589EA21268DA(L_0, 0, NULL);
		U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task16::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_answer_m79F86560D6BDC6059C14154FE39871EDAA94560F (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "2")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_28;
		__this->___mistake_28 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_showhint1_mC4858AB59D368A069B52F03A0456BD97F0A4A301 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz1use_19;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_22;
		__this->___p1_22 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task16::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_notshowhint1_m955FDC707BCD8878FA96F119FD8CD63F13A5F3B4 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_showhint2_m802C46B34FA7C32AFADF71D073CEAC045EE2D837 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2use_20;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p2++;
		int32_t L_8 = __this->___p2_23;
		__this->___p2_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task16::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_notshowhint2_m707CA7778ADBBC07638B93B6272732970F4A5E7A (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::showhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_showhint3_m7CC7237E99791ADD407B8349E3316F9807BCBE3C (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3use_21;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_24;
		__this->___p3_24 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task16::notshowhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_notshowhint3_m734B422B1420EE174E2DA333D103F22C043896EF (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint3.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_notshowver_m8B274DFEC93ACA4E896B4A7D34E2886E3C059849 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_showstatistics_m8631ECBD50838FBDB934B13E98C78CAC08551DA5 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_25;
		int32_t* L_5 = (&__this->___p1_22);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_26;
		int32_t* L_8 = (&__this->___p2_23);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_27;
		int32_t* L_11 = (&__this->___p3_24);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___textoshibka_30;
		int32_t* L_14 = (&__this->___mistake_28);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___texttime_29;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		int32_t* L_19 = (&__this->___time_Ch_6);
		String_t* L_20;
		L_20 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_19, NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_20);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_18;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		int32_t* L_23 = (&__this->___time_M_5);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		int32_t* L_27 = (&__this->___time_S_4);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_28);
		String_t* L_29;
		L_29 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_26, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_29);
		// }
		return;
	}
}
// System.Void task16::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_notshowstatistics_m856B11F8CE23FD0E58CA027494D3C4653C006677 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::nextuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_nextuslzad_mA238B4AAA6E54F1CAF82372B790ECBD667579F3D (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_31;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// usl2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_32;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task16::backuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_backuslzad_mCBD3E2E86CD703E6018732874431C4FB29AFCE6B (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_31;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// usl2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_32;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task16::ForDropdown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16_ForDropdown_m32DAC402A1398DA24AF07E31CCE3AA8DDDCF9236 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i <= 2; i++)
		V_0 = 0;
		goto IL_003a;
	}

IL_0004:
	{
		// if ((dropdown1.value == i) && (dropdown2.value == i))
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___dropdown1_33;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_0, NULL);
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0036;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_3 = __this->___dropdown2_34;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_3, NULL);
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0036;
		}
	}
	{
		// TextCount.text = inputField.text;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___TextCount_35;
		InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_7 = __this->___inputField_36;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_7, NULL);
		NullCheck(L_6);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_8);
	}

IL_0036:
	{
		// for (int i = 0; i <= 2; i++)
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_003a:
	{
		// for (int i = 0; i <= 2; i++)
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) <= ((int32_t)2)))
		{
			goto IL_0004;
		}
	}
	{
		//     if (((dropdown1.value == 0) && (dropdown2.value == 1))
		// || ((dropdown1.value == 1) && (dropdown2.value == 2)))
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_11 = __this->___dropdown1_33;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_11, NULL);
		if (L_12)
		{
			goto IL_0059;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_13 = __this->___dropdown2_34;
		NullCheck(L_13);
		int32_t L_14;
		L_14 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_13, NULL);
		if ((((int32_t)L_14) == ((int32_t)1)))
		{
			goto IL_0075;
		}
	}

IL_0059:
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_15 = __this->___dropdown1_33;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_15, NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_00bf;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_17 = __this->___dropdown2_34;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_17, NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)2))))
		{
			goto IL_00bf;
		}
	}

IL_0075:
	{
		// countStr = inputField.text;
		InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_19 = __this->___inputField_36;
		NullCheck(L_19);
		String_t* L_20;
		L_20 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_19, NULL);
		__this->___countStr_37 = L_20;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_20);
		// count = Convert.ToSingle(countStr) / 60; //??????? ?????? ? ?????
		String_t* L_21 = __this->___countStr_37;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		float L_22;
		L_22 = Convert_ToSingle_m312FA1741E893E5B9D93A208CD15D417B1E4C65C(L_21, NULL);
		__this->___count_38 = ((float)(L_22/(60.0f)));
		// countStr = count.ToString();
		float* L_23 = (&__this->___count_38);
		String_t* L_24;
		L_24 = Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972(L_23, NULL);
		__this->___countStr_37 = L_24;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_24);
		// TextCount.text = countStr;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_25 = __this->___TextCount_35;
		String_t* L_26 = __this->___countStr_37;
		NullCheck(L_25);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_26);
	}

IL_00bf:
	{
		// if ((dropdown1.value == 0) && (dropdown2.value == 2))
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_27 = __this->___dropdown1_33;
		NullCheck(L_27);
		int32_t L_28;
		L_28 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_27, NULL);
		if (L_28)
		{
			goto IL_0124;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_29 = __this->___dropdown2_34;
		NullCheck(L_29);
		int32_t L_30;
		L_30 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_29, NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_0124;
		}
	}
	{
		// countStr = inputField.text;
		InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_31 = __this->___inputField_36;
		NullCheck(L_31);
		String_t* L_32;
		L_32 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_31, NULL);
		__this->___countStr_37 = L_32;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_32);
		// count = Convert.ToSingle(countStr) / 360; //??????? ?????? ? ?????
		String_t* L_33 = __this->___countStr_37;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		float L_34;
		L_34 = Convert_ToSingle_m312FA1741E893E5B9D93A208CD15D417B1E4C65C(L_33, NULL);
		__this->___count_38 = ((float)(L_34/(360.0f)));
		// countStr = count.ToString();
		float* L_35 = (&__this->___count_38);
		String_t* L_36;
		L_36 = Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972(L_35, NULL);
		__this->___countStr_37 = L_36;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_36);
		// TextCount.text = countStr;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_37 = __this->___TextCount_35;
		String_t* L_38 = __this->___countStr_37;
		NullCheck(L_37);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_38);
	}

IL_0124:
	{
		// if ((dropdown1.value == 2) && (dropdown2.value == 0))
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_39 = __this->___dropdown1_33;
		NullCheck(L_39);
		int32_t L_40;
		L_40 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_39, NULL);
		if ((!(((uint32_t)L_40) == ((uint32_t)2))))
		{
			goto IL_0189;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_41 = __this->___dropdown2_34;
		NullCheck(L_41);
		int32_t L_42;
		L_42 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_41, NULL);
		if (L_42)
		{
			goto IL_0189;
		}
	}
	{
		// countStr = inputField.text;
		InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_43 = __this->___inputField_36;
		NullCheck(L_43);
		String_t* L_44;
		L_44 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_43, NULL);
		__this->___countStr_37 = L_44;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_44);
		// count = Convert.ToSingle(countStr) * 360; //??????? ?????? ? ?????
		String_t* L_45 = __this->___countStr_37;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		float L_46;
		L_46 = Convert_ToSingle_m312FA1741E893E5B9D93A208CD15D417B1E4C65C(L_45, NULL);
		__this->___count_38 = ((float)il2cpp_codegen_multiply(L_46, (360.0f)));
		// countStr = count.ToString();
		float* L_47 = (&__this->___count_38);
		String_t* L_48;
		L_48 = Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972(L_47, NULL);
		__this->___countStr_37 = L_48;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_48);
		// TextCount.text = countStr;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_49 = __this->___TextCount_35;
		String_t* L_50 = __this->___countStr_37;
		NullCheck(L_49);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_49, L_50);
	}

IL_0189:
	{
		// if (((dropdown1.value == 2) && (dropdown2.value == 1))
		//     || ((dropdown1.value == 1) && (dropdown2.value == 0)))
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_51 = __this->___dropdown1_33;
		NullCheck(L_51);
		int32_t L_52;
		L_52 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_51, NULL);
		if ((!(((uint32_t)L_52) == ((uint32_t)2))))
		{
			goto IL_01a5;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_53 = __this->___dropdown2_34;
		NullCheck(L_53);
		int32_t L_54;
		L_54 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_53, NULL);
		if ((((int32_t)L_54) == ((int32_t)1)))
		{
			goto IL_01c0;
		}
	}

IL_01a5:
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_55 = __this->___dropdown1_33;
		NullCheck(L_55);
		int32_t L_56;
		L_56 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_55, NULL);
		if ((!(((uint32_t)L_56) == ((uint32_t)1))))
		{
			goto IL_020a;
		}
	}
	{
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_57 = __this->___dropdown2_34;
		NullCheck(L_57);
		int32_t L_58;
		L_58 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_57, NULL);
		if (L_58)
		{
			goto IL_020a;
		}
	}

IL_01c0:
	{
		// countStr = inputField.text;
		InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_59 = __this->___inputField_36;
		NullCheck(L_59);
		String_t* L_60;
		L_60 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_59, NULL);
		__this->___countStr_37 = L_60;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_60);
		// count = Convert.ToSingle(countStr) * 60; //??????? ?????? ? ?????
		String_t* L_61 = __this->___countStr_37;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		float L_62;
		L_62 = Convert_ToSingle_m312FA1741E893E5B9D93A208CD15D417B1E4C65C(L_61, NULL);
		__this->___count_38 = ((float)il2cpp_codegen_multiply(L_62, (60.0f)));
		// countStr = count.ToString();
		float* L_63 = (&__this->___count_38);
		String_t* L_64;
		L_64 = Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972(L_63, NULL);
		__this->___countStr_37 = L_64;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___countStr_37), (void*)L_64);
		// TextCount.text = countStr;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_65 = __this->___TextCount_35;
		String_t* L_66 = __this->___countStr_37;
		NullCheck(L_65);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_65, L_66);
	}

IL_020a:
	{
		// }
		return;
	}
}
// System.Void task16::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task16__ctor_mBABF3C4619737B84F809810132CD363326569D31 (task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task16/<stopwatch>d__36::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__36__ctor_mC91A3522F3CCBEAA4251614DF2EC589EA21268DA (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task16/<stopwatch>d__36::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__36_System_IDisposable_Dispose_mC56EACFB8272315F17AEC51D52E44D929EBDBDB9 (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task16/<stopwatch>d__36::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__36_MoveNext_m8FA85B029768A5A1E1E96A348237F403EB9CCDEA (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_5 = V_1;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_11 = V_1;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_17 = V_1;
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task16_t56A0437BE43073063DF2885A7A931FFF46DD8331* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task16/<stopwatch>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF8028E7E338FB86B13B67CFB381A07ACB18A1F9 (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task16/<stopwatch>d__36::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__36_System_Collections_IEnumerator_Reset_m7DD2F20B5E3227FC44D10C623A4A8AD250F13DD4 (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__36_System_Collections_IEnumerator_Reset_m7DD2F20B5E3227FC44D10C623A4A8AD250F13DD4_RuntimeMethod_var)));
	}
}
// System.Object task16/<stopwatch>d__36::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__36_System_Collections_IEnumerator_get_Current_mD652CCD1701967AA887203CE9237F53EA0966A08 (U3CstopwatchU3Ed__36_t149158A8DCA878B98042FE9503F54D4F2C1EE114* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task9::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_Start_m1346924DC336947D32338D7C98AA89A90E3F34B8 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// podskaz3use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___podskaz3use_21;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task9::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task9_stopwatch_mA46BEEFEE49A0E9B29FD7D2AB9077C88D398B66F (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* L_0 = (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__28__ctor_mAF135CD78100517B8A047C1CC0CC7FBF7D721E99(L_0, 0, NULL);
		U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task9::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_answer_mC8C3825F296DACFD1F437C2309A82B84D1911DF7 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94AA14A52763F3CD8FFB480C9EF5EBD529D39C24);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "28")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral94AA14A52763F3CD8FFB480C9EF5EBD529D39C24, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_28;
		__this->___mistake_28 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task9::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_showhint1_m01431923ED781182C2741837D584361D4E29A334 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_17;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_22;
		__this->___p1_22 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task9::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_notshowhint1_mFF25165362A6516466316A300C365C877344C096 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task9::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_showhint2_m449E4E5B405A3E4FF96D815D751923D6E396725E (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_20;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz3_18;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p2++;
		int32_t L_8 = __this->___p2_23;
		__this->___p2_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task9::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_notshowhint2_m2000D9FE732292BD834BF8FFF15B49EB5B9633DF (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task9::showhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_showhint3_mA5B2DDBF1A42E6ACB46DA5ABD453FCB37282D4D1 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3use_21;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_24;
		__this->___p3_24 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task9::notshowhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_notshowhint3_m19EAC40B6306519BA8B5631DE19A622C89A077F2 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint3.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task9::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_notshowver_m645F024609592C8484F4C96ED7DEE30B83B58F0F (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task9::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_showstatistics_m0124FBE9B774C311F79DDEE10E77438D74D2B21F (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_25;
		int32_t* L_5 = (&__this->___p1_22);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_26;
		int32_t* L_8 = (&__this->___p2_23);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_27;
		int32_t* L_11 = (&__this->___p3_24);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___textoshibka_30;
		int32_t* L_14 = (&__this->___mistake_28);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___texttime_29;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		int32_t* L_19 = (&__this->___time_Ch_6);
		String_t* L_20;
		L_20 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_19, NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_20);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_18;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		int32_t* L_23 = (&__this->___time_M_5);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		int32_t* L_27 = (&__this->___time_S_4);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_28);
		String_t* L_29;
		L_29 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_26, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_29);
		// }
		return;
	}
}
// System.Void task9::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9_notshowstatistics_mBC94AB312464C30BC7CC9A23ED041EC8F84046A7 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task9::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task9__ctor_mA1FBA6261F9771C8D715EAD93286601F4A15E812 (task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task9/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_mAF135CD78100517B8A047C1CC0CC7FBF7D721E99 (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task9/<stopwatch>d__28::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_m968F8B16096B885F620B0C59DD55845E2A2C4DC5 (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task9/<stopwatch>d__28::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__28_MoveNext_m100E4BFAD41A0C25C2FBB6B9B85E4F97FCD56F1A (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_5 = V_1;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_11 = V_1;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_17 = V_1;
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task9_tC73010C90655823D17F5BABBF0D4C0751EF88867* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task9/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0FB3F188DE6B33A3AA00CEB99D557C86D745987 (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task9/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m0EABDE0C4FBC3676A84E8E420FF9CBE70274ED20 (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m0EABDE0C4FBC3676A84E8E420FF9CBE70274ED20_RuntimeMethod_var)));
	}
}
// System.Object task9/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_m471CA932302E91AA3E2CA207064D99F4926A2BE3 (U3CstopwatchU3Ed__28_t6F8C3FC637BB29B91FC2070E7C89054567A59EC9* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task12::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_Start_m12F3B82FC31A306AA7F2AEFDB1A73DF38C645801 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task12::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task12_stopwatch_m97E6C7D2705FF64D3031D1398AC66FD15D2EBCBA (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* L_0 = (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__29__ctor_m1B84C54EC9B518E9CECE83E837776F3276385838(L_0, 0, NULL);
		U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task12::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_answer_m8F3E12E11B29812C5C337A5370A521A0EF646172 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3960B836F80A1121745A7FF318E16CA0377383D6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "360")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral3960B836F80A1121745A7FF318E16CA0377383D6, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_10;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_12;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_27;
		__this->___mistake_27 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_10;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_11;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_12;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_showhint1_mFE34419C0088A5F8349A00AF44E44F197291A4A8 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_18;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p2++;
		int32_t L_8 = __this->___p2_22;
		__this->___p2_22 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task12::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_notshowhint1_m91EC270DCE1562236BF9966E0A46CB7A560098ED (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_showhint2_m32F9D47FA88DB394A6C5D7BC0D2D2EE7F81C4FCC (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___podskaz2use_20;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___hint2_16;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_23;
		__this->___p3_23 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task12::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_notshowhint2_mECD99D666EE7C93E16F065B63A0C119804C51112 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_notshowver_m744582D246AEFE64474182F8DCC05DF12002C5ED (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_showstatistics_m70353CAC3CD7F6C59A2C61CAA2673C4438E47C8F (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_24;
		int32_t* L_5 = (&__this->___p1_21);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_25;
		int32_t* L_8 = (&__this->___p2_22);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_26;
		int32_t* L_11 = (&__this->___p3_23);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___textoshibka_29;
		int32_t* L_14 = (&__this->___mistake_27);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___texttime_28;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		int32_t* L_19 = (&__this->___time_Ch_6);
		String_t* L_20;
		L_20 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_19, NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_20);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_18;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		int32_t* L_23 = (&__this->___time_M_5);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		int32_t* L_27 = (&__this->___time_S_4);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_28);
		String_t* L_29;
		L_29 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_26, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_29);
		// }
		return;
	}
}
// System.Void task12::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_notshowstatistics_m0D59F1CABFECCAD8913D4B5125C3FBCF85C1547D (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_showar_m48FF7FFAF93DDFA744ABAC9B1EDCC34A003E0FEF (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_21;
		__this->___p1_21 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task12::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_notshowar_mA63527CEC7A87103E5554320AF1D6EAD1DEF4C6C (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::nextuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_nextuslzad_mD83718B7F670D3225A5DDEDA82AB7348FC08AC51 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_30;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// usl2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_31;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task12::backuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12_backuslzad_mCF42045F3BE86A0B8244A5B2ABDD33F554D9EF65 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_30;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// usl2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_31;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task12::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task12__ctor_m4EF394A45514CA1BEF91BA0D200F1685DBF4C442 (task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task12/<stopwatch>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__29__ctor_m1B84C54EC9B518E9CECE83E837776F3276385838 (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task12/<stopwatch>d__29::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__29_System_IDisposable_Dispose_m9D3E8528D9D30B0C5D2E52E3C4DBE0D9E1CEFB71 (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task12/<stopwatch>d__29::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__29_MoveNext_m343E5682D5AB7AF38E83737EF657C6D6D5500F87 (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_5 = V_1;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_11 = V_1;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_17 = V_1;
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task12_tE53DA10C5D71F49AB45ADA083F2B98D996A7F330* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task12/<stopwatch>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m972587A5720ABA5F6F3CA4A8EC5CA5508561F00C (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task12/<stopwatch>d__29::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__29_System_Collections_IEnumerator_Reset_m1E65CC4AC36D6F6B3E4A565402C9059B28CFCC56 (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__29_System_Collections_IEnumerator_Reset_m1E65CC4AC36D6F6B3E4A565402C9059B28CFCC56_RuntimeMethod_var)));
	}
}
// System.Object task12/<stopwatch>d__29::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__29_System_Collections_IEnumerator_get_Current_m1E936383D0787D7C393CC2B8F592717EB3257F92 (U3CstopwatchU3Ed__29_t840E22D4F2CFD21A9ED3D92850720A2565100484* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void perehodmodul2::Show_Z1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Z1_m48D40224BA4C5FFBEB8BCC6062679C9E22DC2C96 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7643D699232FD7DBE557F071B1A3A37AEDD1E2DE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Communication_on_the_Internet");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral7643D699232FD7DBE557F071B1A3A37AEDD1E2DE, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_Z2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Z2_mA53C0CDBCCA4A674440C56A563C6D35392322AF9 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF684003298C71D4D13496BF2119604A329CDC4B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Communication_on_the_Internet2");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralAF684003298C71D4D13496BF2119604A329CDC4B, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_Z3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Z3_m585561BFF2A2B80C5360620B91BDD8EC373DC644 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral83A08B95DA45FB82D73D93620C3D06753D73B05E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Cyclist_Elena");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral83A08B95DA45FB82D73D93620C3D06753D73B05E, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_Z4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Z4_mE475D1889E226DFE1FDEDFCA53CCFE17E991EA65 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F0E69BF00A53CC4161A360099669FB996FD6447);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Drop_rate");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral8F0E69BF00A53CC4161A360099669FB996FD6447, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_Z5()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Z5_m09074BC5D8C36079E0D17A6A37FAAAA682816779 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral92F8F26D4A784498D26E165EAA2AF96F6ED97F5E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Cyclist_Elena1");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral92F8F26D4A784498D26E165EAA2AF96F6ED97F5E, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_Z6()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Z6_mBB8C9A35428D6A927664AE0F3E7E1305DCCFCB87 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2B0579ACFE6DC9DC857FCADED1DDADDC6CBBB23D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Apple_trees");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral2B0579ACFE6DC9DC857FCADED1DDADDC6CBBB23D, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_Module()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_Module_m77AACA54583ED62737DD681DEDCC5C16824B2355 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("modules");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40, NULL);
		// r = false;
		il2cpp_codegen_runtime_class_init_inline(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var);
		((perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var))->___r_4 = (bool)0;
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_MainMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_MainMenu_mFEDE69C68D3917E0F1FD7BD0D961D43008C62E44 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("MainMenu");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::Show_zadmodule()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2_Show_zadmodule_m21211DBCD5BBB387F31F7822321244F070574968 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F09A47709D08F77E21625A3482A3A0A98579DE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_2");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral6F09A47709D08F77E21625A3482A3A0A98579DE1, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2__ctor_mDAA3949A688F12FF38836395463D1D86410FD643 (perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void perehodmodul2::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul2__cctor_m9D0CBAEEA949EF4F49098C09B97F693E6337D8BC (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static bool r = true;
		((perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var))->___r_4 = (bool)1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void moduli::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_Start_mFE89B051CA2549C6BE4EDCDB82D950A04D3D1231 (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// k = true;
		__this->___k_7 = (bool)1;
		// if (k == true)
		bool L_0 = __this->___k_7;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		// info.gameObject.SetActive(k);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___info_4;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		bool L_3 = __this->___k_7;
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, L_3, NULL);
	}

IL_0025:
	{
		// if (k == false)
		bool L_4 = __this->___k_7;
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		// info.gameObject.SetActive(k);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___info_4;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		bool L_7 = __this->___k_7;
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, L_7, NULL);
	}

IL_0043:
	{
		// if (perehodmodul1.r == false)
		il2cpp_codegen_runtime_class_init_inline(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var);
		bool L_8 = ((perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var))->___r_4;
		if (L_8)
		{
			goto IL_005f;
		}
	}
	{
		// info.gameObject.SetActive(perehodmodul1.r);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___info_4;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		il2cpp_codegen_runtime_class_init_inline(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var);
		bool L_11 = ((perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var))->___r_4;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, L_11, NULL);
	}

IL_005f:
	{
		// if (perehodmodul2.r == false)
		il2cpp_codegen_runtime_class_init_inline(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var);
		bool L_12 = ((perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var))->___r_4;
		if (L_12)
		{
			goto IL_007b;
		}
	}
	{
		// info.gameObject.SetActive(perehodmodul2.r);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = __this->___info_4;
		NullCheck(L_13);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14;
		L_14 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_13, NULL);
		il2cpp_codegen_runtime_class_init_inline(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var);
		bool L_15 = ((perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul2_tF21708173BB1C2D6E1C17FA4EF6C274F3F2353CA_il2cpp_TypeInfo_var))->___r_4;
		NullCheck(L_14);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_14, L_15, NULL);
	}

IL_007b:
	{
		// if (perehodmodul3.r == false)
		il2cpp_codegen_runtime_class_init_inline(perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_il2cpp_TypeInfo_var);
		bool L_16 = ((perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_il2cpp_TypeInfo_var))->___r_4;
		if (L_16)
		{
			goto IL_0097;
		}
	}
	{
		// info.gameObject.SetActive(perehodmodul3.r);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_17 = __this->___info_4;
		NullCheck(L_17);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
		L_18 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_il2cpp_TypeInfo_var);
		bool L_19 = ((perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul3_t2647A068AF1AEABB7FC8521CB5DF98C4E015A9DB_il2cpp_TypeInfo_var))->___r_4;
		NullCheck(L_18);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_18, L_19, NULL);
	}

IL_0097:
	{
		// if (perehodmodul4.r == false)
		il2cpp_codegen_runtime_class_init_inline(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var);
		bool L_20 = ((perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var))->___r_4;
		if (L_20)
		{
			goto IL_00b3;
		}
	}
	{
		// info.gameObject.SetActive(perehodmodul4.r);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_21 = __this->___info_4;
		NullCheck(L_21);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
		L_22 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_21, NULL);
		il2cpp_codegen_runtime_class_init_inline(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var);
		bool L_23 = ((perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var))->___r_4;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, L_23, NULL);
	}

IL_00b3:
	{
		// }
		return;
	}
}
// System.Void moduli::Show_Module()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_Show_Module_m2559DDF768ECA4FF64E654545F2970D4E5AE53DF (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("modules");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40, NULL);
		// }
		return;
	}
}
// System.Void moduli::Show_Module1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_Show_Module1_m33831E350B0B6FF193AEA728A1E6E373B9FE3533 (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC44C020D752222A424F7F9E300FD9D237C95A6CE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_1");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralC44C020D752222A424F7F9E300FD9D237C95A6CE, NULL);
		// }
		return;
	}
}
// System.Void moduli::Show_Module2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_Show_Module2_m0476848E7788E818995ADCFCCE53652508D3E04E (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F09A47709D08F77E21625A3482A3A0A98579DE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_2");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral6F09A47709D08F77E21625A3482A3A0A98579DE1, NULL);
		// }
		return;
	}
}
// System.Void moduli::Show_Module3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_Show_Module3_m93A2948617CBADDF29C79648DD0F47AA6D86989C (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5479C1C5ECE0533E4A55EE2AF88A08A9E7D1DC56);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_3");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral5479C1C5ECE0533E4A55EE2AF88A08A9E7D1DC56, NULL);
		// }
		return;
	}
}
// System.Void moduli::Show_Module4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_Show_Module4_mB7E3F120B9710790D05C76821ACDB919EF04E9B8 (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F373A07613B0985D213534426C8F7DCE864244);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_4");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral74F373A07613B0985D213534426C8F7DCE864244, NULL);
		// }
		return;
	}
}
// System.Void moduli::showhinfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_showhinfo_mEB85AB45CF79708B67A5403FEEA502CA2B2577E6 (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	{
		// info.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___info_4;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// k = true;
		__this->___k_7 = (bool)1;
		// }
		return;
	}
}
// System.Void moduli::notshowhinfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli_notshowhinfo_mA83845B83F2C39837D655F83D51F5183AC4EE01D (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	{
		// info.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___info_4;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// k = false;
		__this->___k_7 = (bool)0;
		// }
		return;
	}
}
// System.Void moduli::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void moduli__ctor_m3308BDC509003DF6019B20F9C617D84439CC2F2C (moduli_t670C61045602DBFACFB617E31D5F2E8E636FD955* __this, const RuntimeMethod* method) 
{
	{
		// bool textNotDisplayed = true;
		__this->___textNotDisplayed_5 = (bool)1;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void perehodMain::Show_begin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodMain_Show_begin_m28585DB2D7EDFFC5976D3062CB260D868CFC515F (perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("modules");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40, NULL);
		// }
		return;
	}
}
// System.Void perehodMain::Show_avtor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodMain_Show_avtor_mF2DF10E84825192A855847EF76370B3E504F7B99 (perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC12E4A2478C8542173734DE349D50AF90C34898B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Author");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralC12E4A2478C8542173734DE349D50AF90C34898B, NULL);
		// }
		return;
	}
}
// System.Void perehodMain::Vs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodMain_Vs_m51038803483A3CA815C57310A8F450888AE632D2 (perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088* __this, const RuntimeMethod* method) 
{
	{
		// Application.Quit();
		Application_Quit_mE304382DB9A6455C2A474C8F364C7387F37E9281(NULL);
		// }
		return;
	}
}
// System.Void perehodMain::Show_MainMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodMain_Show_MainMenu_mA1217E463C0A60282CD5F8C7103311A27D6708D1 (perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("MainMenu");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719, NULL);
		// }
		return;
	}
}
// System.Void perehodMain::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodMain__ctor_m78B2C53711CF4CDE5E6F9607D93FC6C3E2E54B4C (perehodMain_t7C443097E0451DA1809D7844271AD60CAE3FA088* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task21::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_Start_mC02A24F02E0C44F76E1C6088425529C956755DD2 (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_15;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task21::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task21_stopwatch_mD216EDD0C1860FF19A24B72F9B5865BE6722F35D (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* L_0 = (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__18__ctor_mC5DF3C8476F781C48C8C06C1F8FE1C134F0AD286(L_0, 0, NULL);
		U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task21::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_answer_m771D30DFF6B2BC45FBC0F08105BF1D2208EC0F17 (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "5")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_18;
		__this->___mistake_18 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task21::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_showhint1_m0F0E10E03EFB07A91E47C98529C41CC2950B3EC3 (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_15;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_16;
		__this->___p1_16 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task21::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_notshowhint1_m3F620BE3D3479594B9700046048931D66529BC7B (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task21::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_notshowver_mF5208B348997E2A48802E7244FA4E4FF5421C807 (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task21::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_showstatistics_m4FD072E97700A8CD5F4F94DCD4EFE75ACAC19C3D (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_17;
		int32_t* L_5 = (&__this->___p1_16);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___textoshibka_20;
		int32_t* L_8 = (&__this->___mistake_18);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___texttime_19;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		int32_t* L_13 = (&__this->___time_Ch_6);
		String_t* L_14;
		L_14 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_13, NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_14);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_12;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = L_15;
		int32_t* L_17 = (&__this->___time_M_5);
		String_t* L_18;
		L_18 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_17, NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_18);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_19;
		int32_t* L_21 = (&__this->___time_S_4);
		String_t* L_22;
		L_22 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_21, NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_22);
		String_t* L_23;
		L_23 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_20, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_23);
		// }
		return;
	}
}
// System.Void task21::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21_notshowstatistics_m208D024064A6A5A81E324CB076891F576069BD8D (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task21::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task21__ctor_m8D95BED48C1F0816EB383E854D075070F1EEF41F (task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task21/<stopwatch>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18__ctor_mC5DF3C8476F781C48C8C06C1F8FE1C134F0AD286 (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task21/<stopwatch>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18_System_IDisposable_Dispose_m0267A6292474A75D1A4D0E2C18AA5AE28A9B4A73 (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task21/<stopwatch>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__18_MoveNext_mD4F5172969BAEA7BC62C0D21BC583263D6F81538 (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_5 = V_1;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_11 = V_1;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_17 = V_1;
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task21_tC4A8EE0A9B52EAA3DE408B3B3778F24FEAA093C3* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task21/<stopwatch>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC82D427C0767A5292B87960F2E0ADA4B0C5EFB3 (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task21/<stopwatch>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m0282CAB192F651508DDBBFFA11D8BD5B9B2FCBA7 (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m0282CAB192F651508DDBBFFA11D8BD5B9B2FCBA7_RuntimeMethod_var)));
	}
}
// System.Object task21/<stopwatch>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__18_System_Collections_IEnumerator_get_Current_mA337C9225455F73B492AC4001B77EA49CD7DE6AD (U3CstopwatchU3Ed__18_t53188EF738953CBEF184CB2DD40E7BCA0A927377* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task7::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_Start_m8ACF65834D00626B9F816B24C3D586DA40D5C925 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_17;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_18;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task7::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task7_stopwatch_m2F4090704D490696CD3566C8DEDBF59931378068 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* L_0 = (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__23__ctor_m1665459298850446F9749CA3A9D77E111D5C1874(L_0, 0, NULL);
		U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task7::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_answer_mA837318B090BD474226E8228CCD10BA282AE3855 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "6")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_23;
		__this->___mistake_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task7::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_showhint1_m1C967BBF52730364ECFDC5324A10475894C2A8E4 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2_16;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz1use_17;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_19;
		__this->___p1_19 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task7::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_notshowhint1_mCA2C39E5BEFDF259B03AB5D482331285E5CD7024 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task7::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_showhint2_mA5567332F08A29B24C5423DE128A05FACAEDD739 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_20;
		__this->___p2_20 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task7::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_notshowhint2_mEB289CA39A752CF12D675D206795B8004EC53F65 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task7::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_notshowver_m36B5C6076250C06796C6119993FDAEBC2CF6B2D6 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task7::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_showstatistics_m4881D345BF29881F9A071B58C008831F140E9C06 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_21;
		int32_t* L_5 = (&__this->___p1_19);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_22;
		int32_t* L_8 = (&__this->___p2_20);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_25;
		int32_t* L_11 = (&__this->___mistake_23);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_24;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task7::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7_notshowstatistics_mF9E2E334A2DECF943F03505CCB1C315B3C32BDCB (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task7::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task7__ctor_mCD393C645BEC915987B1302E147A69FD0F186F68 (task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task7/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m1665459298850446F9749CA3A9D77E111D5C1874 (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task7/<stopwatch>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_mA4BFB0E57050964E1A5B3804022C6012B1279CED (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task7/<stopwatch>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__23_MoveNext_m594BDE2FC7552239BD3C27C7D8AAB496FCEFE196 (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_5 = V_1;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_11 = V_1;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_17 = V_1;
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task7_t2D231AE0C3CDADCA54D37BE33A605480B0C2FE67* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task7/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7416789C781BCA7777273FBAB6AB3C544737DD2A (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task7/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_mBC582C5957C3035D3103ECE4C8A6EA16D5FDBB30 (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_mBC582C5957C3035D3103ECE4C8A6EA16D5FDBB30_RuntimeMethod_var)));
	}
}
// System.Object task7/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_m521B0BF4CF9B3D14C05C896005F3AFC5A6DEB90E (U3CstopwatchU3Ed__23_t0D2F510FEACEC561BD8156AED9427D65010857CA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task24::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_Start_m3A79A1745E191FF4A5BD6BEE61F643A73EBCE23E (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_17;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_18;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task24::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task24_stopwatch_m8AB0569BCD18CFB04A79F428EC61DEDDDC71EEB5 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* L_0 = (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__23__ctor_m4967F2F96916BB941EABA3D4B61599D03E007EC6(L_0, 0, NULL);
		U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task24::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_answer_mC791A3F462466B4A712A2C0A706FABBF3674BF3F (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA6A6AB007C12E7673D8479299FDC94A4D003EBF9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "40")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralA6A6AB007C12E7673D8479299FDC94A4D003EBF9, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_23;
		__this->___mistake_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task24::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_showhint1_m2F430E83330DFDF98A174B574C559036DF9B22D8 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2_16;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz1use_17;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_19;
		__this->___p1_19 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task24::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_notshowhint1_m6CC526316584A94F1E3CF01FDC3AD0019235E116 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task24::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_showhint2_mFC66D2C7E43B55789664CC9632FE0AA516A9F3EB (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_20;
		__this->___p2_20 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task24::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_notshowhint2_mEEF8EC41480EA2E5A9D32388686C45BD0970EBA1 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task24::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_notshowver_mA7CE98DA19B53B118C77107435EF049211ECB722 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task24::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_showstatistics_m0AC4C2A771E18B45491EFFF12B86B198E839DF25 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_21;
		int32_t* L_5 = (&__this->___p1_19);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_22;
		int32_t* L_8 = (&__this->___p2_20);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_25;
		int32_t* L_11 = (&__this->___mistake_23);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_24;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task24::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24_notshowstatistics_m40B2C82F3BE67D116AA4AC90BAC0114234D05287 (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task24::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task24__ctor_m71840F19BCED7C1106B466FBC7E3104C72155C3F (task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task24/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m4967F2F96916BB941EABA3D4B61599D03E007EC6 (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task24/<stopwatch>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_m75ED6D4AD626186ADEE51E5878C872341053ED83 (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task24/<stopwatch>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__23_MoveNext_mC9A000339EC7785AE48935C671A1524C59E071D9 (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_5 = V_1;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_11 = V_1;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_17 = V_1;
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task24_t989076BA57BBBCE08F5CDAE743E7881E5065872C* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task24/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE4984AA6593344450A997E75276209C91B110D (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task24/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m87A3860173B5A6BF3C96B25BF9FD502FBC02953F (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m87A3860173B5A6BF3C96B25BF9FD502FBC02953F_RuntimeMethod_var)));
	}
}
// System.Object task24/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_m3FF3FFB2AD9B752EF58B3E26095694374026412A (U3CstopwatchU3Ed__23_t82F3FB669AF7A2BF76E97F5F989E834DDD076B7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task11::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_Start_m1DA2920E46B912D2F00ABFD951E55C27BF151997 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// podskaz3use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___podskaz3use_21;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task11::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task11_stopwatch_mD52F69B056E4BF9C01104BB9249D0F65ED326313 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* L_0 = (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__28__ctor_mAD20EA5F58F53CE11B28B998BAF88A97D3995725(L_0, 0, NULL);
		U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task11::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_answer_mEF69DA0579065EE2C880FD31FAFDA5CE98CD9F5A (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBA6489F9FF78EA53C6B7657F31F26E6124C0AC1D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "282")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralBA6489F9FF78EA53C6B7657F31F26E6124C0AC1D, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_28;
		__this->___mistake_28 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task11::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_showhint1_m06D8BA120D6B837BBA7D438EAD8ADB8635114660 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz1use_19;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_22;
		__this->___p1_22 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task11::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_notshowhint1_mADCC045880BBBA3F4E47DFD3396AF4E59A7BF2C2 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task11::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_showhint2_mB91B9420F48ABABE155753BECE6B7AE76DE18F47 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2use_20;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p2++;
		int32_t L_8 = __this->___p2_23;
		__this->___p2_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task11::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_notshowhint2_m94C3C451FA6BA877AACBEEBCF7166F686B00280F (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task11::showhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_showhint3_m52D5C3B1B30FE5EAAB050A356977E03431A87930 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3use_21;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_24;
		__this->___p3_24 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task11::notshowhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_notshowhint3_m7B23D99A26F698E790F35C6825AAA032914DE195 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint3.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task11::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_notshowver_m83CFDECF9E1ECB2C1C8924FC52B7211FA9672A75 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task11::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_showstatistics_m30BFA103663833A6541D89EE8E9D6B3EB2144594 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_25;
		int32_t* L_5 = (&__this->___p1_22);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_26;
		int32_t* L_8 = (&__this->___p2_23);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_27;
		int32_t* L_11 = (&__this->___p3_24);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___textoshibka_30;
		int32_t* L_14 = (&__this->___mistake_28);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___texttime_29;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		int32_t* L_19 = (&__this->___time_Ch_6);
		String_t* L_20;
		L_20 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_19, NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_20);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_18;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		int32_t* L_23 = (&__this->___time_M_5);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		int32_t* L_27 = (&__this->___time_S_4);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_28);
		String_t* L_29;
		L_29 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_26, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_29);
		// }
		return;
	}
}
// System.Void task11::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11_notshowstatistics_m70A822C5FDC7B75E13C5AD0FE964ABF3020CAD07 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task11::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task11__ctor_m1A154E4B49711116A93674070D463B30A6A5E3B6 (task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task11/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_mAD20EA5F58F53CE11B28B998BAF88A97D3995725 (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task11/<stopwatch>d__28::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mDFC70DC1B153CC1D99BAC77E5A8BFF8C41102385 (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task11/<stopwatch>d__28::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__28_MoveNext_m9C0CE3864B86C47D4209A7F6E4BB3E2DFCACA074 (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_5 = V_1;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_11 = V_1;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_17 = V_1;
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task11_tD2F7BE8F29084D5656ACC225FC3A3C0BDB0347E4* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task11/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE24B00F4E674A2007FD5822A53FF45A4D1B8CB26 (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task11/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mF94B5912F0DE9CE7169D2D881F14DE8DAB775B8D (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mF94B5912F0DE9CE7169D2D881F14DE8DAB775B8D_RuntimeMethod_var)));
	}
}
// System.Object task11/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mE753FB07CB7582FE9A0DB6C25BCD75CB0FF0B069 (U3CstopwatchU3Ed__28_t401C389B19CF3A5A868FB81FBD59C0CEB6ED71CE* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void perehodmodul4::Show_Z1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Z1_m1108703CB2B696AD0B8F5EAAF51CCC7565E63031 (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral908087AF1B00E5A6097DB84E1CA2DD8DDD7EE064);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Choose");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral908087AF1B00E5A6097DB84E1CA2DD8DDD7EE064, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_Z2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Z2_m625B2F863DFB930EED5D6F73E4FF4A663D936C1D (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral93DD1EB39AAB8608AB51651588484F9FDBA9932F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Cyclists");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral93DD1EB39AAB8608AB51651588484F9FDBA9932F, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_Z3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Z3_mAA58DE4A85D889C0E2A9C4A8FB94C4932A47239A (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral57C4A3908A7E58343F767097E3A76C282D0888F9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Step_model");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral57C4A3908A7E58343F767097E3A76C282D0888F9, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_Z4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Z4_m1E4B23FFBF319A6607ECD09994B502F35B223EEE (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD4E05ABC1E12488C6A70B26CAD1AEFE636D14B23);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Bookshelves");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralD4E05ABC1E12488C6A70B26CAD1AEFE636D14B23, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_Z5()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Z5_m06B299B930C53B3FA03E50B74BCC3CAFD049EF3F (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral074DECB0B6A85A8FE24AB2C92BF43E49056C81C8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Revolving_door");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral074DECB0B6A85A8FE24AB2C92BF43E49056C81C8, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_Z6()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Z6_m9B7DD3E4612274933461889E12EC2A2BDBB97C4E (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C3E1FD83C61F9FB95D4043B9CFE9517650E0CC4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Climbing_Mount_Fuji");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral4C3E1FD83C61F9FB95D4043B9CFE9517650E0CC4, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_Module()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_Module_m64FD8ED53471864AEAF5D5D531D18C8478332256 (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("modules");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40, NULL);
		// r = false;
		il2cpp_codegen_runtime_class_init_inline(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var);
		((perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var))->___r_4 = (bool)0;
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_MainMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_MainMenu_mC4875713425DAE3AC1D5E93C5994C2BC2EF25C64 (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("MainMenu");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::Show_zadmodule()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4_Show_zadmodule_m2B7D3D28C0CB69338EC915C3D84AEA7D8AB39F7A (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F373A07613B0985D213534426C8F7DCE864244);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_4");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral74F373A07613B0985D213534426C8F7DCE864244, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul4::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4__ctor_m1937A637E8EF20C5D111A1BC82CDD8C5F66CDC81 (perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void perehodmodul4::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul4__cctor_m31086EB0D7B9B181FBDD1A450FCF9F290B013E32 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static bool r = true;
		((perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul4_t3C514086AF9181D573612E0115B135F1027E98DA_il2cpp_TypeInfo_var))->___r_4 = (bool)1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task23::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_Start_m3DF5B0089FA92D710D4859DD099911F9FE8DE92B (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Textpodskaz.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___Textpodskaz_18;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// People.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___People_23;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hints_people.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hints_people_25;
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// hints_rotate.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___hints_rotate_26;
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// count_activePeople = 0;
		__this->___count_activePeople_27 = 0;
		// count_activeRotate = 0;
		__this->___count_activeRotate_28 = 0;
		// tog_people_active = false;
		__this->___tog_people_active_21 = (bool)0;
		// tog_rotate_active = false;
		__this->___tog_rotate_active_22 = (bool)0;
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_4;
		L_4 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task23::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task23_stopwatch_mE975F1CE89B92638B3883B1106EA88175E41D1FB (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* L_0 = (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__28__ctor_m20ACD953B1BBACC38DDA747FCE0ED1E7EF173B7A(L_0, 0, NULL);
		U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Collections.IEnumerator task23::VisibleRotate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task23_VisibleRotate_m907DCE84B6FFD087E73261817722805381B71114 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* L_0 = (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1*)il2cpp_codegen_object_new(U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CVisibleRotateU3Ed__29__ctor_m32FBBD56576C854E4E4A54DF1CB66A92DC28C367(L_0, 0, NULL);
		U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task23::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_answer_m5FB41B7B4530BFC677F5DD6D03DA7DD661754CD1 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral04E9EBE1F00A77E549A2B09F449656118F2B3197);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "720")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral04E9EBE1F00A77E549A2B09F449656118F2B3197, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_10;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_12;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_15;
		__this->___mistake_15 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_10;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_11;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_12;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task23::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_notshowver_m25BDB45D4AE3239F8D755DE971AC2CD1AF2C9BA5 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task23::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_showstatistics_m62AD2C5237C18A30607E97E3C6124A4764D90A90 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// text_peo.text = count_activePeople.ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_4 = __this->___text_peo_29;
		int32_t* L_5 = (&__this->___count_activePeople_27);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_6);
		// text_rot.text = count_activeRotate.ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_7 = __this->___text_rot_30;
		int32_t* L_8 = (&__this->___count_activeRotate_28);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_10 = __this->___textoshibka_17;
		int32_t* L_11 = (&__this->___mistake_15);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_13 = __this->___texttime_16;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task23::showhint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_showhint_mF6F9DE22D73ADDCC40A91952DB30759B14D13E5E (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	{
		// Textpodskaz.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___Textpodskaz_18;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task23::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_notshowstatistics_m4F8098A3E10F4CA1AFDE7CF9095F8F531D6FBF35 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task23::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_showar_m9A2C0877643E12C6F011B32CFDFE9C69905638AF (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task23::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_notshowar_mAF38B64C8AABFCF744047C33D0C02D1B759045A5 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task23::tog_activePeople(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_tog_activePeople_m2906252B9A3AA6BD326FAC30CA8180DBAE28B955 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, bool ___is_On0, const RuntimeMethod* method) 
{
	{
		// if (Textpodskaz.activeSelf == true)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___Textpodskaz_18;
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368(L_0, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// if (is_On)
		bool L_2 = ___is_On0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// tog_people_active = true;
		__this->___tog_people_active_21 = (bool)1;
		return;
	}

IL_0018:
	{
		// tog_people_active = false;
		__this->___tog_people_active_21 = (bool)0;
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator task23::VisiblePeople()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task23_VisiblePeople_m8AD7861251C1838E8C6B9B263FB45DBC2BB88E2F (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* L_0 = (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761*)il2cpp_codegen_object_new(U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CVisiblePeopleU3Ed__38__ctor_m561B71216B1355DF5FAD403B70C373D043A0E5F1(L_0, 0, NULL);
		U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task23::tog_activeRotate(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_tog_activeRotate_m8C37A2D5818BF1517C760C3D716E83A42FB25DE7 (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, bool ___is_On0, const RuntimeMethod* method) 
{
	{
		// if (Textpodskaz.activeSelf == true)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___Textpodskaz_18;
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368(L_0, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// if (is_On)
		bool L_2 = ___is_On0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// tog_rotate_active = true;
		__this->___tog_rotate_active_22 = (bool)1;
		return;
	}

IL_0018:
	{
		// tog_rotate_active = false;
		__this->___tog_rotate_active_22 = (bool)0;
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Void task23::notshowhint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23_notshowhint_mBB05ACEF9AAAD8B6D2B3A48B648319BD2640002E (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3F82580E4FA37F2BAB42B1DFE3D0FFF5E289B93C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC46E3DE1D683271C065DF2DFD5E2E87DA73D086E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Textpodskaz.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___Textpodskaz_18;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// if (tog_people_active)
		bool L_1 = __this->___tog_people_active_21;
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		// count_activePeople++;
		int32_t L_2 = __this->___count_activePeople_27;
		__this->___count_activePeople_27 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		// StartCoroutine("VisiblePeople");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_3;
		L_3 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteralC46E3DE1D683271C065DF2DFD5E2E87DA73D086E, NULL);
		// tog_people_active = false;
		__this->___tog_people_active_21 = (bool)0;
		// tog_people.isOn = false;
		Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* L_4 = __this->___tog_people_19;
		NullCheck(L_4);
		Toggle_set_isOn_m61D6AB073668E87530A9F49D990A3B3631D2061F(L_4, (bool)0, NULL);
	}

IL_0041:
	{
		// if (tog_rotate_active)
		bool L_5 = __this->___tog_rotate_active_22;
		if (!L_5)
		{
			goto IL_0076;
		}
	}
	{
		// count_activeRotate++;
		int32_t L_6 = __this->___count_activeRotate_28;
		__this->___count_activeRotate_28 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// StartCoroutine("VisibleRotate");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_7;
		L_7 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral3F82580E4FA37F2BAB42B1DFE3D0FFF5E289B93C, NULL);
		// tog_rotate_active = false;
		__this->___tog_rotate_active_22 = (bool)0;
		// tog_rotate.isOn = false;
		Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* L_8 = __this->___tog_rotate_20;
		NullCheck(L_8);
		Toggle_set_isOn_m61D6AB073668E87530A9F49D990A3B3631D2061F(L_8, (bool)0, NULL);
	}

IL_0076:
	{
		// }
		return;
	}
}
// System.Void task23::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task23__ctor_m1624DB91DBA641E4BDC0A2B73C6812A169588E8F (task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task23/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_m20ACD953B1BBACC38DDA747FCE0ED1E7EF173B7A (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task23/<stopwatch>d__28::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_m7E24741B924CF192799351F26AEF913EBC0D8007 (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task23/<stopwatch>d__28::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__28_MoveNext_mDBBBC8C2A2240F7B135217ABB7C6920D4ED2A292 (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_5 = V_1;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_11 = V_1;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_17 = V_1;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task23/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DE1E234065C895734C58CEE233C3B9DF8A732B4 (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task23/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m96D72C34051F787294468EB7E08BA0C90BE22675 (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m96D72C34051F787294468EB7E08BA0C90BE22675_RuntimeMethod_var)));
	}
}
// System.Object task23/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mEE7F5DD2B78F2C4F3C7EDAEA620DA686B246ED76 (U3CstopwatchU3Ed__28_t1ADC55F788F3E99467F3729E0D7F309F839EA1E4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task23/<VisibleRotate>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__29__ctor_m32FBBD56576C854E4E4A54DF1CB66A92DC28C367 (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task23/<VisibleRotate>d__29::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__29_System_IDisposable_Dispose_mBA331491978040189D256BEF30F5890E7FF8CF1A (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task23/<VisibleRotate>d__29::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CVisibleRotateU3Ed__29_MoveNext_mE9611179BF7AEBE7E18A8CCF892CA69FCC5E1601 (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// int i = 0;
		__this->___U3CiU3E5__2_3 = 0;
		// hints_rotate.SetActive(true);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_4 = V_1;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = L_4->___hints_rotate_26;
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
	}

IL_0031:
	{
		// ElevatorRotate.transform.eulerAngles = new Vector3(0, i, 0);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_6 = V_1;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6->___ElevatorRotate_24;
		NullCheck(L_7);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
		L_8 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_7, NULL);
		int32_t L_9 = __this->___U3CiU3E5__2_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_10), (0.0f), ((float)L_9), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004(L_8, L_10, NULL);
		// yield return new WaitForSeconds(0.01f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_11 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_11, (0.00999999978f), NULL);
		__this->___U3CU3E2__current_1 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_11);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0070:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// i++;
		int32_t L_12 = __this->___U3CiU3E5__2_3;
		V_2 = L_12;
		int32_t L_13 = V_2;
		__this->___U3CiU3E5__2_3 = ((int32_t)il2cpp_codegen_add(L_13, 1));
		// } while (i != 360);
		int32_t L_14 = __this->___U3CiU3E5__2_3;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)360)))))
		{
			goto IL_0031;
		}
	}
	{
		// ElevatorRotate.transform.eulerAngles = new Vector3(0, 0, 0);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_15 = V_1;
		NullCheck(L_15);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16 = L_15->___ElevatorRotate_24;
		NullCheck(L_16);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17;
		L_17 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_16, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_18), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004(L_17, L_18, NULL);
		// hints_rotate.SetActive(false);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_19 = V_1;
		NullCheck(L_19);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20 = L_19->___hints_rotate_26;
		NullCheck(L_20);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_20, (bool)0, NULL);
		// }
		return (bool)0;
	}
}
// System.Object task23/<VisibleRotate>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisibleRotateU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF8E4E209E96AC964B05634BA177DD9E1DA4E81E (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task23/<VisibleRotate>d__29::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_Reset_m7CBDF83FC94C799459737385FF0C2E1CFFC5876D (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_Reset_m7CBDF83FC94C799459737385FF0C2E1CFFC5876D_RuntimeMethod_var)));
	}
}
// System.Object task23/<VisibleRotate>d__29::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_get_Current_m8E76273C216A8F4E4694A4F34E0D6B9270319218 (U3CVisibleRotateU3Ed__29_t824A74F45305EAA83997661EE93C08A460C955A1* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task23/<VisiblePeople>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__38__ctor_m561B71216B1355DF5FAD403B70C373D043A0E5F1 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task23/<VisiblePeople>d__38::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__38_System_IDisposable_Dispose_mC336EE931B19EB00A5636EABF1920B5395C98EF7 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task23/<VisiblePeople>d__38::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CVisiblePeopleU3Ed__38_MoveNext_mF8AD53C09E7A8DBDCB840D2B91980DBE665777D4 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_004f;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// People.SetActive(true);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_4 = V_1;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = L_4->___People_23;
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// hints_people.SetActive(true);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_6 = V_1;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6->___hints_people_25;
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// yield return new WaitForSeconds(6.6f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_8 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_8, (6.5999999f), NULL);
		__this->___U3CU3E2__current_1 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_8);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_004f:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// People.SetActive(false);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_9 = V_1;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = L_9->___People_23;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)0, NULL);
		// hints_people.SetActive(false);
		task23_tB6346801589D62DB0F1A34A0DC9E8A4004C1FA42* L_11 = V_1;
		NullCheck(L_11);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = L_11->___hints_people_25;
		NullCheck(L_12);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_12, (bool)0, NULL);
		// }
		return (bool)0;
	}
}
// System.Object task23/<VisiblePeople>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisiblePeopleU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9127517308D43B42DC64A6E6A94348B816BD5A2 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task23/<VisiblePeople>d__38::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_Reset_m4246B29506244980BE7A9236CCDF52291E841A50 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_Reset_m4246B29506244980BE7A9236CCDF52291E841A50_RuntimeMethod_var)));
	}
}
// System.Object task23/<VisiblePeople>d__38::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_get_Current_mBE1B58715B37AE1491A81FC814DBA37FBB200601 (U3CVisiblePeopleU3Ed__38_t8CF9D5C2E61119BE7D052E13F5268E6F6F4A2761* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Task_Door_manager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_Start_m61E86AC6B10DE353D02989A16FE369ABEC17A380 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A V_0;
	memset((&V_0), 0, sizeof(V_0));
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// DLGMHints.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___DLGMHints_4;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// DLGMStatistics.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___DLGMStatistics_6;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// btnStatistics.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___btnStatistics_5;
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// People.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___People_7;
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// hints_people.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___hints_people_9;
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// hints_rotate.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___hints_rotate_10;
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// count_activePeople = 0;
		__this->___count_activePeople_14 = 0;
		// count_activeRotate = 0;
		__this->___count_activeRotate_15 = 0;
		// count_input_error = 0;
		__this->___count_input_error_16 = 0;
		// tog_people_active = false;
		__this->___tog_people_active_28 = (bool)0;
		// tog_rotate_active = false;
		__this->___tog_rotate_active_29 = (bool)0;
		// TimeSpan timeNow = DateTime.Now.TimeOfDay;
		il2cpp_codegen_runtime_class_init_inline(DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_il2cpp_TypeInfo_var);
		DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D L_6;
		L_6 = DateTime_get_Now_m636CB9651A9099D20BA1CF813A0C69637317325C(NULL);
		V_1 = L_6;
		TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A L_7;
		L_7 = DateTime_get_TimeOfDay_mE8933E5F62C0369E4BA6AF928283A00CA9D54D04((&V_1), NULL);
		V_0 = L_7;
		// time_start = (int)((((timeNow.Hours*60)+timeNow.Minutes)*60) + timeNow.Seconds);
		int32_t L_8;
		L_8 = TimeSpan_get_Hours_m770B4B777A816E051EFDA317C28DA9A4F39D6CFB((&V_0), NULL);
		int32_t L_9;
		L_9 = TimeSpan_get_Minutes_m93E37D01CD6DA2DE5B35609D740D322E270B678F((&V_0), NULL);
		int32_t L_10;
		L_10 = TimeSpan_get_Seconds_m8CA21613DC31BD025C5D30D41BAD0ED50827578B((&V_0), NULL);
		__this->___time_start_17 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_8, ((int32_t)60))), L_9)), ((int32_t)60))), L_10));
		// }
		return;
	}
}
// System.Void Task_Door_manager::btn_Hints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_btn_Hints_mAEBAD32B6C3F4F5E9549A3855D00A58DE05F1CBC (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// DLGMHints.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___DLGMHints_4;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::tog_activePeople(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_tog_activePeople_mE862D6F9E2A5B84C3E127C3ABB3767BC7FF21587 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, bool ___is_On0, const RuntimeMethod* method) 
{
	{
		// if (DLGMHints.activeSelf == true)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___DLGMHints_4;
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368(L_0, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// if (is_On)
		bool L_2 = ___is_On0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// tog_people_active = true;
		__this->___tog_people_active_28 = (bool)1;
		return;
	}

IL_0018:
	{
		// tog_people_active = false;
		__this->___tog_people_active_28 = (bool)0;
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator Task_Door_manager::VisiblePeople()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Task_Door_manager_VisiblePeople_m56759FF90D89ABB1C1CCCFB9D8C1EFE99191541B (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* L_0 = (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404*)il2cpp_codegen_object_new(U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CVisiblePeopleU3Ed__29__ctor_m4C06D63C1CC803EC17AC6A2252086760283F82AD(L_0, 0, NULL);
		U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void Task_Door_manager::tog_activeRotate(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_tog_activeRotate_m1C759AF11EF24F052DBB46DAD0021883BB968BD6 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, bool ___is_On0, const RuntimeMethod* method) 
{
	{
		// if (DLGMHints.activeSelf == true)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___DLGMHints_4;
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368(L_0, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// if (is_On)
		bool L_2 = ___is_On0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// tog_rotate_active = true;
		__this->___tog_rotate_active_29 = (bool)1;
		return;
	}

IL_0018:
	{
		// tog_rotate_active = false;
		__this->___tog_rotate_active_29 = (bool)0;
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator Task_Door_manager::VisibleRotate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Task_Door_manager_VisibleRotate_m9D42F74CEC4772F7B045976DE6F62991466B5583 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* L_0 = (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7*)il2cpp_codegen_object_new(U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CVisibleRotateU3Ed__31__ctor_m34B001F2552F17A0BD062867405FE3807320C9C7(L_0, 0, NULL);
		U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void Task_Door_manager::btn_Back()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_btn_Back_mE9C2821DAF9BDE683134BF8C8C28CA6E95EB3A99 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3F82580E4FA37F2BAB42B1DFE3D0FFF5E289B93C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC46E3DE1D683271C065DF2DFD5E2E87DA73D086E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DLGMHints.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___DLGMHints_4;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// if (tog_people_active)
		bool L_1 = __this->___tog_people_active_28;
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		// count_activePeople++;
		int32_t L_2 = __this->___count_activePeople_14;
		__this->___count_activePeople_14 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		// StartCoroutine("VisiblePeople");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_3;
		L_3 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteralC46E3DE1D683271C065DF2DFD5E2E87DA73D086E, NULL);
		// tog_people_active = false;
		__this->___tog_people_active_28 = (bool)0;
		// tog_people.isOn = false;
		Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* L_4 = __this->___tog_people_22;
		NullCheck(L_4);
		Toggle_set_isOn_m61D6AB073668E87530A9F49D990A3B3631D2061F(L_4, (bool)0, NULL);
	}

IL_0041:
	{
		// if (tog_rotate_active)
		bool L_5 = __this->___tog_rotate_active_29;
		if (!L_5)
		{
			goto IL_0076;
		}
	}
	{
		// count_activeRotate++;
		int32_t L_6 = __this->___count_activeRotate_15;
		__this->___count_activeRotate_15 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// StartCoroutine("VisibleRotate");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_7;
		L_7 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral3F82580E4FA37F2BAB42B1DFE3D0FFF5E289B93C, NULL);
		// tog_rotate_active = false;
		__this->___tog_rotate_active_29 = (bool)0;
		// tog_rotate.isOn = false;
		Toggle_tBF13F3EBA485E06826FD8A38F4B4C1380DF21A1F* L_8 = __this->___tog_rotate_23;
		NullCheck(L_8);
		Toggle_set_isOn_m61D6AB073668E87530A9F49D990A3B3631D2061F(L_8, (bool)0, NULL);
	}

IL_0076:
	{
		// }
		return;
	}
}
// System.Void Task_Door_manager::btn_Verify()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_btn_Verify_m8B6E07029E2376B971E38049D5337731618E3447 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral04E9EBE1F00A77E549A2B09F449656118F2B3197);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A V_0;
	memset((&V_0), 0, sizeof(V_0));
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// if (Answer.text == "720")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answer_12;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral04E9EBE1F00A77E549A2B09F449656118F2B3197, NULL);
		if (!L_2)
		{
			goto IL_008d;
		}
	}
	{
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_25;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// Placeholder.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___Placeholder_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// Answer.text = "";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___Answer_12;
		NullCheck(L_6);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// btnStatistics.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___btnStatistics_5;
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// TimeSpan timeNow = DateTime.Now.TimeOfDay;
		il2cpp_codegen_runtime_class_init_inline(DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_il2cpp_TypeInfo_var);
		DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D L_8;
		L_8 = DateTime_get_Now_m636CB9651A9099D20BA1CF813A0C69637317325C(NULL);
		V_1 = L_8;
		TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A L_9;
		L_9 = DateTime_get_TimeOfDay_mE8933E5F62C0369E4BA6AF928283A00CA9D54D04((&V_1), NULL);
		V_0 = L_9;
		// time = ((((timeNow.Hours * 60) + timeNow.Minutes) * 60) + timeNow.Seconds) - time_start;
		int32_t L_10;
		L_10 = TimeSpan_get_Hours_m770B4B777A816E051EFDA317C28DA9A4F39D6CFB((&V_0), NULL);
		int32_t L_11;
		L_11 = TimeSpan_get_Minutes_m93E37D01CD6DA2DE5B35609D740D322E270B678F((&V_0), NULL);
		int32_t L_12;
		L_12 = TimeSpan_get_Seconds_m8CA21613DC31BD025C5D30D41BAD0ED50827578B((&V_0), NULL);
		int32_t L_13 = __this->___time_start_17;
		__this->___time_13 = ((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_10, ((int32_t)60))), L_11)), ((int32_t)60))), L_12)), L_13));
		return;
	}

IL_008d:
	{
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14 = __this->___ok_25;
		NullCheck(L_14);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15;
		L_15 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_14, NULL);
		NullCheck(L_15);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_15, (bool)1, NULL);
		// Answer.text = "";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_16 = __this->___Answer_12;
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// Placeholder.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_17 = __this->___Placeholder_11;
		NullCheck(L_17);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// count_input_error++;
		int32_t L_18 = __this->___count_input_error_16;
		__this->___count_input_error_16 = ((int32_t)il2cpp_codegen_add(L_18, 1));
		// }
		return;
	}
}
// System.Void Task_Door_manager::btn_Statistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_btn_Statistics_m38A49BAF95DCF97E9FB002DD404348FA244280DE (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3BADA17CF62AE1A9C33E0086BFF271B2563C57B9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// text_peo.text = count_activePeople.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_0 = __this->___text_peo_18;
		int32_t* L_1 = (&__this->___count_activePeople_14);
		String_t* L_2;
		L_2 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_1, NULL);
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_0, L_2);
		// text_rot.text = count_activeRotate.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_3 = __this->___text_rot_19;
		int32_t* L_4 = (&__this->___count_activeRotate_15);
		String_t* L_5;
		L_5 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_4, NULL);
		NullCheck(L_3);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_3, L_5);
		// text_error.text = count_input_error.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_6 = __this->___text_error_20;
		int32_t* L_7 = (&__this->___count_input_error_16);
		String_t* L_8;
		L_8 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_7, NULL);
		NullCheck(L_6);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_6, L_8);
		// text_time.text = "";
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_9 = __this->___text_time_21;
		NullCheck(L_9);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_9, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// if (time % 3600  !=0)
		int32_t L_10 = __this->___time_13;
		if (!((int32_t)(L_10%((int32_t)3600))))
		{
			goto IL_00b8;
		}
	}
	{
		// temp = time / 3600;
		int32_t L_11 = __this->___time_13;
		__this->___temp_30 = ((int32_t)(L_11/((int32_t)3600)));
		// time = time - temp * 3600;
		int32_t L_12 = __this->___time_13;
		int32_t L_13 = __this->___temp_30;
		__this->___time_13 = ((int32_t)il2cpp_codegen_subtract(L_12, ((int32_t)il2cpp_codegen_multiply(L_13, ((int32_t)3600)))));
		// text_time.text = text_time.text + temp + ":";
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_14 = __this->___text_time_21;
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_15 = __this->___text_time_21;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(65 /* System.String TMPro.TMP_Text::get_text() */, L_15);
		int32_t* L_17 = (&__this->___temp_30);
		String_t* L_18;
		L_18 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_17, NULL);
		String_t* L_19;
		L_19 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(L_16, L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D, NULL);
		NullCheck(L_14);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_14, L_19);
		goto IL_00d8;
	}

IL_00b8:
	{
		// text_time.text = text_time.text + "00:";
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_20 = __this->___text_time_21;
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_21 = __this->___text_time_21;
		NullCheck(L_21);
		String_t* L_22;
		L_22 = VirtualFuncInvoker0< String_t* >::Invoke(65 /* System.String TMPro.TMP_Text::get_text() */, L_21);
		String_t* L_23;
		L_23 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_22, _stringLiteral3BADA17CF62AE1A9C33E0086BFF271B2563C57B9, NULL);
		NullCheck(L_20);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_20, L_23);
	}

IL_00d8:
	{
		// if (time % 60 != 0)
		int32_t L_24 = __this->___time_13;
		if (!((int32_t)(L_24%((int32_t)60))))
		{
			goto IL_0135;
		}
	}
	{
		// temp = time / 60;
		int32_t L_25 = __this->___time_13;
		__this->___temp_30 = ((int32_t)(L_25/((int32_t)60)));
		// time = time - temp * 60;
		int32_t L_26 = __this->___time_13;
		int32_t L_27 = __this->___temp_30;
		__this->___time_13 = ((int32_t)il2cpp_codegen_subtract(L_26, ((int32_t)il2cpp_codegen_multiply(L_27, ((int32_t)60)))));
		// text_time.text = text_time.text + temp + ":";
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_28 = __this->___text_time_21;
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_29 = __this->___text_time_21;
		NullCheck(L_29);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(65 /* System.String TMPro.TMP_Text::get_text() */, L_29);
		int32_t* L_31 = (&__this->___temp_30);
		String_t* L_32;
		L_32 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_31, NULL);
		String_t* L_33;
		L_33 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(L_30, L_32, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D, NULL);
		NullCheck(L_28);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_28, L_33);
		goto IL_0155;
	}

IL_0135:
	{
		// text_time.text = text_time.text + "00:";
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_34 = __this->___text_time_21;
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_35 = __this->___text_time_21;
		NullCheck(L_35);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(65 /* System.String TMPro.TMP_Text::get_text() */, L_35);
		String_t* L_37;
		L_37 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_36, _stringLiteral3BADA17CF62AE1A9C33E0086BFF271B2563C57B9, NULL);
		NullCheck(L_34);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_34, L_37);
	}

IL_0155:
	{
		// text_time.text = text_time.text + time;
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_38 = __this->___text_time_21;
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_39 = __this->___text_time_21;
		NullCheck(L_39);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(65 /* System.String TMPro.TMP_Text::get_text() */, L_39);
		int32_t* L_41 = (&__this->___time_13);
		String_t* L_42;
		L_42 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_41, NULL);
		String_t* L_43;
		L_43 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_40, L_42, NULL);
		NullCheck(L_38);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_38, L_43);
		// DLGMStatistics.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_44 = __this->___DLGMStatistics_6;
		NullCheck(L_44);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_44, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_notshowver_m556B0BDABE82C3B630DB06A59D1437674A7CDFE3 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_24;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_25;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::showhint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_showhint_m9BCA6A61BC831F7E5E292A2937891EBCCC57290A (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// DLGMHints.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___DLGMHints_4;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_notshowstatistics_m4C1680309C60FBA1DE36DCB55670BE03D5F58DDA (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_24;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// DLGMStatistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___DLGMStatistics_6;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_showar_m44CBA529E9459221D59B166B99E61D1D1FB22F69 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_24;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_26;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// info.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___info_27;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_notshowar_m056D22FDCEDBDED9FF8C26CBC250D22E090876F0 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_24;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_26;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::notinfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager_notinfo_m4F9A478F9DB50ED6B4431289CB27B98F66DD7273 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		// info.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___info_27;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Task_Door_manager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Task_Door_manager__ctor_m2592C61D9BEB9179787DCBDC9DFEF63329325E60 (Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Task_Door_manager/<VisiblePeople>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__29__ctor_m4C06D63C1CC803EC17AC6A2252086760283F82AD (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Task_Door_manager/<VisiblePeople>d__29::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__29_System_IDisposable_Dispose_m9E8A5E3ED2027C01EABD58F8715DC6D64E9A2191 (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean Task_Door_manager/<VisiblePeople>d__29::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CVisiblePeopleU3Ed__29_MoveNext_mC1F19694A09408545418DC076BC23202FE7069E3 (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_004f;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// People.SetActive(true);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_4 = V_1;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = L_4->___People_7;
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// hints_people.SetActive(true);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_6 = V_1;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6->___hints_people_9;
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// yield return new WaitForSeconds(6.6f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_8 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_8, (6.5999999f), NULL);
		__this->___U3CU3E2__current_1 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_8);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_004f:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// People.SetActive(false);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_9 = V_1;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = L_9->___People_7;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)0, NULL);
		// hints_people.SetActive(false);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_11 = V_1;
		NullCheck(L_11);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = L_11->___hints_people_9;
		NullCheck(L_12);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_12, (bool)0, NULL);
		// }
		return (bool)0;
	}
}
// System.Object Task_Door_manager/<VisiblePeople>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisiblePeopleU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02EB33F95B66F03FDCB145E76E542F5BBC14CCA0 (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Task_Door_manager/<VisiblePeople>d__29::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_Reset_mF3D061165A076653B3044E1EEC3FE34959CDBBF8 (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_Reset_mF3D061165A076653B3044E1EEC3FE34959CDBBF8_RuntimeMethod_var)));
	}
}
// System.Object Task_Door_manager/<VisiblePeople>d__29::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_get_Current_mA45ACC0C1443F361432F420CF4A5E040594DE296 (U3CVisiblePeopleU3Ed__29_t74E47A3F452FD4CAABD28F4B9B6B5EE991438404* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Task_Door_manager/<VisibleRotate>d__31::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__31__ctor_m34B001F2552F17A0BD062867405FE3807320C9C7 (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Task_Door_manager/<VisibleRotate>d__31::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__31_System_IDisposable_Dispose_m7571664E714301A3F86C18A1948807192D09683B (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean Task_Door_manager/<VisibleRotate>d__31::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CVisibleRotateU3Ed__31_MoveNext_m2A3118E4C49E683947C2FDD0B55A7D3620010364 (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// int i = 0;
		__this->___U3CiU3E5__2_3 = 0;
		// hints_rotate.SetActive(true);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_4 = V_1;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = L_4->___hints_rotate_10;
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
	}

IL_0031:
	{
		// ElevatorRotate.transform.eulerAngles = new Vector3(0, i, 0);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_6 = V_1;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6->___ElevatorRotate_8;
		NullCheck(L_7);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
		L_8 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_7, NULL);
		int32_t L_9 = __this->___U3CiU3E5__2_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_10), (0.0f), ((float)L_9), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004(L_8, L_10, NULL);
		// yield return new WaitForSeconds(0.01f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_11 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_11, (0.00999999978f), NULL);
		__this->___U3CU3E2__current_1 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_11);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0070:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// i++;
		int32_t L_12 = __this->___U3CiU3E5__2_3;
		V_2 = L_12;
		int32_t L_13 = V_2;
		__this->___U3CiU3E5__2_3 = ((int32_t)il2cpp_codegen_add(L_13, 1));
		// } while (i != 360);
		int32_t L_14 = __this->___U3CiU3E5__2_3;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)360)))))
		{
			goto IL_0031;
		}
	}
	{
		// ElevatorRotate.transform.eulerAngles = new Vector3(0, 0, 0);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_15 = V_1;
		NullCheck(L_15);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16 = L_15->___ElevatorRotate_8;
		NullCheck(L_16);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17;
		L_17 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_16, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_18), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004(L_17, L_18, NULL);
		// hints_rotate.SetActive(false);
		Task_Door_manager_t2085A6D1BEBDDAD20E6D9CD4ACBCCC10311E4753* L_19 = V_1;
		NullCheck(L_19);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20 = L_19->___hints_rotate_10;
		NullCheck(L_20);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_20, (bool)0, NULL);
		// }
		return (bool)0;
	}
}
// System.Object Task_Door_manager/<VisibleRotate>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisibleRotateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91DAAEAF951EB100F86F0C31704D80E325832865 (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Task_Door_manager/<VisibleRotate>d__31::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_Reset_m3DEA0136564A7FBE1C91EF8659AD71DEA872B57C (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_Reset_m3DEA0136564A7FBE1C91EF8659AD71DEA872B57C_RuntimeMethod_var)));
	}
}
// System.Object Task_Door_manager/<VisibleRotate>d__31::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_get_Current_m0A32639DB93E11597F23E763E9DCED2A594BAF92 (U3CVisibleRotateU3Ed__31_tD9C178EE44EF5CDAF8FBA32B327F4EE63C9F7CF7* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task20::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_Start_mF533685D9A82BAC636B28DF5312F3BF696B8AADB (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_16;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task20::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task20_stopwatch_m93DC59FD5E751D67252AC0E3BE7247D598206434 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* L_0 = (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__22__ctor_m6FF962E670F0354B1BF097A4F6D365ABD8A8CEF5(L_0, 0, NULL);
		U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task20::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_answer_m554812542D41A02AF2D4D19EF73F792867CCFC09 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "10")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_10;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_12;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_22;
		__this->___mistake_22 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_10;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_11;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_12;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task20::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_showhint1_mDFA29F24DF2D4D294648F3C3CF09BFB2B949E263 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_16;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_19;
		__this->___p2_19 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task20::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_notshowhint1_mA6846CBF26528EDF711411AFEC41CED9D7110AF3 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task20::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_notshowver_m5674F9846E50B98BFEF6FF6A2CD13C30FC70313B (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task20::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_showstatistics_m1B00594C5438989690DD40E5D09C71D71630FCF2 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_20;
		int32_t* L_5 = (&__this->___p1_18);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_21;
		int32_t* L_8 = (&__this->___p2_19);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_24;
		int32_t* L_11 = (&__this->___mistake_22);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_23;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task20::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_notshowstatistics_m18F42B20B20B7D828C84050E53CD1A0CD552DC89 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task20::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_showar_m5A5A1F7AB7661957EE90F0BCC6F677A5FA146F74 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_18;
		__this->___p1_18 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task20::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20_notshowar_mD282D8948220BC9DC56437FCF17C6A53F21A9551 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task20::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task20__ctor_m370415A6C3F20DD11C23C169FF3F567266379B09 (task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task20/<stopwatch>d__22::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__22__ctor_m6FF962E670F0354B1BF097A4F6D365ABD8A8CEF5 (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task20/<stopwatch>d__22::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__22_System_IDisposable_Dispose_m10B0A8D9B1524330A1E820ED4E74F6B8ED334C43 (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task20/<stopwatch>d__22::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__22_MoveNext_m755FD9840826CBDD979FA6E3BAB0BA7181C0F05E (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_5 = V_1;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_11 = V_1;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_17 = V_1;
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task20_t3D21D377B9C69943268EEB49715FC68A77487D1C* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task20/<stopwatch>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE8869F1C1CB3A564CFA22DD79C4BA855B8817C (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task20/<stopwatch>d__22::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__22_System_Collections_IEnumerator_Reset_m3ECCA74D5197F50D38F45FEB9DF44710AA48C65D (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__22_System_Collections_IEnumerator_Reset_m3ECCA74D5197F50D38F45FEB9DF44710AA48C65D_RuntimeMethod_var)));
	}
}
// System.Object task20/<stopwatch>d__22::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__22_System_Collections_IEnumerator_get_Current_m99C2E8CA3B5BD12A840488A4C2566B4AF44297EA (U3CstopwatchU3Ed__22_t235714227C16D6E3FFAA4C0D4CE7337922FEAFF4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task15::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_Start_m6EB0BC04176274907071B5F00EDC646CE459ED09 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task15::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task15_stopwatch_m857B335C8086453B6354F2D3F0BE344DCD93BD4B (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* L_0 = (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__27__ctor_m2F5D907BB12AB98E504E9D63C163FFDE0FFF6838(L_0, 0, NULL);
		U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task15::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_answer_mAAD9D66C4D1BB53B343E77993156C68E626F767F (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((Answ.text.Contains("7") && Answ.text.Contains("3") && Answ.text.Contains("1") && Answ.text.Contains("5"))||
		//         (Answ.text.Contains("7") && Answ.text.Contains("3") && Answ.text.Contains("6") && Answ.text.Contains("5")) ||
		//         (Answ.text.Contains("6") && Answ.text.Contains("3") && Answ.text.Contains("1") && Answ.text.Contains("5")) ||
		//         (Answ.text.Contains("7") && Answ.text.Contains("3") && Answ.text.Contains("1") && Answ.text.Contains("2")) ||
		//         (Answ.text.Contains("7") && Answ.text.Contains("3") && Answ.text.Contains("2") && Answ.text.Contains("6")) ||
		//         (Answ.text.Contains("6") && Answ.text.Contains("3") && Answ.text.Contains("1") && Answ.text.Contains("2")) ||
		//         (Answ.text.Contains("7") && Answ.text.Contains("2") && Answ.text.Contains("1") && Answ.text.Contains("5")) ||
		//         (Answ.text.Contains("7") && Answ.text.Contains("6") && Answ.text.Contains("2") && Answ.text.Contains("5")) ||
		//         (Answ.text.Contains("1") && Answ.text.Contains("6") && Answ.text.Contains("2") && Answ.text.Contains("5")))
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		NullCheck(L_1);
		bool L_2;
		L_2 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_1, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_2)
		{
			goto IL_005f;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = __this->___Answ_9;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		NullCheck(L_4);
		bool L_5;
		L_5 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_4, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___Answ_9;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
		NullCheck(L_7);
		bool L_8;
		L_8 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_7, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_9 = __this->___Answ_9;
		NullCheck(L_9);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
		NullCheck(L_10);
		bool L_11;
		L_11 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_10, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (L_11)
		{
			goto IL_0351;
		}
	}

IL_005f:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_12 = __this->___Answ_9;
		NullCheck(L_12);
		String_t* L_13;
		L_13 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_12);
		NullCheck(L_13);
		bool L_14;
		L_14 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_13, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_14)
		{
			goto IL_00be;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_15 = __this->___Answ_9;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_15);
		NullCheck(L_16);
		bool L_17;
		L_17 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_16, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_17)
		{
			goto IL_00be;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_18 = __this->___Answ_9;
		NullCheck(L_18);
		String_t* L_19;
		L_19 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_18);
		NullCheck(L_19);
		bool L_20;
		L_20 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_19, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_20)
		{
			goto IL_00be;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___Answ_9;
		NullCheck(L_21);
		String_t* L_22;
		L_22 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_21);
		NullCheck(L_22);
		bool L_23;
		L_23 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_22, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (L_23)
		{
			goto IL_0351;
		}
	}

IL_00be:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_24 = __this->___Answ_9;
		NullCheck(L_24);
		String_t* L_25;
		L_25 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_24);
		NullCheck(L_25);
		bool L_26;
		L_26 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_25, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_26)
		{
			goto IL_011d;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_27 = __this->___Answ_9;
		NullCheck(L_27);
		String_t* L_28;
		L_28 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_27);
		NullCheck(L_28);
		bool L_29;
		L_29 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_28, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_29)
		{
			goto IL_011d;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_30 = __this->___Answ_9;
		NullCheck(L_30);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_30);
		NullCheck(L_31);
		bool L_32;
		L_32 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_31, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_32)
		{
			goto IL_011d;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_33 = __this->___Answ_9;
		NullCheck(L_33);
		String_t* L_34;
		L_34 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_33);
		NullCheck(L_34);
		bool L_35;
		L_35 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_34, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (L_35)
		{
			goto IL_0351;
		}
	}

IL_011d:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_36 = __this->___Answ_9;
		NullCheck(L_36);
		String_t* L_37;
		L_37 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_36);
		NullCheck(L_37);
		bool L_38;
		L_38 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_37, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_38)
		{
			goto IL_017c;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___Answ_9;
		NullCheck(L_39);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_39);
		NullCheck(L_40);
		bool L_41;
		L_41 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_40, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_41)
		{
			goto IL_017c;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_42 = __this->___Answ_9;
		NullCheck(L_42);
		String_t* L_43;
		L_43 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_42);
		NullCheck(L_43);
		bool L_44;
		L_44 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_43, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_44)
		{
			goto IL_017c;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_45 = __this->___Answ_9;
		NullCheck(L_45);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_45);
		NullCheck(L_46);
		bool L_47;
		L_47 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_46, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (L_47)
		{
			goto IL_0351;
		}
	}

IL_017c:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_48 = __this->___Answ_9;
		NullCheck(L_48);
		String_t* L_49;
		L_49 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_48);
		NullCheck(L_49);
		bool L_50;
		L_50 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_49, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_50)
		{
			goto IL_01db;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_51 = __this->___Answ_9;
		NullCheck(L_51);
		String_t* L_52;
		L_52 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_51);
		NullCheck(L_52);
		bool L_53;
		L_53 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_52, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_53)
		{
			goto IL_01db;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_54 = __this->___Answ_9;
		NullCheck(L_54);
		String_t* L_55;
		L_55 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_54);
		NullCheck(L_55);
		bool L_56;
		L_56 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_55, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_56)
		{
			goto IL_01db;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___Answ_9;
		NullCheck(L_57);
		String_t* L_58;
		L_58 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_57);
		NullCheck(L_58);
		bool L_59;
		L_59 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_58, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (L_59)
		{
			goto IL_0351;
		}
	}

IL_01db:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_60 = __this->___Answ_9;
		NullCheck(L_60);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_60);
		NullCheck(L_61);
		bool L_62;
		L_62 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_61, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_62)
		{
			goto IL_023a;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_63 = __this->___Answ_9;
		NullCheck(L_63);
		String_t* L_64;
		L_64 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_63);
		NullCheck(L_64);
		bool L_65;
		L_65 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_64, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_65)
		{
			goto IL_023a;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_66 = __this->___Answ_9;
		NullCheck(L_66);
		String_t* L_67;
		L_67 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_66);
		NullCheck(L_67);
		bool L_68;
		L_68 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_67, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_68)
		{
			goto IL_023a;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_69 = __this->___Answ_9;
		NullCheck(L_69);
		String_t* L_70;
		L_70 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_69);
		NullCheck(L_70);
		bool L_71;
		L_71 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_70, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (L_71)
		{
			goto IL_0351;
		}
	}

IL_023a:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_72 = __this->___Answ_9;
		NullCheck(L_72);
		String_t* L_73;
		L_73 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_72);
		NullCheck(L_73);
		bool L_74;
		L_74 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_73, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_74)
		{
			goto IL_0299;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___Answ_9;
		NullCheck(L_75);
		String_t* L_76;
		L_76 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_75);
		NullCheck(L_76);
		bool L_77;
		L_77 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_76, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_77)
		{
			goto IL_0299;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_78 = __this->___Answ_9;
		NullCheck(L_78);
		String_t* L_79;
		L_79 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_78);
		NullCheck(L_79);
		bool L_80;
		L_80 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_79, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_80)
		{
			goto IL_0299;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_81 = __this->___Answ_9;
		NullCheck(L_81);
		String_t* L_82;
		L_82 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_81);
		NullCheck(L_82);
		bool L_83;
		L_83 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_82, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (L_83)
		{
			goto IL_0351;
		}
	}

IL_0299:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_84 = __this->___Answ_9;
		NullCheck(L_84);
		String_t* L_85;
		L_85 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_84);
		NullCheck(L_85);
		bool L_86;
		L_86 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_85, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_86)
		{
			goto IL_02f5;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_87 = __this->___Answ_9;
		NullCheck(L_87);
		String_t* L_88;
		L_88 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_87);
		NullCheck(L_88);
		bool L_89;
		L_89 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_88, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_89)
		{
			goto IL_02f5;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_90 = __this->___Answ_9;
		NullCheck(L_90);
		String_t* L_91;
		L_91 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_90);
		NullCheck(L_91);
		bool L_92;
		L_92 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_91, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_92)
		{
			goto IL_02f5;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___Answ_9;
		NullCheck(L_93);
		String_t* L_94;
		L_94 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_93);
		NullCheck(L_94);
		bool L_95;
		L_95 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_94, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (L_95)
		{
			goto IL_0351;
		}
	}

IL_02f5:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_96 = __this->___Answ_9;
		NullCheck(L_96);
		String_t* L_97;
		L_97 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_96);
		NullCheck(L_97);
		bool L_98;
		L_98 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_97, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_98)
		{
			goto IL_038b;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_99 = __this->___Answ_9;
		NullCheck(L_99);
		String_t* L_100;
		L_100 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_99);
		NullCheck(L_100);
		bool L_101;
		L_101 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_100, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_101)
		{
			goto IL_038b;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_102 = __this->___Answ_9;
		NullCheck(L_102);
		String_t* L_103;
		L_103 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_102);
		NullCheck(L_103);
		bool L_104;
		L_104 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_103, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_104)
		{
			goto IL_038b;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_105 = __this->___Answ_9;
		NullCheck(L_105);
		String_t* L_106;
		L_106 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_105);
		NullCheck(L_106);
		bool L_107;
		L_107 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_106, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (!L_107)
		{
			goto IL_038b;
		}
	}

IL_0351:
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_108 = __this->___ok_10;
		NullCheck(L_108);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_109;
		L_109 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_108, NULL);
		NullCheck(L_109);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_109, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_110 = __this->___prov_11;
		NullCheck(L_110);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_110, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_111 = __this->___stat_12;
		NullCheck(L_111);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112;
		L_112 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_111, NULL);
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		return;
	}

IL_038b:
	{
		// mistake++;
		int32_t L_113 = __this->___mistake_25;
		__this->___mistake_25 = ((int32_t)il2cpp_codegen_add(L_113, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_114 = __this->___ok_10;
		NullCheck(L_114);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_115;
		L_115 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_114, NULL);
		NullCheck(L_115);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_115, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_116 = __this->___prov_11;
		NullCheck(L_116);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_116, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_117 = __this->___stat_12;
		NullCheck(L_117);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_118;
		L_118 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_117, NULL);
		NullCheck(L_118);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_118, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_showhint1_m926551F5466C175BD8491C5CF6A5E7A0EAA76790 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_18;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_21;
		__this->___p1_21 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task15::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_notshowhint1_m92C2929AA61E1480014F708D7874E3501DB4BC93 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_showhint2_mD12453AAE226EB208500330C872BC24B9507C4A5 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_20;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_22;
		__this->___p2_22 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task15::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_notshowhint2_mF38918D67803F3BE629461BCE3A83A6789971902 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_notshowver_mAD2841186D6D793181CC894A00111EA040F63C7E (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_showstatistics_mEFC8CEE07759B268371CC8C570F0AB43F493C001 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_23;
		int32_t* L_5 = (&__this->___p1_21);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_24;
		int32_t* L_8 = (&__this->___p2_22);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_27;
		int32_t* L_11 = (&__this->___mistake_25);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_26;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task15::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_notshowstatistics_m33DB1025BC0687FB342E35DEA2E67789ADF71225 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_showar_m84D9FBA12BA90E39F1FCC4FB8634FE671A4D6FD1 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task15::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_notshowar_m66FC2733997F7E8C9443376EA9080106028FB989 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::nextuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_nextuslzad_m93F7F754A2A90EF8BEBBFA46F4A0593F8F435BF7 (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_28;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// usl2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_29;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task15::backuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15_backuslzad_mCD089CDF8822FD7E70BD984183FD591140E4B1AB (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_28;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// usl2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_29;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task15::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task15__ctor_m15D7CD3FDA4992323F534948AE1F3348277ED97E (task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task15/<stopwatch>d__27::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__27__ctor_m2F5D907BB12AB98E504E9D63C163FFDE0FFF6838 (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task15/<stopwatch>d__27::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__27_System_IDisposable_Dispose_m195C6BFB6CB1125CE2DBAFFC4D66754F377CED92 (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task15/<stopwatch>d__27::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__27_MoveNext_mF0249B2C4FB9D3C1D42A56B682ED56258B279ADD (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_5 = V_1;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_11 = V_1;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_17 = V_1;
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task15_t2357C75B04D5FC197226D143B011FEE5445DE0CA* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task15/<stopwatch>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19807947C7AFE56A572BD94EEDDD297A4E9C6186 (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task15/<stopwatch>d__27::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__27_System_Collections_IEnumerator_Reset_mBA30D813137118067E8CCF6E4B421B60764FAB4A (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__27_System_Collections_IEnumerator_Reset_mBA30D813137118067E8CCF6E4B421B60764FAB4A_RuntimeMethod_var)));
	}
}
// System.Object task15/<stopwatch>d__27::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__27_System_Collections_IEnumerator_get_Current_m30B7BFAA601AAF6027028C4B2ECAC126BA82D405 (U3CstopwatchU3Ed__27_t8C626F14252BCF8C9E60B19A8CF651C573B8BCC5* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CubeRotate::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CubeRotate_Update_m5329A8708151CE19E112FC96D731DADDE6E78748 (CubeRotate_t28274E131964C53FEEBDE5D076CFAFF181D3C45F* __this, const RuntimeMethod* method) 
{
	{
		// transform.Rotate(new Vector3(0, 0, 45) * Time.deltaTime);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_1), (0.0f), (0.0f), (45.0f), /*hidden argument*/NULL);
		float L_2;
		L_2 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_1, L_2, NULL);
		NullCheck(L_0);
		Transform_Rotate_m2A308205498AFEEA3DF784B1C86E4F7C126CA2EE(L_0, L_3, NULL);
		// }
		return;
	}
}
// System.Void CubeRotate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CubeRotate__ctor_m8FABF7E0E452666796420B4BE9DCA4E64BD70767 (CubeRotate_t28274E131964C53FEEBDE5D076CFAFF181D3C45F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task2::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_Start_mA76252C3325EBE77E36F02340536F5C1FCF3C0DD (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_17;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task2::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task2_stopwatch_mE2A3C1694CCA003374884201048E08DF0EE56BA5 (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* L_0 = (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__23__ctor_m6A763E528C337619B93BA87550784E8CD7AEB70D(L_0, 0, NULL);
		U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task2::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_answer_mFC72D92CE1238B594899EC1E09FC146B76B25F73 (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral556DDA907570BAA94FD8305D5E082166623C2DE7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral700C1596CA301B3AA61533700CEED311DC1053BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "23" || Answ.text == "32")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral700C1596CA301B3AA61533700CEED311DC1053BC, NULL);
		if (L_2)
		{
			goto IL_002e;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = __this->___Answ_9;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		bool L_5;
		L_5 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_4, _stringLiteral556DDA907570BAA94FD8305D5E082166623C2DE7, NULL);
		if (!L_5)
		{
			goto IL_0068;
		}
	}

IL_002e:
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___ok_10;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_8 = __this->___prov_11;
		NullCheck(L_8);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___stat_12;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		return;
	}

IL_0068:
	{
		// mistake++;
		int32_t L_11 = __this->___mistake_21;
		__this->___mistake_21 = ((int32_t)il2cpp_codegen_add(L_11, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___ok_10;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_14 = __this->___prov_11;
		NullCheck(L_14);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15 = __this->___stat_12;
		NullCheck(L_15);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16;
		L_16 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_15, NULL);
		NullCheck(L_16);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_16, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task2::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_notshowver_m14633FA86BD512BB228A0777BE07AFB1691CD9C6 (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task2::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_showstatistics_m87E64E1632DE256166AE6AEABA238B2F78986291 (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_19;
		int32_t* L_5 = (&__this->___p1_15);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_20;
		int32_t* L_8 = (&__this->___p2_16);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_23;
		int32_t* L_11 = (&__this->___mistake_21);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_22;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task2::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_notshowstatistics_m1B5C4DC751E25CE24C776B87A14A79A9593908F9 (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task2::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_showar_mCEF19E2CBBDD2EEB8E2E58D4B9F7295F3CA3178D (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_15;
		__this->___p1_15 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task2::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_notshowar_m349E31545F60799DFFB362C8D6BBBAFD5286937B (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task2::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_showhint1_m7EA5948CCE3ABD87BB49F47CBF420382D676F5A9 (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___hint1_24;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// p1++;
		int32_t L_2 = __this->___p1_15;
		__this->___p1_15 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz1use_17;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task2::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_notshowhint1_m199BFBB9B5F4CB10986E084E8F4AA8724D8D922C (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___hint1_24;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task2::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_showhint2_mEF8DC2FED820C4E160939C37493C65C4F9CD515E (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___hint2_25;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// p2++;
		int32_t L_2 = __this->___p2_16;
		__this->___p2_16 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		// }
		return;
	}
}
// System.Void task2::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2_notshowhint2_m59FEEA9B1E29DD3E027FAF21890F4C198801DC8A (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___hint2_25;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task2__ctor_m509B319AF9AE752692253595723EA2CB736742FB (task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task2/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m6A763E528C337619B93BA87550784E8CD7AEB70D (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task2/<stopwatch>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_m05F05ECBC6DDBB6F840407617AE050983215232A (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task2/<stopwatch>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__23_MoveNext_mB8EE0A685B880CD5249656BCD077B5B30A62CDBB (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_5 = V_1;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_11 = V_1;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_17 = V_1;
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task2_t2C5D69F0A329B1B40F552703669EA12C355019C4* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task2/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD11EA88F3846CE46262CFB25D589596F212C0C (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task2/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m7687E13C63EB80EA50D717667B6A601259F2C817 (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m7687E13C63EB80EA50D717667B6A601259F2C817_RuntimeMethod_var)));
	}
}
// System.Object task2/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_mC1C7451693935FBC3A16C1529A24063300610D88 (U3CstopwatchU3Ed__23_t92DDC7FADB07DEDBA633514C9146E23902D02CC6* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task6::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_Start_m0153FB064AEAA594FEEDF93D7206E57E6F022A17 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 00;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_20;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task6::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task6_stopwatch_mE1D169201A449AE4106C2A25BAFD87D0E64FEB64 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* L_0 = (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__25__ctor_mBE7488CED855751FE6A9873967B29D948A39278E(L_0, 0, NULL);
		U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task6::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_answer_mBC1716119E4FAB35C676C654C523867B5B55DDEC (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((Answ1.text == "1") && (Answ2.text == "5") && (Answ3.text == "4") &&
		//     (Answ4.text == "2") && (Answ5.text == "6") && (Answ6.text == "5"))
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ1_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (!L_2)
		{
			goto IL_00ca;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = __this->___Answ2_9;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		bool L_5;
		L_5 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_4, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (!L_5)
		{
			goto IL_00ca;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___Answ3_10;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
		bool L_8;
		L_8 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_7, _stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9, NULL);
		if (!L_8)
		{
			goto IL_00ca;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_9 = __this->___Answ4_11;
		NullCheck(L_9);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
		bool L_11;
		L_11 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_10, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_11)
		{
			goto IL_00ca;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_12 = __this->___Answ5_12;
		NullCheck(L_12);
		String_t* L_13;
		L_13 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_12);
		bool L_14;
		L_14 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_13, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_14)
		{
			goto IL_00ca;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_15 = __this->___Answ6_13;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_15);
		bool L_17;
		L_17 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_16, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (!L_17)
		{
			goto IL_00ca;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18 = __this->___ok_14;
		NullCheck(L_18);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19;
		L_19 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_18, NULL);
		NullCheck(L_19);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_19, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_20 = __this->___prov_15;
		NullCheck(L_20);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_21 = __this->___stat_16;
		NullCheck(L_21);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
		L_22 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_21, NULL);
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		return;
	}

IL_00ca:
	{
		// mistake++;
		int32_t L_23 = __this->___mistake_23;
		__this->___mistake_23 = ((int32_t)il2cpp_codegen_add(L_23, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_24 = __this->___ok_14;
		NullCheck(L_24);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25;
		L_25 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_24, NULL);
		NullCheck(L_25);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_25, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_26 = __this->___prov_15;
		NullCheck(L_26);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_26, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = __this->___stat_16;
		NullCheck(L_27);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_27, NULL);
		NullCheck(L_28);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_28, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task6::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_notshowver_m17AA4B30E03D8B9C0A440B859B35475AFEEEA476 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_17;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task6::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_showstatistics_m2A895869CDF30E62C90B700B37EE408E7E7C7E32 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_17;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_18;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_22;
		int32_t* L_5 = (&__this->___p1_21);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___textoshibka_25;
		int32_t* L_8 = (&__this->___mistake_23);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___texttime_24;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		int32_t* L_13 = (&__this->___time_Ch_6);
		String_t* L_14;
		L_14 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_13, NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_14);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_12;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = L_15;
		int32_t* L_17 = (&__this->___time_M_5);
		String_t* L_18;
		L_18 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_17, NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_18);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_19;
		int32_t* L_21 = (&__this->___time_S_4);
		String_t* L_22;
		L_22 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_21, NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_22);
		String_t* L_23;
		L_23 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_20, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_23);
		// }
		return;
	}
}
// System.Void task6::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_notshowstatistics_m204E489F3604C7454F61EB21CDAD8707FA134ED2 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_17;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_18;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task6::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_showhint1_m10B5A77D70D20ACBD5E2A56163F9440639C2D2E8 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_17;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_19;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// p1++;
		int32_t L_4 = __this->___p1_21;
		__this->___p1_21 = ((int32_t)il2cpp_codegen_add(L_4, 1));
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___podskaz1use_20;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task6::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_notshowhint1_m6BD59DCD61198571580BB6F36EBF557767C7E1BC (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_17;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_19;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task6::nextuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_nextuslzad_m3D39D538A4A3BF92A1E04C3D79AF7465B7ECA010 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_26;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// usl2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_27;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task6::backuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6_backuslzad_mD72CFE1AEAD9A6C161693C2673F846BFEA662F2B (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_26;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// usl2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_27;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task6::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task6__ctor_mDCB2060F7ED7DA50A69019BD6AC07D994307FDD8 (task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task6/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mBE7488CED855751FE6A9873967B29D948A39278E (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task6/<stopwatch>d__25::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m53B282F84A7D3076991C9F51A5D8D05E510E1CC8 (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task6/<stopwatch>d__25::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__25_MoveNext_m1F504BD1F2F28B3C7A3AF309466FD7C96B5F1BB8 (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_5 = V_1;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 00;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_11 = V_1;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 00;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_17 = V_1;
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task6_t743E4AEDB8420BAFC9A1B63835320F3CD903F5ED* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task6/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F3AEEC752291A571E12EB2CC7CE72B53A8958E9 (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task6/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m896AFD90834C83785294BC43E7A161D3966D3DC6 (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m896AFD90834C83785294BC43E7A161D3966D3DC6_RuntimeMethod_var)));
	}
}
// System.Object task6/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m429B23341905A2972FA9BCC40F1FD9F27285CBD3 (U3CstopwatchU3Ed__25_t2E0D472C104A2B784A434897B13A6E0E05A3C947* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task17::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_Start_m8D9C457CCE8CE04DDA700238DA4E4BE8B60DAB2F (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task17::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task17_stopwatch_m250A4B51D7AC3DE3436B2380FC038A6577BBC735 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* L_0 = (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__25__ctor_mF47A706BF25A5698C8E4C2445AE0B63B9F7E7ABB(L_0, 0, NULL);
		U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task17::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_answer_mCD958FCA676848081930963384E4571EC89FD715 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF859CAA74FC4BD45E85F9AFF276F614AE47D3DA2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "18")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralF859CAA74FC4BD45E85F9AFF276F614AE47D3DA2, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_10;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_12;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_25;
		__this->___mistake_25 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_10;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_11;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_12;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task17::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_showhint1_m2B3B0C12A9D2F6E7A2FC8FC8850428224749B5C7 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_18;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_21;
		__this->___p1_21 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task17::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_notshowhint1_mEFDD70AB171DE2AFA5D22E1B788B72C69525571C (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task17::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_showhint2_m154361BF007D60D0AD8868383F2A25A3C9495352 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_20;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_22;
		__this->___p2_22 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task17::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_notshowhint2_mF659745FE8D2F702CC458EAFF6D855AE24B94091 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task17::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_notshowver_m0B2A39583BBAD2DFA42B1921C4430C5FA3BCF1FB (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task17::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_showstatistics_mFBA0CAF6F359C78DBB076464B37AF371BBC01BA5 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_23;
		int32_t* L_5 = (&__this->___p1_21);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_24;
		int32_t* L_8 = (&__this->___p2_22);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_27;
		int32_t* L_11 = (&__this->___mistake_25);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_26;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task17::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_notshowstatistics_m5AE3EE321D44CAC645AEAD9A3858077F7F3CDBCE (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task17::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_showar_m8D87DBD8DA7AEEFD8998449ADF4AA0F81F809815 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task17::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17_notshowar_m55B28011E60D5617F632116007C6F4683AA15C44 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task17::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task17__ctor_mE62EC7FC104AE9BEB8F26EFF80CFC2B7315CCE67 (task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task17/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mF47A706BF25A5698C8E4C2445AE0B63B9F7E7ABB (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task17/<stopwatch>d__25::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_mABF71C27E50CE6F620863B20B0FBE34600A93176 (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task17/<stopwatch>d__25::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__25_MoveNext_m5C32301722B7469C5589ADC687B1A513BDCD8915 (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_5 = V_1;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_11 = V_1;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_17 = V_1;
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task17_t31BD90159BED97C8AF144D4987B7CD17FB655180* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task17/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD660F032C4E2C84CABEB86CAF8D6E7F12295F5F (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task17/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m93082B657640111769C616B95E39BA4DEB00CA36 (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m93082B657640111769C616B95E39BA4DEB00CA36_RuntimeMethod_var)));
	}
}
// System.Object task17/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m76EF3B4BB6FFE23805A22FEB0DDE76A133536635 (U3CstopwatchU3Ed__25_t0F6FB8B99EF73653AFF46D7C747E4FEBF8A1D26E* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void perehodmodul1::Show_Z1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z1_mE89DBDFC63B7DFC79F7757B801AC59C19B5827E3 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA16D90C9F2FF27AD42EBD31D0D6A2DFEF1664E81);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Cubes");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralA16D90C9F2FF27AD42EBD31D0D6A2DFEF1664E81, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Z2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z2_m6F736CBF216742E3E8206BE84D1770F374B244A1 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral412AC43995D234B24609E29CBD8D8CFA87AA8D0A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("The_gardener");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral412AC43995D234B24609E29CBD8D8CFA87AA8D0A, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Z3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z3_m6DC752D9C486992F2461125FAB37A05A77AFC063 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF34425C641201FE6B55C8FCBE7A668330A6033AC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Cubes1");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralF34425C641201FE6B55C8FCBE7A668330A6033AC, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Z4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z4_m34B54AF42B189AB40555F42C25E7A7556E9AC0C0 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB4E68654B7D1C85BED3057BC5DB05DF42FB53BC5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("View_of_the_tower");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralB4E68654B7D1C85BED3057BC5DB05DF42FB53BC5, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Z5()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z5_mEEBDB8AF0F3849C2D586CD2352D8B7FC3B7EE64E (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3D246D5CC5B87BFCC0C03F310D4D97A8C3C477B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Buying_an_apartment");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralE3D246D5CC5B87BFCC0C03F310D4D97A8C3C477B, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Z6()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z6_mB2DF05C533B21235F041F0B04578AB50CC695486 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE18044B8B947F5B124682EBE52BE2E13C5E422D8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Ladder");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralE18044B8B947F5B124682EBE52BE2E13C5E422D8, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Z7()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Z7_mF5832523BC3C4637B12A71C88A2957303313FD7B (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral75388A0FCDF389F8530BFB9F20E55AB8AD7F95F9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Sailing_ships");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral75388A0FCDF389F8530BFB9F20E55AB8AD7F95F9, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_Module()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_Module_mEB554CB9CB8BBB884C20065CFFE1A4EE26BB91B3 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("modules");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral21DC614E11B520E65A3620861532F43FB5A05E40, NULL);
		// r = false;
		il2cpp_codegen_runtime_class_init_inline(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var);
		((perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var))->___r_4 = (bool)0;
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_MainMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_MainMenu_m614162B8305AFF8D9083839F99771620569FB6E3 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("MainMenu");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::Show_zadmodule()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1_Show_zadmodule_m175CF2CEBF36AD62B491E05DFCB8C13E83E416C7 (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC44C020D752222A424F7F9E300FD9D237C95A6CE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Task_of_module_1");
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralC44C020D752222A424F7F9E300FD9D237C95A6CE, NULL);
		// }
		return;
	}
}
// System.Void perehodmodul1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1__ctor_mDE0461E54757B9DF125732C219BF22C48CBD0BFD (perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void perehodmodul1::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void perehodmodul1__cctor_m81F7961CF0BA9A386B5D15F2E47D6F6676699EDE (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static bool r = true;
		((perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_StaticFields*)il2cpp_codegen_static_fields_for(perehodmodul1_t3B423FE2150F686DCA952A79F49D08A9AAEBED5D_il2cpp_TypeInfo_var))->___r_4 = (bool)1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task19::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_Start_m578957EBD321FD3BB4A0F4D33866020F82CED659 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_17;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_18;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task19::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task19_stopwatch_m31B69CC2CBA23466BBFA045D897E57E049C20EA2 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* L_0 = (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__25__ctor_mACB511D54A1CF1255DE5F80BD47A29E5DDA01A24(L_0, 0, NULL);
		U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task19::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_answer_m51AB5D299521D313519AE438D9139F52FA59E02B (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA941ABAA66D5C286D010F3318C47E9E1C1EF5EB9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "212")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralA941ABAA66D5C286D010F3318C47E9E1C1EF5EB9, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_23;
		__this->___mistake_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task19::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_showhint1_m95DD7DD077C362A5A4C655ACCB5887CDE9B5A17B (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_16;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_19;
		__this->___p1_19 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task19::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_notshowhint1_mAD4C6C8D04561767B5863F44E1343D26EE1635E3 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task19::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_showhint2_m04A63664817B3B9523C2F3350067880CFEC2C892 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_20;
		__this->___p2_20 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task19::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_notshowhint2_m6F0E9BA917911CF4EF15C2D206269CDE6D9BAC8B (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task19::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_notshowver_m2C4058021D44679D93BB0F5C2E910B9E9697DD7E (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task19::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_showstatistics_m726AEE438FBC63FEF71968EBF1D104290C6C2432 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_21;
		int32_t* L_5 = (&__this->___p1_19);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_22;
		int32_t* L_8 = (&__this->___p2_20);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_25;
		int32_t* L_11 = (&__this->___mistake_23);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_24;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task19::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_notshowstatistics_m27B568E87D6115A37BF995E53D3A42724F010F17 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task19::nextuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_nextuslzad_m149390164F4FEC7D402BC16D599C0A6525E2A9E7 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_26;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// usl2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_27;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task19::backuslzad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19_backuslzad_mF7E1CEABD221792521E926961A94201B68BAADDB (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		// usl1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___usl1_26;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// usl2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___usl2_27;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task19::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task19__ctor_m4914C174FB30C944160EDC9CAA4A07AB799833C3 (task19_t98E5806FE743E604F715DFA8F534387E08B569CC* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task19/<stopwatch>d__25::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25__ctor_mACB511D54A1CF1255DE5F80BD47A29E5DDA01A24 (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task19/<stopwatch>d__25::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m2DA8529AC2DCD42A557306FC5E0851194E493C31 (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task19/<stopwatch>d__25::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__25_MoveNext_m9330F7700F985C7D2A90AF73453E22C31320A1E5 (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task19_t98E5806FE743E604F715DFA8F534387E08B569CC* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_5 = V_1;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_11 = V_1;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_17 = V_1;
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task19_t98E5806FE743E604F715DFA8F534387E08B569CC* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task19/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4906CE9EDBDEB222D97A4DC80E1A19B0BB6E09A7 (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task19/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m81FC0300E7EA90226FEF616DB832873969C16A32 (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m81FC0300E7EA90226FEF616DB832873969C16A32_RuntimeMethod_var)));
	}
}
// System.Object task19/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m6FEAAF4424650347E2B7B41D23D7579F986CEC3A (U3CstopwatchU3Ed__25_tC81D8DDA8E81C85AC9D6ADDF72256336A4A98DDB* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Growth_Menu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_Start_mEADF62E543FF2798C0E8B9A25A027EB1D925A5E6 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 00;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator Growth_Menu::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Growth_Menu_stopwatch_mCBEF9E4055584D62459871F29F8440B11E27DB38 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* L_0 = (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__89__ctor_m501F56E1C4172B9FDD76399C097A2D1C961CE401(L_0, 0, NULL);
		U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void Growth_Menu::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_showar_m5CC26B8EBC609D737588DACC38C2AE1165DC58C6 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnY1A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnY1A_34;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// btnY2A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnY2A_35;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)0, NULL);
		// btnN4A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_8 = __this->___btnN4A_37;
		NullCheck(L_8);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9;
		L_9 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_8, NULL);
		NullCheck(L_9);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_9, (bool)0, NULL);
		// btnN3A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_10 = __this->___btnN3A_36;
		NullCheck(L_10);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11;
		L_11 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_10, NULL);
		NullCheck(L_11);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_11, (bool)1, NULL);
		// btnY1B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_12 = __this->___btnY1B_38;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)1, NULL);
		// btnY2B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_14 = __this->___btnY2B_39;
		NullCheck(L_14);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15;
		L_15 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_14, NULL);
		NullCheck(L_15);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_15, (bool)0, NULL);
		// btnN4B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_16 = __this->___btnN4B_41;
		NullCheck(L_16);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_17;
		L_17 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_16, NULL);
		NullCheck(L_17);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_17, (bool)0, NULL);
		// btnN3B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_18 = __this->___btnN3B_40;
		NullCheck(L_18);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19;
		L_19 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_18, NULL);
		NullCheck(L_19);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_19, (bool)1, NULL);
		// btnY1C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_20 = __this->___btnY1C_42;
		NullCheck(L_20);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_21;
		L_21 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_20, NULL);
		NullCheck(L_21);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_21, (bool)1, NULL);
		// btnY2C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_22 = __this->___btnY2C_43;
		NullCheck(L_22);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23;
		L_23 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_22, NULL);
		NullCheck(L_23);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_23, (bool)0, NULL);
		// btnN4C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_24 = __this->___btnN4C_45;
		NullCheck(L_24);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25;
		L_25 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_24, NULL);
		NullCheck(L_25);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_25, (bool)0, NULL);
		// btnN3C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_26 = __this->___btnN3C_44;
		NullCheck(L_26);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27;
		L_27 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_26, NULL);
		NullCheck(L_27);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_27, (bool)1, NULL);
		// btnY1D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_28 = __this->___btnY1D_46;
		NullCheck(L_28);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29;
		L_29 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_28, NULL);
		NullCheck(L_29);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_29, (bool)1, NULL);
		// btnY2D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_30 = __this->___btnY2D_47;
		NullCheck(L_30);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_31;
		L_31 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_30, NULL);
		NullCheck(L_31);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_31, (bool)0, NULL);
		// btnN4D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_32 = __this->___btnN4D_49;
		NullCheck(L_32);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_33;
		L_33 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_32, NULL);
		NullCheck(L_33);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_33, (bool)0, NULL);
		// btnN3D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_34 = __this->___btnN3D_48;
		NullCheck(L_34);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_35;
		L_35 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_34, NULL);
		NullCheck(L_35);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_35, (bool)1, NULL);
		// YesA = false;
		__this->___YesA_50 = (bool)0;
		// NoA = false;
		__this->___NoA_51 = (bool)0;
		// YesB = false;
		__this->___YesB_52 = (bool)0;
		// NoB = false;
		__this->___NoB_53 = (bool)0;
		// YesC = false;
		__this->___YesC_54 = (bool)0;
		// NoC = false;
		__this->___NoC_55 = (bool)0;
		// YesD = false;
		__this->___YesD_56 = (bool)0;
		// NoD = false;
		__this->___NoD_57 = (bool)0;
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForYesA()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForYesA_mC9159BABB08FBC512E44271E982F6CC859EE6FAA (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnY1A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnY1A_34;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// YesA = true;
		__this->___YesA_50 = (bool)1;
		// btnY2A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnY2A_35;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnN4A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnN4A_37;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnN3A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnN3A_36;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForNoA()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForNoA_m063A46B1352462F3348389A05EDC1825F44F51E5 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnN3A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnN3A_36;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// NoA = false;
		__this->___NoA_51 = (bool)0;
		// btnN4A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnN4A_37;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnY2A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnY2A_35;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnY1A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnY1A_34;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForYesB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForYesB_m3A99D4C1B2570AE7DDECDB44E42C1EF174342963 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnY1B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnY1B_38;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// YesB = false;
		__this->___YesB_52 = (bool)0;
		// btnY2B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnY2B_39;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnN4B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnN4B_41;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnN3B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnN3B_40;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForNoB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForNoB_m2457BCAC4BC5E78B0E1ABD8B7E9E78C21D84B0D2 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnN3B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnN3B_40;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// btnN4B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnN4B_41;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// NoB = true;
		__this->___NoB_53 = (bool)1;
		// btnY2B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnY2B_39;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnY1B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnY1B_38;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForYesC()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForYesC_m52434244BDFAC42A3A1CC74C7DFEA3F318CA1385 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnY1C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnY1C_42;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// YesC = true;
		__this->___YesC_54 = (bool)1;
		// btnY2C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnY2C_43;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnN4C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnN4C_45;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnN3C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnN3C_44;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForNoC()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForNoC_mAA0B48C321A261A82BAD671BD475F91E36CA34A3 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnN3C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnN3C_44;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// NoC = false;
		__this->___NoC_55 = (bool)0;
		// btnN4C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnN4C_45;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnY2C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnY2C_43;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnY1C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnY1C_42;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForYesD()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForYesD_m09017F7CA9490B48E89B2B2CE4BAB4D352A54A34 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnY1D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnY1D_46;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// YesD = true;
		__this->___YesD_56 = (bool)1;
		// btnY2D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnY2D_47;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnN4D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnN4D_49;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnN3D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnN3D_48;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::ChangeColorForNoD()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ChangeColorForNoD_mF192296DF26FF74EC55658EEF7009325D44AC069 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// btnN3D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___btnN3D_48;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// NoD = false;
		__this->___NoD_57 = (bool)0;
		// btnN4D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___btnN4D_49;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// btnY2D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___btnY2D_47;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// btnY1D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___btnY1D_46;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_answer_m9248041B9D2C21692F3F39E491BE4949604B15DA (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((YesA == true) && (NoB == true) && (YesC == true) && (YesD == true))
		bool L_0 = __this->___YesA_50;
		if (!L_0)
		{
			goto IL_005a;
		}
	}
	{
		bool L_1 = __this->___NoB_53;
		if (!L_1)
		{
			goto IL_005a;
		}
	}
	{
		bool L_2 = __this->___YesC_54;
		if (!L_2)
		{
			goto IL_005a;
		}
	}
	{
		bool L_3 = __this->___YesD_56;
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___ok_8;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___prov_9;
		NullCheck(L_6);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___stat_11;
		NullCheck(L_7);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8;
		L_8 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_7, NULL);
		NullCheck(L_8);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_8, (bool)1, NULL);
		return;
	}

IL_005a:
	{
		// mistake++;
		int32_t L_9 = __this->___mistake_31;
		__this->___mistake_31 = ((int32_t)il2cpp_codegen_add(L_9, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___ok_8;
		NullCheck(L_10);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11;
		L_11 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_10, NULL);
		NullCheck(L_11);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_11, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_12 = __this->___prov_9;
		NullCheck(L_12);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = __this->___stat_11;
		NullCheck(L_13);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14;
		L_14 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_13, NULL);
		NullCheck(L_14);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_14, (bool)0, NULL);
		// btnY1A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_15 = __this->___btnY1A_34;
		NullCheck(L_15);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16;
		L_16 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_15, NULL);
		NullCheck(L_16);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_16, (bool)1, NULL);
		// btnY2A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_17 = __this->___btnY2A_35;
		NullCheck(L_17);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
		L_18 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_17, NULL);
		NullCheck(L_18);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_18, (bool)0, NULL);
		// btnN4A.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_19 = __this->___btnN4A_37;
		NullCheck(L_19);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20;
		L_20 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_19, NULL);
		NullCheck(L_20);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_20, (bool)0, NULL);
		// btnN3A.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_21 = __this->___btnN3A_36;
		NullCheck(L_21);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
		L_22 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_21, NULL);
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// btnY1B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_23 = __this->___btnY1B_38;
		NullCheck(L_23);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_24;
		L_24 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_23, NULL);
		NullCheck(L_24);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_24, (bool)1, NULL);
		// btnY2B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_25 = __this->___btnY2B_39;
		NullCheck(L_25);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26;
		L_26 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_25, NULL);
		NullCheck(L_26);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_26, (bool)0, NULL);
		// btnN4B.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_27 = __this->___btnN4B_41;
		NullCheck(L_27);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_27, NULL);
		NullCheck(L_28);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_28, (bool)0, NULL);
		// btnN3B.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_29 = __this->___btnN3B_40;
		NullCheck(L_29);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30;
		L_30 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_29, NULL);
		NullCheck(L_30);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_30, (bool)1, NULL);
		// btnY1C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_31 = __this->___btnY1C_42;
		NullCheck(L_31);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_32;
		L_32 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_31, NULL);
		NullCheck(L_32);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_32, (bool)1, NULL);
		// btnY2C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_33 = __this->___btnY2C_43;
		NullCheck(L_33);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_34;
		L_34 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_33, NULL);
		NullCheck(L_34);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_34, (bool)0, NULL);
		// btnN4C.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_35 = __this->___btnN4C_45;
		NullCheck(L_35);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_36;
		L_36 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_35, NULL);
		NullCheck(L_36);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_36, (bool)0, NULL);
		// btnN3C.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_37 = __this->___btnN3C_44;
		NullCheck(L_37);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_38;
		L_38 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_37, NULL);
		NullCheck(L_38);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_38, (bool)1, NULL);
		// btnY1D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_39 = __this->___btnY1D_46;
		NullCheck(L_39);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40;
		L_40 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_39, NULL);
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// btnY2D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_41 = __this->___btnY2D_47;
		NullCheck(L_41);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_42;
		L_42 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_41, NULL);
		NullCheck(L_42);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_42, (bool)0, NULL);
		// btnN4D.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_43 = __this->___btnN4D_49;
		NullCheck(L_43);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_44;
		L_44 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_43, NULL);
		NullCheck(L_44);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_44, (bool)0, NULL);
		// btnN3D.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_45 = __this->___btnN3D_48;
		NullCheck(L_45);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_46;
		L_46 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_45, NULL);
		NullCheck(L_46);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_46, (bool)1, NULL);
		// YesA = false;
		__this->___YesA_50 = (bool)0;
		// NoA = false;
		__this->___NoA_51 = (bool)0;
		// YesB = false;
		__this->___YesB_52 = (bool)0;
		// NoB = false;
		__this->___NoB_53 = (bool)0;
		// YesC = false;
		__this->___YesC_54 = (bool)0;
		// NoC = false;
		__this->___NoC_55 = (bool)0;
		// YesD = false;
		__this->___YesD_56 = (bool)0;
		// NoD = false;
		__this->___NoD_57 = (bool)0;
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnHelpBig()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnHelpBig_m8A2B0A77DE20966DE22342C198C067A8F7598A5D (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// BtnHelpD.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___BtnHelpD_64;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// BtnHelpC.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___BtnHelpC_62;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpB.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpB_60;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// BtnHelpA.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___BtnHelpA_58;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// BtnHelpuse.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_8 = __this->___BtnHelpuse_75;
		NullCheck(L_8);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9;
		L_9 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_8, NULL);
		NullCheck(L_9);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_9, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpA()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpA_mBFA289F7265C12241714C7BF7AA03B14E89BDFF3 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelA.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelA_76;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// BtnHelpAuse.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___BtnHelpAuse_67;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpA1.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpA1_59;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p1++;
		int32_t L_6 = __this->___p1_15;
		__this->___p1_15 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpA1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpA1_mA3EB874CA2332561963F3385455951D7B4C4A2E5 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelA.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelA_76;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// PanelA1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PanelA1_77;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpA1use.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpA1use_68;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_16;
		__this->___p2_16 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpB_mDD5454E7A8E966686ED687B784ADC9D9B59BC3FF (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// BtnHelpB1.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___BtnHelpB1_61;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// BtnHelpBuse.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___BtnHelpBuse_69;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// PanelB.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___PanelB_78;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_17;
		__this->___p3_17 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpB1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpB1_m1395C5771BE0BE27BAE8BEA57886EDBE02EB1421 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelB.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelB_78;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// PanelB1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PanelB1_79;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpB1use.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpB1use_70;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p4++;
		int32_t L_6 = __this->___p4_18;
		__this->___p4_18 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpC()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpC_m3BC08A645A3A2F9DFC7307CDBF4C6CC9F22A3EFA (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// BtnHelpC1.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_0 = __this->___BtnHelpC1_63;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// PanelC.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PanelC_80;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpCuse.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpCuse_71;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p5++;
		int32_t L_6 = __this->___p5_19;
		__this->___p5_19 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpC1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpC1_m0289494A20BBCE4AA1FEFB0717F33246A782CE40 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelC.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelC_80;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// PanelC1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PanelC1_81;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpC1use.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpC1use_72;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p6++;
		int32_t L_6 = __this->___p6_20;
		__this->___p6_20 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpD()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpD_m7BC333394DDE8D84B35840A2F5DFC281E2F21118 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelD.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelD_82;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// BtnHelpD1.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_2 = __this->___BtnHelpD1_65;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpDuse.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpDuse_73;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p7++;
		int32_t L_6 = __this->___p7_21;
		__this->___p7_21 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::BtnForHelpD1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_BtnForHelpD1_m6F13A6854F26F5B940A1EE48F0C6B1A1869C6197 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelD.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelD_82;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// PanelD1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PanelD1_83;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// BtnHelpD1use.gameObject.SetActive(true);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpD1use_74;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p8++;
		int32_t L_6 = __this->___p8_22;
		__this->___p8_22 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void Growth_Menu::ForBtnOKad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_ForBtnOKad_m9DC2299B3309FC5ED9D731E05B14215D165F146E (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// PanelA.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___PanelA_76;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// PanelA1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PanelA1_77;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// PanelB.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___PanelB_78;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// PanelB1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___PanelB1_79;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)0, NULL);
		// PanelC.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = __this->___PanelC_80;
		NullCheck(L_8);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9;
		L_9 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_8, NULL);
		NullCheck(L_9);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_9, (bool)0, NULL);
		// PanelC1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___PanelC1_81;
		NullCheck(L_10);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11;
		L_11 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_10, NULL);
		NullCheck(L_11);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_11, (bool)0, NULL);
		// PanelD.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___PanelD_82;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// PanelD1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14 = __this->___PanelD1_83;
		NullCheck(L_14);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15;
		L_15 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_14, NULL);
		NullCheck(L_15);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_15, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_showstatistics_m9A2740F18A101613AFB48ED4C71EF3278696FC23 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original1_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_23;
		int32_t* L_5 = (&__this->___p1_15);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_24;
		int32_t* L_8 = (&__this->___p2_16);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_25;
		int32_t* L_11 = (&__this->___p3_17);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// pod4.text = p4.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___pod4_26;
		int32_t* L_14 = (&__this->___p4_18);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// pod5.text = p5.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___pod5_27;
		int32_t* L_17 = (&__this->___p5_19);
		String_t* L_18;
		L_18 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_17, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_18);
		// pod6.text = p6.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_19 = __this->___pod6_28;
		int32_t* L_20 = (&__this->___p6_20);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_19, L_21);
		// pod7.text = p7.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_22 = __this->___pod7_29;
		int32_t* L_23 = (&__this->___p7_21);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_22, L_24);
		// pod8.text = p8.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_25 = __this->___pod8_30;
		int32_t* L_26 = (&__this->___p8_22);
		String_t* L_27;
		L_27 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_26, NULL);
		NullCheck(L_25);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_25, L_27);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_28 = __this->___textoshibka_33;
		int32_t* L_29 = (&__this->___mistake_31);
		String_t* L_30;
		L_30 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_29, NULL);
		NullCheck(L_28);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_28, L_30);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_31 = __this->___texttime_32;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_32 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_33 = L_32;
		int32_t* L_34 = (&__this->___time_Ch_6);
		String_t* L_35;
		L_35 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_34, NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_35);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_36 = L_33;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_37 = L_36;
		int32_t* L_38 = (&__this->___time_M_5);
		String_t* L_39;
		L_39 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_38, NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_39);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_39);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_40 = L_37;
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_41 = L_40;
		int32_t* L_42 = (&__this->___time_S_4);
		String_t* L_43;
		L_43 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_42, NULL);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_43);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_43);
		String_t* L_44;
		L_44 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_41, NULL);
		NullCheck(L_31);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_31, L_44);
		// }
		return;
	}
}
// System.Void Growth_Menu::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_notshowar_mA977602AF772B62B37D90897D6EC98A5E6E0BEBE (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// BtnHelpAuse.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_4 = __this->___BtnHelpAuse_67;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// BtnHelpA1use.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_6 = __this->___BtnHelpA1use_68;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)0, NULL);
		// BtnHelpBuse.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_8 = __this->___BtnHelpBuse_69;
		NullCheck(L_8);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9;
		L_9 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_8, NULL);
		NullCheck(L_9);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_9, (bool)0, NULL);
		// BtnHelpB1use.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_10 = __this->___BtnHelpB1use_70;
		NullCheck(L_10);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11;
		L_11 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_10, NULL);
		NullCheck(L_11);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_11, (bool)0, NULL);
		// BtnHelpCuse.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_12 = __this->___BtnHelpCuse_71;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// BtnHelpC1use.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_14 = __this->___BtnHelpC1use_72;
		NullCheck(L_14);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15;
		L_15 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_14, NULL);
		NullCheck(L_15);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_15, (bool)0, NULL);
		// BtnHelpDuse.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_16 = __this->___BtnHelpDuse_73;
		NullCheck(L_16);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_17;
		L_17 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_16, NULL);
		NullCheck(L_17);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_17, (bool)0, NULL);
		// BtnHelpD1use.gameObject.SetActive(false);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_18 = __this->___BtnHelpD1use_74;
		NullCheck(L_18);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19;
		L_19 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_18, NULL);
		NullCheck(L_19);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_19, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu_notshowver_m5917AC51417687872696FB2DEB60BC7A7603B475 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___ok_8;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Growth_Menu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Growth_Menu__ctor_m6DF50EED17A3ACA94D920E21E60DBA47F4A0EE31 (Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Growth_Menu/<stopwatch>d__89::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__89__ctor_m501F56E1C4172B9FDD76399C097A2D1C961CE401 (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Growth_Menu/<stopwatch>d__89::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__89_System_IDisposable_Dispose_mCAA189524DF4478AE19409A5A44702805A223AFC (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean Growth_Menu/<stopwatch>d__89::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__89_MoveNext_mAA6962B9EE57E075023054D41FE31EE0F0901BE5 (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_5 = V_1;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 00;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_11 = V_1;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 00;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_17 = V_1;
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		Growth_Menu_t6788A22ED5E343B19C4C031343C5D4CEDA8107AF* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Growth_Menu/<stopwatch>d__89::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB12092191FCC417B85E6D13C38F06E3CA2C4F11 (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Growth_Menu/<stopwatch>d__89::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__89_System_Collections_IEnumerator_Reset_m7021A424310BDF3F5810B457A25C99FCC22AC91C (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__89_System_Collections_IEnumerator_Reset_m7021A424310BDF3F5810B457A25C99FCC22AC91C_RuntimeMethod_var)));
	}
}
// System.Object Growth_Menu/<stopwatch>d__89::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__89_System_Collections_IEnumerator_get_Current_mD7A472DCB5A7C94F8F0C339FD81B4D4942CE2B60 (U3CstopwatchU3Ed__89_t4B365AD523FE03A52213BF98477986A781A5C381* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task10::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_Start_m881E5E8DBDA0422AB51A87880988BED1C1646A83 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_21;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_22;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// podskaz3use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___podskaz3use_23;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task10::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task10_stopwatch_mC1E54BEF6E6D680A676099BC00F6CB0FFDE8D2EF (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* L_0 = (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__30__ctor_m67EEB770039EA89A16B5BE37CD3B2B4A0E1380EE(L_0, 0, NULL);
		U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task10::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_answer_m2714F5DEF614520D613DA5388F86E335E1C07FF4 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF63DF0A24E87ED583133D4C27862F85ABB36A8F5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "43122")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_9;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralF63DF0A24E87ED583133D4C27862F85ABB36A8F5, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_10;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_11;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_12;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_30;
		__this->___mistake_30 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_10;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_11;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_12;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_showhint1_m016FDB31177588D8017CBC318EAEDF9ED49B6B1C (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_21;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_19;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_24;
		__this->___p1_24 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task10::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_notshowhint1_m1E83AC7BCD63D2B8D78BCE3F2EE82DC2A2DA1E1A (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_showhint2_m85A01E4C96873825FADE814890F2E9746B7F99C3 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_22;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz3_20;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p2++;
		int32_t L_8 = __this->___p2_25;
		__this->___p2_25 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task10::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_notshowhint2_mB19713F87318AEB083D8887473DE12A5A62AB681 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::showhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_showhint3_mCCBB67DF9DF31B118F4F64CB1A5CCA4E8BE8B955 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_17;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3use_23;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_26;
		__this->___p3_26 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task10::notshowhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_notshowhint3_m276ACA285A05741EC0F44444D1DEF98A7AB50CD4 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint3.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_17;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_notshowver_m469F61FCCD2F2E0ED2C423A8954D610C081D0428 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_10;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_showstatistics_m803B3B5FAF53D7F7700B6D94DD2D0F79D0855389 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_27;
		int32_t* L_5 = (&__this->___p1_24);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_28;
		int32_t* L_8 = (&__this->___p2_25);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_29;
		int32_t* L_11 = (&__this->___p3_26);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___textoshibka_32;
		int32_t* L_14 = (&__this->___mistake_30);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___texttime_31;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		int32_t* L_19 = (&__this->___time_Ch_6);
		String_t* L_20;
		L_20 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_19, NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_20);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_18;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		int32_t* L_23 = (&__this->___time_M_5);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		int32_t* L_27 = (&__this->___time_S_4);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_28);
		String_t* L_29;
		L_29 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_26, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_29);
		// }
		return;
	}
}
// System.Void task10::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_notshowstatistics_m29821EB3DB8376DB483A0E15E250B22753147856 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::showar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_showar_m59E636D6CACBE026F2D1A96AC91A71A3F9B63D9A (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// ar.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void task10::notshowar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10_notshowar_m839EEFF3495CABC264E871F2DE1E55F22747417F (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_13;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ar.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ar_8;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task10::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task10__ctor_m56565A0D78CD005D9800EAAE57F8F997D2A68B52 (task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task10/<stopwatch>d__30::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__30__ctor_m67EEB770039EA89A16B5BE37CD3B2B4A0E1380EE (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task10/<stopwatch>d__30::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__30_System_IDisposable_Dispose_m83A03F429A791E0CA6455A76F3A3DE23B3927443 (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task10/<stopwatch>d__30::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__30_MoveNext_m50FFCCCA93FA8AD29B213688A13F6770A0364938 (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_5 = V_1;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_11 = V_1;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_17 = V_1;
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task10_tDE5028C32B8C330404B0B6888C2780FAC891472F* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task10/<stopwatch>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4A896137509D01794355AE9276BC4B4A1703102 (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task10/<stopwatch>d__30::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__30_System_Collections_IEnumerator_Reset_m5E7FA5648CD08E9E609369D53888336719995879 (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__30_System_Collections_IEnumerator_Reset_m5E7FA5648CD08E9E609369D53888336719995879_RuntimeMethod_var)));
	}
}
// System.Object task10/<stopwatch>d__30::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__30_System_Collections_IEnumerator_get_Current_m77EF3C48D26D8097CC84976ECEF616B5F6B40478 (U3CstopwatchU3Ed__30_t840E4CD17F627D6833A1D7254BE4A92D41C1A5C0* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task8::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_Start_m151B9EC6287C8B230F68D4250E17BF845CF8114B (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_17;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_18;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task8::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task8_stopwatch_m792AAF04C893B1D23E2967D06C904366B49545AA (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* L_0 = (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__23__ctor_m5B9E8360E8BACEF61B8A182C111973FD1348B85A(L_0, 0, NULL);
		U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task8::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_answer_m77D1CD75AF48DD0420B7EED302B1156548C7045B (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral521FB55423CC068B92B5E8453A36DF0D6CBDD93F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "20")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral521FB55423CC068B92B5E8453A36DF0D6CBDD93F, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_23;
		__this->___mistake_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task8::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_showhint1_mA14CDB363E25AFEAEA22031658201A7EA6E4F13E (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_17;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_16;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_19;
		__this->___p1_19 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task8::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_notshowhint1_m57B0BBD2E2B7CECF50B1E450615C95FECDD4E24E (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task8::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_showhint2_mC544FED42165AFBCE089988EAE2A7619E435FFEE (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz2use_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p2++;
		int32_t L_6 = __this->___p2_20;
		__this->___p2_20 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task8::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_notshowhint2_m30584DCE942668BF0B85CE5B5FDBB68E3700696B (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task8::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_notshowver_m4321DD9DB476DFB7861ECD347F76FC47B9AF0E3B (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task8::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_showstatistics_m729627A2779BE7B78A079DAB4A2E370CD656751B (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_21;
		int32_t* L_5 = (&__this->___p1_19);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_22;
		int32_t* L_8 = (&__this->___p2_20);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___textoshibka_25;
		int32_t* L_11 = (&__this->___mistake_23);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___texttime_24;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		int32_t* L_16 = (&__this->___time_Ch_6);
		String_t* L_17;
		L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_16, NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_17);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		int32_t* L_20 = (&__this->___time_M_5);
		String_t* L_21;
		L_21 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_20, NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_21);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
		int32_t* L_24 = (&__this->___time_S_4);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_24, NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		String_t* L_26;
		L_26 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_23, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_26);
		// }
		return;
	}
}
// System.Void task8::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8_notshowstatistics_mF21D153ECFE1D7846025279475326E493B0638DD (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task8::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task8__ctor_mAC9B9EF8027C9FB1386EBF64DE5352E2BD98921F (task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task8/<stopwatch>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23__ctor_m5B9E8360E8BACEF61B8A182C111973FD1348B85A (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task8/<stopwatch>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_mC5B5F01A01B2C74E8F59E85D83BEE98A0A848C71 (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task8/<stopwatch>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__23_MoveNext_m387CFACA3B4E273FDE11DBBA5841FF7E45316815 (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_5 = V_1;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_11 = V_1;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_17 = V_1;
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task8_t3309CF5C6BE318EE39F8E45733F3D9081DDF0061* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task8/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DC2068FC9E25C3F02C41FD773CDD13A2A496AF9 (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task8/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m3DD586B5B64D48A3E26DE70E99D865E74BC6B68E (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m3DD586B5B64D48A3E26DE70E99D865E74BC6B68E_RuntimeMethod_var)));
	}
}
// System.Object task8/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_mFC8554F3B08E90BC922E05C865A02425975775C8 (U3CstopwatchU3Ed__23_t979B6A0007DE9969A9B379926C3604485210F038* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task5::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_Start_mF7BA4D594AE21B29261701C1CC98C1766AC7ED09 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time_S = 0;
		__this->___time_S_4 = 0;
		// stop = true;
		__this->___stop_7 = (bool)1;
		// StartCoroutine("stopwatch");
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0;
		L_0 = MonoBehaviour_StartCoroutine_m10C4B693B96175C42B0FD00911E072701C220DB4(__this, _stringLiteral6CD70A82940B82673221AE70AE0714800025D4A6, NULL);
		// podskaz1use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___podskaz1use_19;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// podskaz2use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___podskaz2use_20;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)0, NULL);
		// podskaz3use.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___podskaz3use_21;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator task5::stopwatch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* task5_stopwatch_m8F9D0516FDC32240E288A60CEBD9F88F6BC8CB9D (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* L_0 = (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170*)il2cpp_codegen_object_new(U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CstopwatchU3Ed__28__ctor_mD77B2A6660FF72176CB31E51282D3F5FB5D3E06B(L_0, 0, NULL);
		U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void task5::answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_answer_mAD95124A6748BAAC990A8F2958F3AAC3526C869D (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1DC9F6BC39F00E092B0BEEEEE189243DA24E1C20);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Answ.text == "64")
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___Answ_8;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteral1DC9F6BC39F00E092B0BEEEEE189243DA24E1C20, NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		// stop = false;
		__this->___stop_7 = (bool)0;
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___ok_9;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_3, NULL);
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// prov.text = "?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_5 = __this->___prov_10;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteralEE5434E8C9B96AEBFCC55521ABD8A12566C4A141);
		// stat.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___stat_11;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		return;
	}

IL_0051:
	{
		// mistake++;
		int32_t L_8 = __this->___mistake_28;
		__this->___mistake_28 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// ok.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___ok_9;
		NullCheck(L_9);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_9, NULL);
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
		// prov.text = "?? ?????";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11 = __this->___prov_10;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral283150161DF7A339C4006A3C360664CFACE3EBE9);
		// stat.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___stat_11;
		NullCheck(L_12);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_12, NULL);
		NullCheck(L_13);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_13, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task5::showhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_showhint1_m136196EEAD1ABC9EBBBCF5ED516581D5C752FE86 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint1.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz1use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz1use_19;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2_17;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p1++;
		int32_t L_8 = __this->___p1_22;
		__this->___p1_22 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task5::notshowhint1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_notshowhint1_m10BB18B54F72282A630AC511AE59DF67D30CFEA6 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint1.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint1_14;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task5::showhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_showhint2_m8D36872BDA39EA1CF59F751AF65B912EE6F7B822 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint2.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3_18;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// podskaz2use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___podskaz2use_20;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)1, NULL);
		// p2++;
		int32_t L_8 = __this->___p2_23;
		__this->___p2_23 = ((int32_t)il2cpp_codegen_add(L_8, 1));
		// }
		return;
	}
}
// System.Void task5::notshowhint2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_notshowhint2_mEAF4E26E38C44F1AA8C26D98E199A7535F1E56E0 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint2.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint2_15;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task5::showhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_showhint3_m86702356C7F32003C0C04C5F533A4EE183F930AB (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// hint3.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// podskaz3use.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___podskaz3use_21;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// p3++;
		int32_t L_6 = __this->___p3_24;
		__this->___p3_24 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		// }
		return;
	}
}
// System.Void task5::notshowhint3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_notshowhint3_m012E12286EE4661FC2681BD038D61F359592A372 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// hint3.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___hint3_16;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task5::notshowver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_notshowver_mE5CDA86BAF719935FEDCA564D6EA213B1EC3F626 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// ok.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___ok_9;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task5::showstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_showstatistics_m8631BD548F01336041D16CFBDBB8918333D64365 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// original.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// statistics.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)1, NULL);
		// pod1.text = p1.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_4 = __this->___pod1_25;
		int32_t* L_5 = (&__this->___p1_22);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_4, L_6);
		// pod2.text = p2.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_7 = __this->___pod2_26;
		int32_t* L_8 = (&__this->___p2_23);
		String_t* L_9;
		L_9 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_8, NULL);
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_7, L_9);
		// pod3.text = p3.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_10 = __this->___pod3_27;
		int32_t* L_11 = (&__this->___p3_24);
		String_t* L_12;
		L_12 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_11, NULL);
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_10, L_12);
		// textoshibka.text = mistake.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_13 = __this->___textoshibka_30;
		int32_t* L_14 = (&__this->___mistake_28);
		String_t* L_15;
		L_15 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_14, NULL);
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_13, L_15);
		// texttime.text = time_Ch.ToString() + ':' + time_M.ToString() + ':' + time_S.ToString();
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_16 = __this->___texttime_29;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		int32_t* L_19 = (&__this->___time_Ch_6);
		String_t* L_20;
		L_20 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_19, NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_20);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_18;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		int32_t* L_23 = (&__this->___time_M_5);
		String_t* L_24;
		L_24 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		int32_t* L_27 = (&__this->___time_S_4);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_28);
		String_t* L_29;
		L_29 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_26, NULL);
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_16, L_29);
		// }
		return;
	}
}
// System.Void task5::notshowstatistics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5_notshowstatistics_mD8FE95CFAAF4317CEA4C1FEB8F2F28C49D3826E6 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		// original.gameObject.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___original_12;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// statistics.gameObject.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___statistics_13;
		NullCheck(L_2);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_get_gameObject_m0878015B8CF7F5D432B583C187725810D27B57DC(L_2, NULL);
		NullCheck(L_3);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_3, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void task5::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void task5__ctor_m0DFF264E71F3228A76283FF05DB5204F5AFBD7A1 (task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void task5/<stopwatch>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28__ctor_mD77B2A6660FF72176CB31E51282D3F5FB5D3E06B (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void task5/<stopwatch>d__28::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mEBE25A514B0A74F3AA2E84D18F4DB811D942CF11 (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean task5/<stopwatch>d__28::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CstopwatchU3Ed__28_MoveNext_m2461C28DD63283CDBA86482258B75733CA4DD368 (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008c;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(1f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (1.0f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// time_S++;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_5 = V_1;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = L_6->___time_S_4;
		NullCheck(L_5);
		L_5->___time_S_4 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// if (time_S == 60)
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->___time_S_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_006d;
		}
	}
	{
		// time_S = 0;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_10 = V_1;
		NullCheck(L_10);
		L_10->___time_S_4 = 0;
		// time_M++;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_11 = V_1;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->___time_M_5;
		NullCheck(L_11);
		L_11->___time_M_5 = ((int32_t)il2cpp_codegen_add(L_13, 1));
	}

IL_006d:
	{
		// if (time_M == 60)
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->___time_M_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_008c;
		}
	}
	{
		// time_M = 0;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_16 = V_1;
		NullCheck(L_16);
		L_16->___time_M_5 = 0;
		// time_Ch++;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_17 = V_1;
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->___time_Ch_6;
		NullCheck(L_17);
		L_17->___time_Ch_6 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_008c:
	{
		// while (stop)
		task5_t3AC3009978ED525D2AD871FE37C21C2FD6842023* L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->___stop_7;
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object task5/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8217D2CBE113C96D6D5B16660EE4C488BDAD3F2 (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void task5/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m740F6D1D83CC0F796D56F666CA6391FE4AE06534 (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m740F6D1D83CC0F796D56F666CA6391FE4AE06534_RuntimeMethod_var)));
	}
}
// System.Object task5/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mCFA409562E12000D407977266C8AEF134C99D96D (U3CstopwatchU3Ed__28_t9D6DE9FDF8BD599A00987351151FD70677F03170* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) 
{
	{
		// return m_Value;
		int32_t L_0 = __this->___m_Value_25;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline (InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* __this, const RuntimeMethod* method) 
{
	{
		// return m_Text;
		String_t* L_0 = __this->___m_Text_41;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
