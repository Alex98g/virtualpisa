﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void task18::Start()
extern void task18_Start_m1AAE1EFFA5315271B1197642276249E743BF3C70 (void);
// 0x00000002 System.Collections.IEnumerator task18::stopwatch()
extern void task18_stopwatch_mEA8809D2BEF7BC2CE791218486EE678C51AC41BA (void);
// 0x00000003 System.Void task18::answer()
extern void task18_answer_m30E40F26D9BAC45E527BE61F623C1F1D2A571C43 (void);
// 0x00000004 System.Void task18::showhint1()
extern void task18_showhint1_mB1C3D2D881AE78E4D5A55D7602937E52116D0D20 (void);
// 0x00000005 System.Void task18::notshowhint1()
extern void task18_notshowhint1_m6E9DD3A71239F426A8383961B87C3D52F9F6453E (void);
// 0x00000006 System.Void task18::notshowver()
extern void task18_notshowver_mFD72CD1A5C4BB5913AAA58AAD9FC8401C5F516C5 (void);
// 0x00000007 System.Void task18::showstatistics()
extern void task18_showstatistics_m5650CA72DE99959FAE28FB35712EF51F25181E0F (void);
// 0x00000008 System.Void task18::notshowstatistics()
extern void task18_notshowstatistics_mBD2FC50E4562BB95666567B0F226BEF0F9C5D5A9 (void);
// 0x00000009 System.Void task18::.ctor()
extern void task18__ctor_mEF834A6BC1981BA55DD06B3CD1A094F22EC4A6E2 (void);
// 0x0000000A System.Void task18/<stopwatch>d__18::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__18__ctor_m9C98BAF99D3194A475B10F43E12EC1467D6EF023 (void);
// 0x0000000B System.Void task18/<stopwatch>d__18::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__18_System_IDisposable_Dispose_m03C6C59F8C3C0FF5A36144D3BDC693CFA69061C4 (void);
// 0x0000000C System.Boolean task18/<stopwatch>d__18::MoveNext()
extern void U3CstopwatchU3Ed__18_MoveNext_m42F5DFE4E1380ADEACAD6EEC6699B18FED7C83EC (void);
// 0x0000000D System.Object task18/<stopwatch>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E33CCBBE0AD7F1388E75C63050C728C23B1329B (void);
// 0x0000000E System.Void task18/<stopwatch>d__18::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m4A765E673B465AB278707B6FF492C7595152324E (void);
// 0x0000000F System.Object task18/<stopwatch>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__18_System_Collections_IEnumerator_get_Current_m3E57CA1A69F2ED7DA1C3B19DD22899A8C13ECB93 (void);
// 0x00000010 System.Void task1::Start()
extern void task1_Start_m3CDDCAABF1E9FCDF93D124E6F74AB1B5DDC0A55A (void);
// 0x00000011 System.Collections.IEnumerator task1::stopwatch()
extern void task1_stopwatch_mAC140E804336901EE2D25D9A2D6C6CBE8A96CB4E (void);
// 0x00000012 System.Void task1::answer()
extern void task1_answer_m03F536155A966C9965198798BCCD295C25E3B687 (void);
// 0x00000013 System.Void task1::showhint1()
extern void task1_showhint1_m4C4905A5980A8F60285FD3937709A023A990762B (void);
// 0x00000014 System.Void task1::notshowhint1()
extern void task1_notshowhint1_m58B495C81C4CF335F65500D9336CC32FA14DA61F (void);
// 0x00000015 System.Void task1::showhint2()
extern void task1_showhint2_m189F7CF98EA1C9A47263D3EE311530951C3B790F (void);
// 0x00000016 System.Void task1::notshowhint2()
extern void task1_notshowhint2_m745C6BE79B610C92FE18E83DCD2923D3F6A680ED (void);
// 0x00000017 System.Void task1::notshowver()
extern void task1_notshowver_mBD486C906F67837E25A8836358EB6D8691D6006C (void);
// 0x00000018 System.Void task1::showstatistics()
extern void task1_showstatistics_m9425EFA1B3BFBE3369ADB102FE515E869153C96A (void);
// 0x00000019 System.Void task1::notshowstatistics()
extern void task1_notshowstatistics_m97C72FEA1BDB8AB2B05F178ED137E45C89F06598 (void);
// 0x0000001A System.Void task1::showar()
extern void task1_showar_m3156D703A467152E915900D8F55103D2884093D0 (void);
// 0x0000001B System.Void task1::notshowar()
extern void task1_notshowar_mF3D02CFDB8679942256A373796F833F63F30C749 (void);
// 0x0000001C System.Void task1::.ctor()
extern void task1__ctor_m6BB42C0D3B2B26637FD35AA11D1868EAB472A1A5 (void);
// 0x0000001D System.Void task1/<stopwatch>d__25::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__25__ctor_mF4FF86AF458829AF5663B0E71B4745CC95329BCD (void);
// 0x0000001E System.Void task1/<stopwatch>d__25::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m58A3826412DA336685BCA3CD51C27470C8FD760F (void);
// 0x0000001F System.Boolean task1/<stopwatch>d__25::MoveNext()
extern void U3CstopwatchU3Ed__25_MoveNext_mB49BCAFD4060480C1F2071848D3918B43DFF9D95 (void);
// 0x00000020 System.Object task1/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m958693B16603CEF13E2250BFE3595E3EA6A9884F (void);
// 0x00000021 System.Void task1/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_mBCC6462A07E23707896D3C34E923E1ED21704053 (void);
// 0x00000022 System.Object task1/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_mB2C5972D09EA4D0BD26BB7F009188AC2D95626E0 (void);
// 0x00000023 System.Void task4::Start()
extern void task4_Start_m0B1D772E15AB95915391247131C17D5067FED8D8 (void);
// 0x00000024 System.Collections.IEnumerator task4::stopwatch()
extern void task4_stopwatch_m95DDCF773F9417486F4A3B7DFD7F9304044095C9 (void);
// 0x00000025 System.Void task4::answer()
extern void task4_answer_m4DE52F25C330730E1191012EC4F210C871D0D333 (void);
// 0x00000026 System.Void task4::notshowver()
extern void task4_notshowver_m1413B9D28D4E264B82631BFD301BF6D23E0BB28D (void);
// 0x00000027 System.Void task4::showstatistics()
extern void task4_showstatistics_m1EE69CE4A686DEB76C973DD61926BCBAF48188BB (void);
// 0x00000028 System.Void task4::notshowstatistics()
extern void task4_notshowstatistics_mB117841FA16A32F8CE605E9BC01FA86B31D364AD (void);
// 0x00000029 System.Void task4::showhint1()
extern void task4_showhint1_m41D33F7070217066E07F6B43AFF81A2B5A761EE1 (void);
// 0x0000002A System.Void task4::notshowhint1()
extern void task4_notshowhint1_m5032296A1C5864BF54B9C840D5D2D3842E4697CD (void);
// 0x0000002B System.Void task4::showar()
extern void task4_showar_m37988850128214C0E8685CB89044779FD5FC01CC (void);
// 0x0000002C System.Void task4::notshowar()
extern void task4_notshowar_mB09DBBFE50531517979CAE5A298A275120CD462B (void);
// 0x0000002D System.Void task4::nextuslzad()
extern void task4_nextuslzad_mD8368986A60FA50E94965FD5B3D67587A43928EC (void);
// 0x0000002E System.Void task4::backuslzad()
extern void task4_backuslzad_m5DB41551643432B8608F84D9A0D0B9F8B7C0FAF7 (void);
// 0x0000002F System.Void task4::.ctor()
extern void task4__ctor_mA47FE455EA13015141BBE3690DB9CC612842C58B (void);
// 0x00000030 System.Void task4/<stopwatch>d__25::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__25__ctor_m409BDAEB39E1686BFF71A2B46CFF8E553453EF8C (void);
// 0x00000031 System.Void task4/<stopwatch>d__25::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_mC9B982DB50D40262AEFB26036F95AD5864144C49 (void);
// 0x00000032 System.Boolean task4/<stopwatch>d__25::MoveNext()
extern void U3CstopwatchU3Ed__25_MoveNext_m85115A36EDAA0AC8AC668128539790BE3606EAE4 (void);
// 0x00000033 System.Object task4/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE196CC004A5D105E636406B83FC98CF574D25241 (void);
// 0x00000034 System.Void task4/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m2E2D4AA52F089E0BD63A0C14067D282CAB53530B (void);
// 0x00000035 System.Object task4/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m2B920A2072A94FABED0614800B71C1232E07D828 (void);
// 0x00000036 System.Void task16::Start()
extern void task16_Start_m317742F3C73D2B7672E7812FFCFCE805D1A2CC6C (void);
// 0x00000037 System.Collections.IEnumerator task16::stopwatch()
extern void task16_stopwatch_m2A2DB95C40486C258C29A239F25E038055F058F7 (void);
// 0x00000038 System.Void task16::answer()
extern void task16_answer_m79F86560D6BDC6059C14154FE39871EDAA94560F (void);
// 0x00000039 System.Void task16::showhint1()
extern void task16_showhint1_mC4858AB59D368A069B52F03A0456BD97F0A4A301 (void);
// 0x0000003A System.Void task16::notshowhint1()
extern void task16_notshowhint1_m955FDC707BCD8878FA96F119FD8CD63F13A5F3B4 (void);
// 0x0000003B System.Void task16::showhint2()
extern void task16_showhint2_m802C46B34FA7C32AFADF71D073CEAC045EE2D837 (void);
// 0x0000003C System.Void task16::notshowhint2()
extern void task16_notshowhint2_m707CA7778ADBBC07638B93B6272732970F4A5E7A (void);
// 0x0000003D System.Void task16::showhint3()
extern void task16_showhint3_m7CC7237E99791ADD407B8349E3316F9807BCBE3C (void);
// 0x0000003E System.Void task16::notshowhint3()
extern void task16_notshowhint3_m734B422B1420EE174E2DA333D103F22C043896EF (void);
// 0x0000003F System.Void task16::notshowver()
extern void task16_notshowver_m8B274DFEC93ACA4E896B4A7D34E2886E3C059849 (void);
// 0x00000040 System.Void task16::showstatistics()
extern void task16_showstatistics_m8631ECBD50838FBDB934B13E98C78CAC08551DA5 (void);
// 0x00000041 System.Void task16::notshowstatistics()
extern void task16_notshowstatistics_m856B11F8CE23FD0E58CA027494D3C4653C006677 (void);
// 0x00000042 System.Void task16::nextuslzad()
extern void task16_nextuslzad_mA238B4AAA6E54F1CAF82372B790ECBD667579F3D (void);
// 0x00000043 System.Void task16::backuslzad()
extern void task16_backuslzad_mCBD3E2E86CD703E6018732874431C4FB29AFCE6B (void);
// 0x00000044 System.Void task16::ForDropdown()
extern void task16_ForDropdown_m32DAC402A1398DA24AF07E31CCE3AA8DDDCF9236 (void);
// 0x00000045 System.Void task16::.ctor()
extern void task16__ctor_mBABF3C4619737B84F809810132CD363326569D31 (void);
// 0x00000046 System.Void task16/<stopwatch>d__36::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__36__ctor_mC91A3522F3CCBEAA4251614DF2EC589EA21268DA (void);
// 0x00000047 System.Void task16/<stopwatch>d__36::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__36_System_IDisposable_Dispose_mC56EACFB8272315F17AEC51D52E44D929EBDBDB9 (void);
// 0x00000048 System.Boolean task16/<stopwatch>d__36::MoveNext()
extern void U3CstopwatchU3Ed__36_MoveNext_m8FA85B029768A5A1E1E96A348237F403EB9CCDEA (void);
// 0x00000049 System.Object task16/<stopwatch>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF8028E7E338FB86B13B67CFB381A07ACB18A1F9 (void);
// 0x0000004A System.Void task16/<stopwatch>d__36::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__36_System_Collections_IEnumerator_Reset_m7DD2F20B5E3227FC44D10C623A4A8AD250F13DD4 (void);
// 0x0000004B System.Object task16/<stopwatch>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__36_System_Collections_IEnumerator_get_Current_mD652CCD1701967AA887203CE9237F53EA0966A08 (void);
// 0x0000004C System.Void task9::Start()
extern void task9_Start_m1346924DC336947D32338D7C98AA89A90E3F34B8 (void);
// 0x0000004D System.Collections.IEnumerator task9::stopwatch()
extern void task9_stopwatch_mA46BEEFEE49A0E9B29FD7D2AB9077C88D398B66F (void);
// 0x0000004E System.Void task9::answer()
extern void task9_answer_mC8C3825F296DACFD1F437C2309A82B84D1911DF7 (void);
// 0x0000004F System.Void task9::showhint1()
extern void task9_showhint1_m01431923ED781182C2741837D584361D4E29A334 (void);
// 0x00000050 System.Void task9::notshowhint1()
extern void task9_notshowhint1_mFF25165362A6516466316A300C365C877344C096 (void);
// 0x00000051 System.Void task9::showhint2()
extern void task9_showhint2_m449E4E5B405A3E4FF96D815D751923D6E396725E (void);
// 0x00000052 System.Void task9::notshowhint2()
extern void task9_notshowhint2_m2000D9FE732292BD834BF8FFF15B49EB5B9633DF (void);
// 0x00000053 System.Void task9::showhint3()
extern void task9_showhint3_mA5B2DDBF1A42E6ACB46DA5ABD453FCB37282D4D1 (void);
// 0x00000054 System.Void task9::notshowhint3()
extern void task9_notshowhint3_m19EAC40B6306519BA8B5631DE19A622C89A077F2 (void);
// 0x00000055 System.Void task9::notshowver()
extern void task9_notshowver_m645F024609592C8484F4C96ED7DEE30B83B58F0F (void);
// 0x00000056 System.Void task9::showstatistics()
extern void task9_showstatistics_m0124FBE9B774C311F79DDEE10E77438D74D2B21F (void);
// 0x00000057 System.Void task9::notshowstatistics()
extern void task9_notshowstatistics_mBC94AB312464C30BC7CC9A23ED041EC8F84046A7 (void);
// 0x00000058 System.Void task9::.ctor()
extern void task9__ctor_mA1FBA6261F9771C8D715EAD93286601F4A15E812 (void);
// 0x00000059 System.Void task9/<stopwatch>d__28::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__28__ctor_mAF135CD78100517B8A047C1CC0CC7FBF7D721E99 (void);
// 0x0000005A System.Void task9/<stopwatch>d__28::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_m968F8B16096B885F620B0C59DD55845E2A2C4DC5 (void);
// 0x0000005B System.Boolean task9/<stopwatch>d__28::MoveNext()
extern void U3CstopwatchU3Ed__28_MoveNext_m100E4BFAD41A0C25C2FBB6B9B85E4F97FCD56F1A (void);
// 0x0000005C System.Object task9/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0FB3F188DE6B33A3AA00CEB99D557C86D745987 (void);
// 0x0000005D System.Void task9/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m0EABDE0C4FBC3676A84E8E420FF9CBE70274ED20 (void);
// 0x0000005E System.Object task9/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_m471CA932302E91AA3E2CA207064D99F4926A2BE3 (void);
// 0x0000005F System.Void task12::Start()
extern void task12_Start_m12F3B82FC31A306AA7F2AEFDB1A73DF38C645801 (void);
// 0x00000060 System.Collections.IEnumerator task12::stopwatch()
extern void task12_stopwatch_m97E6C7D2705FF64D3031D1398AC66FD15D2EBCBA (void);
// 0x00000061 System.Void task12::answer()
extern void task12_answer_m8F3E12E11B29812C5C337A5370A521A0EF646172 (void);
// 0x00000062 System.Void task12::showhint1()
extern void task12_showhint1_mFE34419C0088A5F8349A00AF44E44F197291A4A8 (void);
// 0x00000063 System.Void task12::notshowhint1()
extern void task12_notshowhint1_m91EC270DCE1562236BF9966E0A46CB7A560098ED (void);
// 0x00000064 System.Void task12::showhint2()
extern void task12_showhint2_m32F9D47FA88DB394A6C5D7BC0D2D2EE7F81C4FCC (void);
// 0x00000065 System.Void task12::notshowhint2()
extern void task12_notshowhint2_mECD99D666EE7C93E16F065B63A0C119804C51112 (void);
// 0x00000066 System.Void task12::notshowver()
extern void task12_notshowver_m744582D246AEFE64474182F8DCC05DF12002C5ED (void);
// 0x00000067 System.Void task12::showstatistics()
extern void task12_showstatistics_m70353CAC3CD7F6C59A2C61CAA2673C4438E47C8F (void);
// 0x00000068 System.Void task12::notshowstatistics()
extern void task12_notshowstatistics_m0D59F1CABFECCAD8913D4B5125C3FBCF85C1547D (void);
// 0x00000069 System.Void task12::showar()
extern void task12_showar_m48FF7FFAF93DDFA744ABAC9B1EDCC34A003E0FEF (void);
// 0x0000006A System.Void task12::notshowar()
extern void task12_notshowar_mA63527CEC7A87103E5554320AF1D6EAD1DEF4C6C (void);
// 0x0000006B System.Void task12::nextuslzad()
extern void task12_nextuslzad_mD83718B7F670D3225A5DDEDA82AB7348FC08AC51 (void);
// 0x0000006C System.Void task12::backuslzad()
extern void task12_backuslzad_mCF42045F3BE86A0B8244A5B2ABDD33F554D9EF65 (void);
// 0x0000006D System.Void task12::.ctor()
extern void task12__ctor_m4EF394A45514CA1BEF91BA0D200F1685DBF4C442 (void);
// 0x0000006E System.Void task12/<stopwatch>d__29::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__29__ctor_m1B84C54EC9B518E9CECE83E837776F3276385838 (void);
// 0x0000006F System.Void task12/<stopwatch>d__29::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__29_System_IDisposable_Dispose_m9D3E8528D9D30B0C5D2E52E3C4DBE0D9E1CEFB71 (void);
// 0x00000070 System.Boolean task12/<stopwatch>d__29::MoveNext()
extern void U3CstopwatchU3Ed__29_MoveNext_m343E5682D5AB7AF38E83737EF657C6D6D5500F87 (void);
// 0x00000071 System.Object task12/<stopwatch>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m972587A5720ABA5F6F3CA4A8EC5CA5508561F00C (void);
// 0x00000072 System.Void task12/<stopwatch>d__29::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__29_System_Collections_IEnumerator_Reset_m1E65CC4AC36D6F6B3E4A565402C9059B28CFCC56 (void);
// 0x00000073 System.Object task12/<stopwatch>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__29_System_Collections_IEnumerator_get_Current_m1E936383D0787D7C393CC2B8F592717EB3257F92 (void);
// 0x00000074 System.Void perehodmodul2::Show_Z1()
extern void perehodmodul2_Show_Z1_m48D40224BA4C5FFBEB8BCC6062679C9E22DC2C96 (void);
// 0x00000075 System.Void perehodmodul2::Show_Z2()
extern void perehodmodul2_Show_Z2_mA53C0CDBCCA4A674440C56A563C6D35392322AF9 (void);
// 0x00000076 System.Void perehodmodul2::Show_Z3()
extern void perehodmodul2_Show_Z3_m585561BFF2A2B80C5360620B91BDD8EC373DC644 (void);
// 0x00000077 System.Void perehodmodul2::Show_Z4()
extern void perehodmodul2_Show_Z4_mE475D1889E226DFE1FDEDFCA53CCFE17E991EA65 (void);
// 0x00000078 System.Void perehodmodul2::Show_Z5()
extern void perehodmodul2_Show_Z5_m09074BC5D8C36079E0D17A6A37FAAAA682816779 (void);
// 0x00000079 System.Void perehodmodul2::Show_Z6()
extern void perehodmodul2_Show_Z6_mBB8C9A35428D6A927664AE0F3E7E1305DCCFCB87 (void);
// 0x0000007A System.Void perehodmodul2::Show_Module()
extern void perehodmodul2_Show_Module_m77AACA54583ED62737DD681DEDCC5C16824B2355 (void);
// 0x0000007B System.Void perehodmodul2::Show_MainMenu()
extern void perehodmodul2_Show_MainMenu_mFEDE69C68D3917E0F1FD7BD0D961D43008C62E44 (void);
// 0x0000007C System.Void perehodmodul2::Show_zadmodule()
extern void perehodmodul2_Show_zadmodule_m21211DBCD5BBB387F31F7822321244F070574968 (void);
// 0x0000007D System.Void perehodmodul2::.ctor()
extern void perehodmodul2__ctor_mDAA3949A688F12FF38836395463D1D86410FD643 (void);
// 0x0000007E System.Void perehodmodul2::.cctor()
extern void perehodmodul2__cctor_m9D0CBAEEA949EF4F49098C09B97F693E6337D8BC (void);
// 0x0000007F System.Void moduli::Start()
extern void moduli_Start_mFE89B051CA2549C6BE4EDCDB82D950A04D3D1231 (void);
// 0x00000080 System.Void moduli::Show_Module()
extern void moduli_Show_Module_m2559DDF768ECA4FF64E654545F2970D4E5AE53DF (void);
// 0x00000081 System.Void moduli::Show_Module1()
extern void moduli_Show_Module1_m33831E350B0B6FF193AEA728A1E6E373B9FE3533 (void);
// 0x00000082 System.Void moduli::Show_Module2()
extern void moduli_Show_Module2_m0476848E7788E818995ADCFCCE53652508D3E04E (void);
// 0x00000083 System.Void moduli::Show_Module3()
extern void moduli_Show_Module3_m93A2948617CBADDF29C79648DD0F47AA6D86989C (void);
// 0x00000084 System.Void moduli::Show_Module4()
extern void moduli_Show_Module4_mB7E3F120B9710790D05C76821ACDB919EF04E9B8 (void);
// 0x00000085 System.Void moduli::showhinfo()
extern void moduli_showhinfo_mEB85AB45CF79708B67A5403FEEA502CA2B2577E6 (void);
// 0x00000086 System.Void moduli::notshowhinfo()
extern void moduli_notshowhinfo_mA83845B83F2C39837D655F83D51F5183AC4EE01D (void);
// 0x00000087 System.Void moduli::.ctor()
extern void moduli__ctor_m3308BDC509003DF6019B20F9C617D84439CC2F2C (void);
// 0x00000088 System.Void perehodMain::Show_begin()
extern void perehodMain_Show_begin_m28585DB2D7EDFFC5976D3062CB260D868CFC515F (void);
// 0x00000089 System.Void perehodMain::Show_avtor()
extern void perehodMain_Show_avtor_mF2DF10E84825192A855847EF76370B3E504F7B99 (void);
// 0x0000008A System.Void perehodMain::Vs()
extern void perehodMain_Vs_m51038803483A3CA815C57310A8F450888AE632D2 (void);
// 0x0000008B System.Void perehodMain::Show_MainMenu()
extern void perehodMain_Show_MainMenu_mA1217E463C0A60282CD5F8C7103311A27D6708D1 (void);
// 0x0000008C System.Void perehodMain::.ctor()
extern void perehodMain__ctor_m78B2C53711CF4CDE5E6F9607D93FC6C3E2E54B4C (void);
// 0x0000008D System.Void task21::Start()
extern void task21_Start_mC02A24F02E0C44F76E1C6088425529C956755DD2 (void);
// 0x0000008E System.Collections.IEnumerator task21::stopwatch()
extern void task21_stopwatch_mD216EDD0C1860FF19A24B72F9B5865BE6722F35D (void);
// 0x0000008F System.Void task21::answer()
extern void task21_answer_m771D30DFF6B2BC45FBC0F08105BF1D2208EC0F17 (void);
// 0x00000090 System.Void task21::showhint1()
extern void task21_showhint1_m0F0E10E03EFB07A91E47C98529C41CC2950B3EC3 (void);
// 0x00000091 System.Void task21::notshowhint1()
extern void task21_notshowhint1_m3F620BE3D3479594B9700046048931D66529BC7B (void);
// 0x00000092 System.Void task21::notshowver()
extern void task21_notshowver_mF5208B348997E2A48802E7244FA4E4FF5421C807 (void);
// 0x00000093 System.Void task21::showstatistics()
extern void task21_showstatistics_m4FD072E97700A8CD5F4F94DCD4EFE75ACAC19C3D (void);
// 0x00000094 System.Void task21::notshowstatistics()
extern void task21_notshowstatistics_m208D024064A6A5A81E324CB076891F576069BD8D (void);
// 0x00000095 System.Void task21::.ctor()
extern void task21__ctor_m8D95BED48C1F0816EB383E854D075070F1EEF41F (void);
// 0x00000096 System.Void task21/<stopwatch>d__18::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__18__ctor_mC5DF3C8476F781C48C8C06C1F8FE1C134F0AD286 (void);
// 0x00000097 System.Void task21/<stopwatch>d__18::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__18_System_IDisposable_Dispose_m0267A6292474A75D1A4D0E2C18AA5AE28A9B4A73 (void);
// 0x00000098 System.Boolean task21/<stopwatch>d__18::MoveNext()
extern void U3CstopwatchU3Ed__18_MoveNext_mD4F5172969BAEA7BC62C0D21BC583263D6F81538 (void);
// 0x00000099 System.Object task21/<stopwatch>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC82D427C0767A5292B87960F2E0ADA4B0C5EFB3 (void);
// 0x0000009A System.Void task21/<stopwatch>d__18::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m0282CAB192F651508DDBBFFA11D8BD5B9B2FCBA7 (void);
// 0x0000009B System.Object task21/<stopwatch>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__18_System_Collections_IEnumerator_get_Current_mA337C9225455F73B492AC4001B77EA49CD7DE6AD (void);
// 0x0000009C System.Void task7::Start()
extern void task7_Start_m8ACF65834D00626B9F816B24C3D586DA40D5C925 (void);
// 0x0000009D System.Collections.IEnumerator task7::stopwatch()
extern void task7_stopwatch_m2F4090704D490696CD3566C8DEDBF59931378068 (void);
// 0x0000009E System.Void task7::answer()
extern void task7_answer_mA837318B090BD474226E8228CCD10BA282AE3855 (void);
// 0x0000009F System.Void task7::showhint1()
extern void task7_showhint1_m1C967BBF52730364ECFDC5324A10475894C2A8E4 (void);
// 0x000000A0 System.Void task7::notshowhint1()
extern void task7_notshowhint1_mCA2C39E5BEFDF259B03AB5D482331285E5CD7024 (void);
// 0x000000A1 System.Void task7::showhint2()
extern void task7_showhint2_mA5567332F08A29B24C5423DE128A05FACAEDD739 (void);
// 0x000000A2 System.Void task7::notshowhint2()
extern void task7_notshowhint2_mEB289CA39A752CF12D675D206795B8004EC53F65 (void);
// 0x000000A3 System.Void task7::notshowver()
extern void task7_notshowver_m36B5C6076250C06796C6119993FDAEBC2CF6B2D6 (void);
// 0x000000A4 System.Void task7::showstatistics()
extern void task7_showstatistics_m4881D345BF29881F9A071B58C008831F140E9C06 (void);
// 0x000000A5 System.Void task7::notshowstatistics()
extern void task7_notshowstatistics_mF9E2E334A2DECF943F03505CCB1C315B3C32BDCB (void);
// 0x000000A6 System.Void task7::.ctor()
extern void task7__ctor_mCD393C645BEC915987B1302E147A69FD0F186F68 (void);
// 0x000000A7 System.Void task7/<stopwatch>d__23::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__23__ctor_m1665459298850446F9749CA3A9D77E111D5C1874 (void);
// 0x000000A8 System.Void task7/<stopwatch>d__23::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_mA4BFB0E57050964E1A5B3804022C6012B1279CED (void);
// 0x000000A9 System.Boolean task7/<stopwatch>d__23::MoveNext()
extern void U3CstopwatchU3Ed__23_MoveNext_m594BDE2FC7552239BD3C27C7D8AAB496FCEFE196 (void);
// 0x000000AA System.Object task7/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7416789C781BCA7777273FBAB6AB3C544737DD2A (void);
// 0x000000AB System.Void task7/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_mBC582C5957C3035D3103ECE4C8A6EA16D5FDBB30 (void);
// 0x000000AC System.Object task7/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_m521B0BF4CF9B3D14C05C896005F3AFC5A6DEB90E (void);
// 0x000000AD System.Void task24::Start()
extern void task24_Start_m3A79A1745E191FF4A5BD6BEE61F643A73EBCE23E (void);
// 0x000000AE System.Collections.IEnumerator task24::stopwatch()
extern void task24_stopwatch_m8AB0569BCD18CFB04A79F428EC61DEDDDC71EEB5 (void);
// 0x000000AF System.Void task24::answer()
extern void task24_answer_mC791A3F462466B4A712A2C0A706FABBF3674BF3F (void);
// 0x000000B0 System.Void task24::showhint1()
extern void task24_showhint1_m2F430E83330DFDF98A174B574C559036DF9B22D8 (void);
// 0x000000B1 System.Void task24::notshowhint1()
extern void task24_notshowhint1_m6CC526316584A94F1E3CF01FDC3AD0019235E116 (void);
// 0x000000B2 System.Void task24::showhint2()
extern void task24_showhint2_mFC66D2C7E43B55789664CC9632FE0AA516A9F3EB (void);
// 0x000000B3 System.Void task24::notshowhint2()
extern void task24_notshowhint2_mEEF8EC41480EA2E5A9D32388686C45BD0970EBA1 (void);
// 0x000000B4 System.Void task24::notshowver()
extern void task24_notshowver_mA7CE98DA19B53B118C77107435EF049211ECB722 (void);
// 0x000000B5 System.Void task24::showstatistics()
extern void task24_showstatistics_m0AC4C2A771E18B45491EFFF12B86B198E839DF25 (void);
// 0x000000B6 System.Void task24::notshowstatistics()
extern void task24_notshowstatistics_m40B2C82F3BE67D116AA4AC90BAC0114234D05287 (void);
// 0x000000B7 System.Void task24::.ctor()
extern void task24__ctor_m71840F19BCED7C1106B466FBC7E3104C72155C3F (void);
// 0x000000B8 System.Void task24/<stopwatch>d__23::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__23__ctor_m4967F2F96916BB941EABA3D4B61599D03E007EC6 (void);
// 0x000000B9 System.Void task24/<stopwatch>d__23::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_m75ED6D4AD626186ADEE51E5878C872341053ED83 (void);
// 0x000000BA System.Boolean task24/<stopwatch>d__23::MoveNext()
extern void U3CstopwatchU3Ed__23_MoveNext_mC9A000339EC7785AE48935C671A1524C59E071D9 (void);
// 0x000000BB System.Object task24/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE4984AA6593344450A997E75276209C91B110D (void);
// 0x000000BC System.Void task24/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m87A3860173B5A6BF3C96B25BF9FD502FBC02953F (void);
// 0x000000BD System.Object task24/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_m3FF3FFB2AD9B752EF58B3E26095694374026412A (void);
// 0x000000BE System.Void task11::Start()
extern void task11_Start_m1DA2920E46B912D2F00ABFD951E55C27BF151997 (void);
// 0x000000BF System.Collections.IEnumerator task11::stopwatch()
extern void task11_stopwatch_mD52F69B056E4BF9C01104BB9249D0F65ED326313 (void);
// 0x000000C0 System.Void task11::answer()
extern void task11_answer_mEF69DA0579065EE2C880FD31FAFDA5CE98CD9F5A (void);
// 0x000000C1 System.Void task11::showhint1()
extern void task11_showhint1_m06D8BA120D6B837BBA7D438EAD8ADB8635114660 (void);
// 0x000000C2 System.Void task11::notshowhint1()
extern void task11_notshowhint1_mADCC045880BBBA3F4E47DFD3396AF4E59A7BF2C2 (void);
// 0x000000C3 System.Void task11::showhint2()
extern void task11_showhint2_mB91B9420F48ABABE155753BECE6B7AE76DE18F47 (void);
// 0x000000C4 System.Void task11::notshowhint2()
extern void task11_notshowhint2_m94C3C451FA6BA877AACBEEBCF7166F686B00280F (void);
// 0x000000C5 System.Void task11::showhint3()
extern void task11_showhint3_m52D5C3B1B30FE5EAAB050A356977E03431A87930 (void);
// 0x000000C6 System.Void task11::notshowhint3()
extern void task11_notshowhint3_m7B23D99A26F698E790F35C6825AAA032914DE195 (void);
// 0x000000C7 System.Void task11::notshowver()
extern void task11_notshowver_m83CFDECF9E1ECB2C1C8924FC52B7211FA9672A75 (void);
// 0x000000C8 System.Void task11::showstatistics()
extern void task11_showstatistics_m30BFA103663833A6541D89EE8E9D6B3EB2144594 (void);
// 0x000000C9 System.Void task11::notshowstatistics()
extern void task11_notshowstatistics_m70A822C5FDC7B75E13C5AD0FE964ABF3020CAD07 (void);
// 0x000000CA System.Void task11::.ctor()
extern void task11__ctor_m1A154E4B49711116A93674070D463B30A6A5E3B6 (void);
// 0x000000CB System.Void task11/<stopwatch>d__28::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__28__ctor_mAD20EA5F58F53CE11B28B998BAF88A97D3995725 (void);
// 0x000000CC System.Void task11/<stopwatch>d__28::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mDFC70DC1B153CC1D99BAC77E5A8BFF8C41102385 (void);
// 0x000000CD System.Boolean task11/<stopwatch>d__28::MoveNext()
extern void U3CstopwatchU3Ed__28_MoveNext_m9C0CE3864B86C47D4209A7F6E4BB3E2DFCACA074 (void);
// 0x000000CE System.Object task11/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE24B00F4E674A2007FD5822A53FF45A4D1B8CB26 (void);
// 0x000000CF System.Void task11/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mF94B5912F0DE9CE7169D2D881F14DE8DAB775B8D (void);
// 0x000000D0 System.Object task11/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mE753FB07CB7582FE9A0DB6C25BCD75CB0FF0B069 (void);
// 0x000000D1 System.Void perehodmodul4::Show_Z1()
extern void perehodmodul4_Show_Z1_m1108703CB2B696AD0B8F5EAAF51CCC7565E63031 (void);
// 0x000000D2 System.Void perehodmodul4::Show_Z2()
extern void perehodmodul4_Show_Z2_m625B2F863DFB930EED5D6F73E4FF4A663D936C1D (void);
// 0x000000D3 System.Void perehodmodul4::Show_Z3()
extern void perehodmodul4_Show_Z3_mAA58DE4A85D889C0E2A9C4A8FB94C4932A47239A (void);
// 0x000000D4 System.Void perehodmodul4::Show_Z4()
extern void perehodmodul4_Show_Z4_m1E4B23FFBF319A6607ECD09994B502F35B223EEE (void);
// 0x000000D5 System.Void perehodmodul4::Show_Z5()
extern void perehodmodul4_Show_Z5_m06B299B930C53B3FA03E50B74BCC3CAFD049EF3F (void);
// 0x000000D6 System.Void perehodmodul4::Show_Z6()
extern void perehodmodul4_Show_Z6_m9B7DD3E4612274933461889E12EC2A2BDBB97C4E (void);
// 0x000000D7 System.Void perehodmodul4::Show_Module()
extern void perehodmodul4_Show_Module_m64FD8ED53471864AEAF5D5D531D18C8478332256 (void);
// 0x000000D8 System.Void perehodmodul4::Show_MainMenu()
extern void perehodmodul4_Show_MainMenu_mC4875713425DAE3AC1D5E93C5994C2BC2EF25C64 (void);
// 0x000000D9 System.Void perehodmodul4::Show_zadmodule()
extern void perehodmodul4_Show_zadmodule_m2B7D3D28C0CB69338EC915C3D84AEA7D8AB39F7A (void);
// 0x000000DA System.Void perehodmodul4::.ctor()
extern void perehodmodul4__ctor_m1937A637E8EF20C5D111A1BC82CDD8C5F66CDC81 (void);
// 0x000000DB System.Void perehodmodul4::.cctor()
extern void perehodmodul4__cctor_m31086EB0D7B9B181FBDD1A450FCF9F290B013E32 (void);
// 0x000000DC System.Void task23::Start()
extern void task23_Start_m3DF5B0089FA92D710D4859DD099911F9FE8DE92B (void);
// 0x000000DD System.Collections.IEnumerator task23::stopwatch()
extern void task23_stopwatch_mE975F1CE89B92638B3883B1106EA88175E41D1FB (void);
// 0x000000DE System.Collections.IEnumerator task23::VisibleRotate()
extern void task23_VisibleRotate_m907DCE84B6FFD087E73261817722805381B71114 (void);
// 0x000000DF System.Void task23::answer()
extern void task23_answer_m5FB41B7B4530BFC677F5DD6D03DA7DD661754CD1 (void);
// 0x000000E0 System.Void task23::notshowver()
extern void task23_notshowver_m25BDB45D4AE3239F8D755DE971AC2CD1AF2C9BA5 (void);
// 0x000000E1 System.Void task23::showstatistics()
extern void task23_showstatistics_m62AD2C5237C18A30607E97E3C6124A4764D90A90 (void);
// 0x000000E2 System.Void task23::showhint()
extern void task23_showhint_mF6F9DE22D73ADDCC40A91952DB30759B14D13E5E (void);
// 0x000000E3 System.Void task23::notshowstatistics()
extern void task23_notshowstatistics_m4F8098A3E10F4CA1AFDE7CF9095F8F531D6FBF35 (void);
// 0x000000E4 System.Void task23::showar()
extern void task23_showar_m9A2C0877643E12C6F011B32CFDFE9C69905638AF (void);
// 0x000000E5 System.Void task23::notshowar()
extern void task23_notshowar_mAF38B64C8AABFCF744047C33D0C02D1B759045A5 (void);
// 0x000000E6 System.Void task23::tog_activePeople(System.Boolean)
extern void task23_tog_activePeople_m2906252B9A3AA6BD326FAC30CA8180DBAE28B955 (void);
// 0x000000E7 System.Collections.IEnumerator task23::VisiblePeople()
extern void task23_VisiblePeople_m8AD7861251C1838E8C6B9B263FB45DBC2BB88E2F (void);
// 0x000000E8 System.Void task23::tog_activeRotate(System.Boolean)
extern void task23_tog_activeRotate_m8C37A2D5818BF1517C760C3D716E83A42FB25DE7 (void);
// 0x000000E9 System.Void task23::notshowhint()
extern void task23_notshowhint_mBB05ACEF9AAAD8B6D2B3A48B648319BD2640002E (void);
// 0x000000EA System.Void task23::.ctor()
extern void task23__ctor_m1624DB91DBA641E4BDC0A2B73C6812A169588E8F (void);
// 0x000000EB System.Void task23/<stopwatch>d__28::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__28__ctor_m20ACD953B1BBACC38DDA747FCE0ED1E7EF173B7A (void);
// 0x000000EC System.Void task23/<stopwatch>d__28::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_m7E24741B924CF192799351F26AEF913EBC0D8007 (void);
// 0x000000ED System.Boolean task23/<stopwatch>d__28::MoveNext()
extern void U3CstopwatchU3Ed__28_MoveNext_mDBBBC8C2A2240F7B135217ABB7C6920D4ED2A292 (void);
// 0x000000EE System.Object task23/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DE1E234065C895734C58CEE233C3B9DF8A732B4 (void);
// 0x000000EF System.Void task23/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m96D72C34051F787294468EB7E08BA0C90BE22675 (void);
// 0x000000F0 System.Object task23/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mEE7F5DD2B78F2C4F3C7EDAEA620DA686B246ED76 (void);
// 0x000000F1 System.Void task23/<VisibleRotate>d__29::.ctor(System.Int32)
extern void U3CVisibleRotateU3Ed__29__ctor_m32FBBD56576C854E4E4A54DF1CB66A92DC28C367 (void);
// 0x000000F2 System.Void task23/<VisibleRotate>d__29::System.IDisposable.Dispose()
extern void U3CVisibleRotateU3Ed__29_System_IDisposable_Dispose_mBA331491978040189D256BEF30F5890E7FF8CF1A (void);
// 0x000000F3 System.Boolean task23/<VisibleRotate>d__29::MoveNext()
extern void U3CVisibleRotateU3Ed__29_MoveNext_mE9611179BF7AEBE7E18A8CCF892CA69FCC5E1601 (void);
// 0x000000F4 System.Object task23/<VisibleRotate>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVisibleRotateU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF8E4E209E96AC964B05634BA177DD9E1DA4E81E (void);
// 0x000000F5 System.Void task23/<VisibleRotate>d__29::System.Collections.IEnumerator.Reset()
extern void U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_Reset_m7CBDF83FC94C799459737385FF0C2E1CFFC5876D (void);
// 0x000000F6 System.Object task23/<VisibleRotate>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_get_Current_m8E76273C216A8F4E4694A4F34E0D6B9270319218 (void);
// 0x000000F7 System.Void task23/<VisiblePeople>d__38::.ctor(System.Int32)
extern void U3CVisiblePeopleU3Ed__38__ctor_m561B71216B1355DF5FAD403B70C373D043A0E5F1 (void);
// 0x000000F8 System.Void task23/<VisiblePeople>d__38::System.IDisposable.Dispose()
extern void U3CVisiblePeopleU3Ed__38_System_IDisposable_Dispose_mC336EE931B19EB00A5636EABF1920B5395C98EF7 (void);
// 0x000000F9 System.Boolean task23/<VisiblePeople>d__38::MoveNext()
extern void U3CVisiblePeopleU3Ed__38_MoveNext_mF8AD53C09E7A8DBDCB840D2B91980DBE665777D4 (void);
// 0x000000FA System.Object task23/<VisiblePeople>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVisiblePeopleU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9127517308D43B42DC64A6E6A94348B816BD5A2 (void);
// 0x000000FB System.Void task23/<VisiblePeople>d__38::System.Collections.IEnumerator.Reset()
extern void U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_Reset_m4246B29506244980BE7A9236CCDF52291E841A50 (void);
// 0x000000FC System.Object task23/<VisiblePeople>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_get_Current_mBE1B58715B37AE1491A81FC814DBA37FBB200601 (void);
// 0x000000FD System.Void Task_Door_manager::Start()
extern void Task_Door_manager_Start_m61E86AC6B10DE353D02989A16FE369ABEC17A380 (void);
// 0x000000FE System.Void Task_Door_manager::btn_Hints()
extern void Task_Door_manager_btn_Hints_mAEBAD32B6C3F4F5E9549A3855D00A58DE05F1CBC (void);
// 0x000000FF System.Void Task_Door_manager::tog_activePeople(System.Boolean)
extern void Task_Door_manager_tog_activePeople_mE862D6F9E2A5B84C3E127C3ABB3767BC7FF21587 (void);
// 0x00000100 System.Collections.IEnumerator Task_Door_manager::VisiblePeople()
extern void Task_Door_manager_VisiblePeople_m56759FF90D89ABB1C1CCCFB9D8C1EFE99191541B (void);
// 0x00000101 System.Void Task_Door_manager::tog_activeRotate(System.Boolean)
extern void Task_Door_manager_tog_activeRotate_m1C759AF11EF24F052DBB46DAD0021883BB968BD6 (void);
// 0x00000102 System.Collections.IEnumerator Task_Door_manager::VisibleRotate()
extern void Task_Door_manager_VisibleRotate_m9D42F74CEC4772F7B045976DE6F62991466B5583 (void);
// 0x00000103 System.Void Task_Door_manager::btn_Back()
extern void Task_Door_manager_btn_Back_mE9C2821DAF9BDE683134BF8C8C28CA6E95EB3A99 (void);
// 0x00000104 System.Void Task_Door_manager::btn_Verify()
extern void Task_Door_manager_btn_Verify_m8B6E07029E2376B971E38049D5337731618E3447 (void);
// 0x00000105 System.Void Task_Door_manager::btn_Statistics()
extern void Task_Door_manager_btn_Statistics_m38A49BAF95DCF97E9FB002DD404348FA244280DE (void);
// 0x00000106 System.Void Task_Door_manager::notshowver()
extern void Task_Door_manager_notshowver_m556B0BDABE82C3B630DB06A59D1437674A7CDFE3 (void);
// 0x00000107 System.Void Task_Door_manager::showhint()
extern void Task_Door_manager_showhint_m9BCA6A61BC831F7E5E292A2937891EBCCC57290A (void);
// 0x00000108 System.Void Task_Door_manager::notshowstatistics()
extern void Task_Door_manager_notshowstatistics_m4C1680309C60FBA1DE36DCB55670BE03D5F58DDA (void);
// 0x00000109 System.Void Task_Door_manager::showar()
extern void Task_Door_manager_showar_m44CBA529E9459221D59B166B99E61D1D1FB22F69 (void);
// 0x0000010A System.Void Task_Door_manager::notshowar()
extern void Task_Door_manager_notshowar_m056D22FDCEDBDED9FF8C26CBC250D22E090876F0 (void);
// 0x0000010B System.Void Task_Door_manager::notinfo()
extern void Task_Door_manager_notinfo_m4F9A478F9DB50ED6B4431289CB27B98F66DD7273 (void);
// 0x0000010C System.Void Task_Door_manager::.ctor()
extern void Task_Door_manager__ctor_m2592C61D9BEB9179787DCBDC9DFEF63329325E60 (void);
// 0x0000010D System.Void Task_Door_manager/<VisiblePeople>d__29::.ctor(System.Int32)
extern void U3CVisiblePeopleU3Ed__29__ctor_m4C06D63C1CC803EC17AC6A2252086760283F82AD (void);
// 0x0000010E System.Void Task_Door_manager/<VisiblePeople>d__29::System.IDisposable.Dispose()
extern void U3CVisiblePeopleU3Ed__29_System_IDisposable_Dispose_m9E8A5E3ED2027C01EABD58F8715DC6D64E9A2191 (void);
// 0x0000010F System.Boolean Task_Door_manager/<VisiblePeople>d__29::MoveNext()
extern void U3CVisiblePeopleU3Ed__29_MoveNext_mC1F19694A09408545418DC076BC23202FE7069E3 (void);
// 0x00000110 System.Object Task_Door_manager/<VisiblePeople>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVisiblePeopleU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02EB33F95B66F03FDCB145E76E542F5BBC14CCA0 (void);
// 0x00000111 System.Void Task_Door_manager/<VisiblePeople>d__29::System.Collections.IEnumerator.Reset()
extern void U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_Reset_mF3D061165A076653B3044E1EEC3FE34959CDBBF8 (void);
// 0x00000112 System.Object Task_Door_manager/<VisiblePeople>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_get_Current_mA45ACC0C1443F361432F420CF4A5E040594DE296 (void);
// 0x00000113 System.Void Task_Door_manager/<VisibleRotate>d__31::.ctor(System.Int32)
extern void U3CVisibleRotateU3Ed__31__ctor_m34B001F2552F17A0BD062867405FE3807320C9C7 (void);
// 0x00000114 System.Void Task_Door_manager/<VisibleRotate>d__31::System.IDisposable.Dispose()
extern void U3CVisibleRotateU3Ed__31_System_IDisposable_Dispose_m7571664E714301A3F86C18A1948807192D09683B (void);
// 0x00000115 System.Boolean Task_Door_manager/<VisibleRotate>d__31::MoveNext()
extern void U3CVisibleRotateU3Ed__31_MoveNext_m2A3118E4C49E683947C2FDD0B55A7D3620010364 (void);
// 0x00000116 System.Object Task_Door_manager/<VisibleRotate>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVisibleRotateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91DAAEAF951EB100F86F0C31704D80E325832865 (void);
// 0x00000117 System.Void Task_Door_manager/<VisibleRotate>d__31::System.Collections.IEnumerator.Reset()
extern void U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_Reset_m3DEA0136564A7FBE1C91EF8659AD71DEA872B57C (void);
// 0x00000118 System.Object Task_Door_manager/<VisibleRotate>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_get_Current_m0A32639DB93E11597F23E763E9DCED2A594BAF92 (void);
// 0x00000119 System.Void task20::Start()
extern void task20_Start_mF533685D9A82BAC636B28DF5312F3BF696B8AADB (void);
// 0x0000011A System.Collections.IEnumerator task20::stopwatch()
extern void task20_stopwatch_m93DC59FD5E751D67252AC0E3BE7247D598206434 (void);
// 0x0000011B System.Void task20::answer()
extern void task20_answer_m554812542D41A02AF2D4D19EF73F792867CCFC09 (void);
// 0x0000011C System.Void task20::showhint1()
extern void task20_showhint1_mDFA29F24DF2D4D294648F3C3CF09BFB2B949E263 (void);
// 0x0000011D System.Void task20::notshowhint1()
extern void task20_notshowhint1_mA6846CBF26528EDF711411AFEC41CED9D7110AF3 (void);
// 0x0000011E System.Void task20::notshowver()
extern void task20_notshowver_m5674F9846E50B98BFEF6FF6A2CD13C30FC70313B (void);
// 0x0000011F System.Void task20::showstatistics()
extern void task20_showstatistics_m1B00594C5438989690DD40E5D09C71D71630FCF2 (void);
// 0x00000120 System.Void task20::notshowstatistics()
extern void task20_notshowstatistics_m18F42B20B20B7D828C84050E53CD1A0CD552DC89 (void);
// 0x00000121 System.Void task20::showar()
extern void task20_showar_m5A5A1F7AB7661957EE90F0BCC6F677A5FA146F74 (void);
// 0x00000122 System.Void task20::notshowar()
extern void task20_notshowar_mD282D8948220BC9DC56437FCF17C6A53F21A9551 (void);
// 0x00000123 System.Void task20::.ctor()
extern void task20__ctor_m370415A6C3F20DD11C23C169FF3F567266379B09 (void);
// 0x00000124 System.Void task20/<stopwatch>d__22::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__22__ctor_m6FF962E670F0354B1BF097A4F6D365ABD8A8CEF5 (void);
// 0x00000125 System.Void task20/<stopwatch>d__22::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__22_System_IDisposable_Dispose_m10B0A8D9B1524330A1E820ED4E74F6B8ED334C43 (void);
// 0x00000126 System.Boolean task20/<stopwatch>d__22::MoveNext()
extern void U3CstopwatchU3Ed__22_MoveNext_m755FD9840826CBDD979FA6E3BAB0BA7181C0F05E (void);
// 0x00000127 System.Object task20/<stopwatch>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE8869F1C1CB3A564CFA22DD79C4BA855B8817C (void);
// 0x00000128 System.Void task20/<stopwatch>d__22::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__22_System_Collections_IEnumerator_Reset_m3ECCA74D5197F50D38F45FEB9DF44710AA48C65D (void);
// 0x00000129 System.Object task20/<stopwatch>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__22_System_Collections_IEnumerator_get_Current_m99C2E8CA3B5BD12A840488A4C2566B4AF44297EA (void);
// 0x0000012A System.Void task15::Start()
extern void task15_Start_m6EB0BC04176274907071B5F00EDC646CE459ED09 (void);
// 0x0000012B System.Collections.IEnumerator task15::stopwatch()
extern void task15_stopwatch_m857B335C8086453B6354F2D3F0BE344DCD93BD4B (void);
// 0x0000012C System.Void task15::answer()
extern void task15_answer_mAAD9D66C4D1BB53B343E77993156C68E626F767F (void);
// 0x0000012D System.Void task15::showhint1()
extern void task15_showhint1_m926551F5466C175BD8491C5CF6A5E7A0EAA76790 (void);
// 0x0000012E System.Void task15::notshowhint1()
extern void task15_notshowhint1_m92C2929AA61E1480014F708D7874E3501DB4BC93 (void);
// 0x0000012F System.Void task15::showhint2()
extern void task15_showhint2_mD12453AAE226EB208500330C872BC24B9507C4A5 (void);
// 0x00000130 System.Void task15::notshowhint2()
extern void task15_notshowhint2_mF38918D67803F3BE629461BCE3A83A6789971902 (void);
// 0x00000131 System.Void task15::notshowver()
extern void task15_notshowver_mAD2841186D6D793181CC894A00111EA040F63C7E (void);
// 0x00000132 System.Void task15::showstatistics()
extern void task15_showstatistics_mEFC8CEE07759B268371CC8C570F0AB43F493C001 (void);
// 0x00000133 System.Void task15::notshowstatistics()
extern void task15_notshowstatistics_m33DB1025BC0687FB342E35DEA2E67789ADF71225 (void);
// 0x00000134 System.Void task15::showar()
extern void task15_showar_m84D9FBA12BA90E39F1FCC4FB8634FE671A4D6FD1 (void);
// 0x00000135 System.Void task15::notshowar()
extern void task15_notshowar_m66FC2733997F7E8C9443376EA9080106028FB989 (void);
// 0x00000136 System.Void task15::nextuslzad()
extern void task15_nextuslzad_m93F7F754A2A90EF8BEBBFA46F4A0593F8F435BF7 (void);
// 0x00000137 System.Void task15::backuslzad()
extern void task15_backuslzad_mCD089CDF8822FD7E70BD984183FD591140E4B1AB (void);
// 0x00000138 System.Void task15::.ctor()
extern void task15__ctor_m15D7CD3FDA4992323F534948AE1F3348277ED97E (void);
// 0x00000139 System.Void task15/<stopwatch>d__27::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__27__ctor_m2F5D907BB12AB98E504E9D63C163FFDE0FFF6838 (void);
// 0x0000013A System.Void task15/<stopwatch>d__27::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__27_System_IDisposable_Dispose_m195C6BFB6CB1125CE2DBAFFC4D66754F377CED92 (void);
// 0x0000013B System.Boolean task15/<stopwatch>d__27::MoveNext()
extern void U3CstopwatchU3Ed__27_MoveNext_mF0249B2C4FB9D3C1D42A56B682ED56258B279ADD (void);
// 0x0000013C System.Object task15/<stopwatch>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19807947C7AFE56A572BD94EEDDD297A4E9C6186 (void);
// 0x0000013D System.Void task15/<stopwatch>d__27::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__27_System_Collections_IEnumerator_Reset_mBA30D813137118067E8CCF6E4B421B60764FAB4A (void);
// 0x0000013E System.Object task15/<stopwatch>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__27_System_Collections_IEnumerator_get_Current_m30B7BFAA601AAF6027028C4B2ECAC126BA82D405 (void);
// 0x0000013F System.Void CubeRotate::Update()
extern void CubeRotate_Update_m5329A8708151CE19E112FC96D731DADDE6E78748 (void);
// 0x00000140 System.Void CubeRotate::.ctor()
extern void CubeRotate__ctor_m8FABF7E0E452666796420B4BE9DCA4E64BD70767 (void);
// 0x00000141 System.Void task2::Start()
extern void task2_Start_mA76252C3325EBE77E36F02340536F5C1FCF3C0DD (void);
// 0x00000142 System.Collections.IEnumerator task2::stopwatch()
extern void task2_stopwatch_mE2A3C1694CCA003374884201048E08DF0EE56BA5 (void);
// 0x00000143 System.Void task2::answer()
extern void task2_answer_mFC72D92CE1238B594899EC1E09FC146B76B25F73 (void);
// 0x00000144 System.Void task2::notshowver()
extern void task2_notshowver_m14633FA86BD512BB228A0777BE07AFB1691CD9C6 (void);
// 0x00000145 System.Void task2::showstatistics()
extern void task2_showstatistics_m87E64E1632DE256166AE6AEABA238B2F78986291 (void);
// 0x00000146 System.Void task2::notshowstatistics()
extern void task2_notshowstatistics_m1B5C4DC751E25CE24C776B87A14A79A9593908F9 (void);
// 0x00000147 System.Void task2::showar()
extern void task2_showar_mCEF19E2CBBDD2EEB8E2E58D4B9F7295F3CA3178D (void);
// 0x00000148 System.Void task2::notshowar()
extern void task2_notshowar_m349E31545F60799DFFB362C8D6BBBAFD5286937B (void);
// 0x00000149 System.Void task2::showhint1()
extern void task2_showhint1_m7EA5948CCE3ABD87BB49F47CBF420382D676F5A9 (void);
// 0x0000014A System.Void task2::notshowhint1()
extern void task2_notshowhint1_m199BFBB9B5F4CB10986E084E8F4AA8724D8D922C (void);
// 0x0000014B System.Void task2::showhint2()
extern void task2_showhint2_mEF8DC2FED820C4E160939C37493C65C4F9CD515E (void);
// 0x0000014C System.Void task2::notshowhint2()
extern void task2_notshowhint2_m59FEEA9B1E29DD3E027FAF21890F4C198801DC8A (void);
// 0x0000014D System.Void task2::.ctor()
extern void task2__ctor_m509B319AF9AE752692253595723EA2CB736742FB (void);
// 0x0000014E System.Void task2/<stopwatch>d__23::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__23__ctor_m6A763E528C337619B93BA87550784E8CD7AEB70D (void);
// 0x0000014F System.Void task2/<stopwatch>d__23::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_m05F05ECBC6DDBB6F840407617AE050983215232A (void);
// 0x00000150 System.Boolean task2/<stopwatch>d__23::MoveNext()
extern void U3CstopwatchU3Ed__23_MoveNext_mB8EE0A685B880CD5249656BCD077B5B30A62CDBB (void);
// 0x00000151 System.Object task2/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD11EA88F3846CE46262CFB25D589596F212C0C (void);
// 0x00000152 System.Void task2/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m7687E13C63EB80EA50D717667B6A601259F2C817 (void);
// 0x00000153 System.Object task2/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_mC1C7451693935FBC3A16C1529A24063300610D88 (void);
// 0x00000154 System.Void task6::Start()
extern void task6_Start_m0153FB064AEAA594FEEDF93D7206E57E6F022A17 (void);
// 0x00000155 System.Collections.IEnumerator task6::stopwatch()
extern void task6_stopwatch_mE1D169201A449AE4106C2A25BAFD87D0E64FEB64 (void);
// 0x00000156 System.Void task6::answer()
extern void task6_answer_mBC1716119E4FAB35C676C654C523867B5B55DDEC (void);
// 0x00000157 System.Void task6::notshowver()
extern void task6_notshowver_m17AA4B30E03D8B9C0A440B859B35475AFEEEA476 (void);
// 0x00000158 System.Void task6::showstatistics()
extern void task6_showstatistics_m2A895869CDF30E62C90B700B37EE408E7E7C7E32 (void);
// 0x00000159 System.Void task6::notshowstatistics()
extern void task6_notshowstatistics_m204E489F3604C7454F61EB21CDAD8707FA134ED2 (void);
// 0x0000015A System.Void task6::showhint1()
extern void task6_showhint1_m10B5A77D70D20ACBD5E2A56163F9440639C2D2E8 (void);
// 0x0000015B System.Void task6::notshowhint1()
extern void task6_notshowhint1_m6BD59DCD61198571580BB6F36EBF557767C7E1BC (void);
// 0x0000015C System.Void task6::nextuslzad()
extern void task6_nextuslzad_m3D39D538A4A3BF92A1E04C3D79AF7465B7ECA010 (void);
// 0x0000015D System.Void task6::backuslzad()
extern void task6_backuslzad_mD72CFE1AEAD9A6C161693C2673F846BFEA662F2B (void);
// 0x0000015E System.Void task6::.ctor()
extern void task6__ctor_mDCB2060F7ED7DA50A69019BD6AC07D994307FDD8 (void);
// 0x0000015F System.Void task6/<stopwatch>d__25::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__25__ctor_mBE7488CED855751FE6A9873967B29D948A39278E (void);
// 0x00000160 System.Void task6/<stopwatch>d__25::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m53B282F84A7D3076991C9F51A5D8D05E510E1CC8 (void);
// 0x00000161 System.Boolean task6/<stopwatch>d__25::MoveNext()
extern void U3CstopwatchU3Ed__25_MoveNext_m1F504BD1F2F28B3C7A3AF309466FD7C96B5F1BB8 (void);
// 0x00000162 System.Object task6/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F3AEEC752291A571E12EB2CC7CE72B53A8958E9 (void);
// 0x00000163 System.Void task6/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m896AFD90834C83785294BC43E7A161D3966D3DC6 (void);
// 0x00000164 System.Object task6/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m429B23341905A2972FA9BCC40F1FD9F27285CBD3 (void);
// 0x00000165 System.Void task17::Start()
extern void task17_Start_m8D9C457CCE8CE04DDA700238DA4E4BE8B60DAB2F (void);
// 0x00000166 System.Collections.IEnumerator task17::stopwatch()
extern void task17_stopwatch_m250A4B51D7AC3DE3436B2380FC038A6577BBC735 (void);
// 0x00000167 System.Void task17::answer()
extern void task17_answer_mCD958FCA676848081930963384E4571EC89FD715 (void);
// 0x00000168 System.Void task17::showhint1()
extern void task17_showhint1_m2B3B0C12A9D2F6E7A2FC8FC8850428224749B5C7 (void);
// 0x00000169 System.Void task17::notshowhint1()
extern void task17_notshowhint1_mEFDD70AB171DE2AFA5D22E1B788B72C69525571C (void);
// 0x0000016A System.Void task17::showhint2()
extern void task17_showhint2_m154361BF007D60D0AD8868383F2A25A3C9495352 (void);
// 0x0000016B System.Void task17::notshowhint2()
extern void task17_notshowhint2_mF659745FE8D2F702CC458EAFF6D855AE24B94091 (void);
// 0x0000016C System.Void task17::notshowver()
extern void task17_notshowver_m0B2A39583BBAD2DFA42B1921C4430C5FA3BCF1FB (void);
// 0x0000016D System.Void task17::showstatistics()
extern void task17_showstatistics_mFBA0CAF6F359C78DBB076464B37AF371BBC01BA5 (void);
// 0x0000016E System.Void task17::notshowstatistics()
extern void task17_notshowstatistics_m5AE3EE321D44CAC645AEAD9A3858077F7F3CDBCE (void);
// 0x0000016F System.Void task17::showar()
extern void task17_showar_m8D87DBD8DA7AEEFD8998449ADF4AA0F81F809815 (void);
// 0x00000170 System.Void task17::notshowar()
extern void task17_notshowar_m55B28011E60D5617F632116007C6F4683AA15C44 (void);
// 0x00000171 System.Void task17::.ctor()
extern void task17__ctor_mE62EC7FC104AE9BEB8F26EFF80CFC2B7315CCE67 (void);
// 0x00000172 System.Void task17/<stopwatch>d__25::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__25__ctor_mF47A706BF25A5698C8E4C2445AE0B63B9F7E7ABB (void);
// 0x00000173 System.Void task17/<stopwatch>d__25::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_mABF71C27E50CE6F620863B20B0FBE34600A93176 (void);
// 0x00000174 System.Boolean task17/<stopwatch>d__25::MoveNext()
extern void U3CstopwatchU3Ed__25_MoveNext_m5C32301722B7469C5589ADC687B1A513BDCD8915 (void);
// 0x00000175 System.Object task17/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD660F032C4E2C84CABEB86CAF8D6E7F12295F5F (void);
// 0x00000176 System.Void task17/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m93082B657640111769C616B95E39BA4DEB00CA36 (void);
// 0x00000177 System.Object task17/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m76EF3B4BB6FFE23805A22FEB0DDE76A133536635 (void);
// 0x00000178 System.Void perehodmodul1::Show_Z1()
extern void perehodmodul1_Show_Z1_mE89DBDFC63B7DFC79F7757B801AC59C19B5827E3 (void);
// 0x00000179 System.Void perehodmodul1::Show_Z2()
extern void perehodmodul1_Show_Z2_m6F736CBF216742E3E8206BE84D1770F374B244A1 (void);
// 0x0000017A System.Void perehodmodul1::Show_Z3()
extern void perehodmodul1_Show_Z3_m6DC752D9C486992F2461125FAB37A05A77AFC063 (void);
// 0x0000017B System.Void perehodmodul1::Show_Z4()
extern void perehodmodul1_Show_Z4_m34B54AF42B189AB40555F42C25E7A7556E9AC0C0 (void);
// 0x0000017C System.Void perehodmodul1::Show_Z5()
extern void perehodmodul1_Show_Z5_mEEBDB8AF0F3849C2D586CD2352D8B7FC3B7EE64E (void);
// 0x0000017D System.Void perehodmodul1::Show_Z6()
extern void perehodmodul1_Show_Z6_mB2DF05C533B21235F041F0B04578AB50CC695486 (void);
// 0x0000017E System.Void perehodmodul1::Show_Z7()
extern void perehodmodul1_Show_Z7_mF5832523BC3C4637B12A71C88A2957303313FD7B (void);
// 0x0000017F System.Void perehodmodul1::Show_Module()
extern void perehodmodul1_Show_Module_mEB554CB9CB8BBB884C20065CFFE1A4EE26BB91B3 (void);
// 0x00000180 System.Void perehodmodul1::Show_MainMenu()
extern void perehodmodul1_Show_MainMenu_m614162B8305AFF8D9083839F99771620569FB6E3 (void);
// 0x00000181 System.Void perehodmodul1::Show_zadmodule()
extern void perehodmodul1_Show_zadmodule_m175CF2CEBF36AD62B491E05DFCB8C13E83E416C7 (void);
// 0x00000182 System.Void perehodmodul1::.ctor()
extern void perehodmodul1__ctor_mDE0461E54757B9DF125732C219BF22C48CBD0BFD (void);
// 0x00000183 System.Void perehodmodul1::.cctor()
extern void perehodmodul1__cctor_m81F7961CF0BA9A386B5D15F2E47D6F6676699EDE (void);
// 0x00000184 System.Void task19::Start()
extern void task19_Start_m578957EBD321FD3BB4A0F4D33866020F82CED659 (void);
// 0x00000185 System.Collections.IEnumerator task19::stopwatch()
extern void task19_stopwatch_m31B69CC2CBA23466BBFA045D897E57E049C20EA2 (void);
// 0x00000186 System.Void task19::answer()
extern void task19_answer_m51AB5D299521D313519AE438D9139F52FA59E02B (void);
// 0x00000187 System.Void task19::showhint1()
extern void task19_showhint1_m95DD7DD077C362A5A4C655ACCB5887CDE9B5A17B (void);
// 0x00000188 System.Void task19::notshowhint1()
extern void task19_notshowhint1_mAD4C6C8D04561767B5863F44E1343D26EE1635E3 (void);
// 0x00000189 System.Void task19::showhint2()
extern void task19_showhint2_m04A63664817B3B9523C2F3350067880CFEC2C892 (void);
// 0x0000018A System.Void task19::notshowhint2()
extern void task19_notshowhint2_m6F0E9BA917911CF4EF15C2D206269CDE6D9BAC8B (void);
// 0x0000018B System.Void task19::notshowver()
extern void task19_notshowver_m2C4058021D44679D93BB0F5C2E910B9E9697DD7E (void);
// 0x0000018C System.Void task19::showstatistics()
extern void task19_showstatistics_m726AEE438FBC63FEF71968EBF1D104290C6C2432 (void);
// 0x0000018D System.Void task19::notshowstatistics()
extern void task19_notshowstatistics_m27B568E87D6115A37BF995E53D3A42724F010F17 (void);
// 0x0000018E System.Void task19::nextuslzad()
extern void task19_nextuslzad_m149390164F4FEC7D402BC16D599C0A6525E2A9E7 (void);
// 0x0000018F System.Void task19::backuslzad()
extern void task19_backuslzad_mF7E1CEABD221792521E926961A94201B68BAADDB (void);
// 0x00000190 System.Void task19::.ctor()
extern void task19__ctor_m4914C174FB30C944160EDC9CAA4A07AB799833C3 (void);
// 0x00000191 System.Void task19/<stopwatch>d__25::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__25__ctor_mACB511D54A1CF1255DE5F80BD47A29E5DDA01A24 (void);
// 0x00000192 System.Void task19/<stopwatch>d__25::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m2DA8529AC2DCD42A557306FC5E0851194E493C31 (void);
// 0x00000193 System.Boolean task19/<stopwatch>d__25::MoveNext()
extern void U3CstopwatchU3Ed__25_MoveNext_m9330F7700F985C7D2A90AF73453E22C31320A1E5 (void);
// 0x00000194 System.Object task19/<stopwatch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4906CE9EDBDEB222D97A4DC80E1A19B0BB6E09A7 (void);
// 0x00000195 System.Void task19/<stopwatch>d__25::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m81FC0300E7EA90226FEF616DB832873969C16A32 (void);
// 0x00000196 System.Object task19/<stopwatch>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m6FEAAF4424650347E2B7B41D23D7579F986CEC3A (void);
// 0x00000197 System.Void Growth_Menu::Start()
extern void Growth_Menu_Start_mEADF62E543FF2798C0E8B9A25A027EB1D925A5E6 (void);
// 0x00000198 System.Collections.IEnumerator Growth_Menu::stopwatch()
extern void Growth_Menu_stopwatch_mCBEF9E4055584D62459871F29F8440B11E27DB38 (void);
// 0x00000199 System.Void Growth_Menu::showar()
extern void Growth_Menu_showar_m5CC26B8EBC609D737588DACC38C2AE1165DC58C6 (void);
// 0x0000019A System.Void Growth_Menu::ChangeColorForYesA()
extern void Growth_Menu_ChangeColorForYesA_mC9159BABB08FBC512E44271E982F6CC859EE6FAA (void);
// 0x0000019B System.Void Growth_Menu::ChangeColorForNoA()
extern void Growth_Menu_ChangeColorForNoA_m063A46B1352462F3348389A05EDC1825F44F51E5 (void);
// 0x0000019C System.Void Growth_Menu::ChangeColorForYesB()
extern void Growth_Menu_ChangeColorForYesB_m3A99D4C1B2570AE7DDECDB44E42C1EF174342963 (void);
// 0x0000019D System.Void Growth_Menu::ChangeColorForNoB()
extern void Growth_Menu_ChangeColorForNoB_m2457BCAC4BC5E78B0E1ABD8B7E9E78C21D84B0D2 (void);
// 0x0000019E System.Void Growth_Menu::ChangeColorForYesC()
extern void Growth_Menu_ChangeColorForYesC_m52434244BDFAC42A3A1CC74C7DFEA3F318CA1385 (void);
// 0x0000019F System.Void Growth_Menu::ChangeColorForNoC()
extern void Growth_Menu_ChangeColorForNoC_mAA0B48C321A261A82BAD671BD475F91E36CA34A3 (void);
// 0x000001A0 System.Void Growth_Menu::ChangeColorForYesD()
extern void Growth_Menu_ChangeColorForYesD_m09017F7CA9490B48E89B2B2CE4BAB4D352A54A34 (void);
// 0x000001A1 System.Void Growth_Menu::ChangeColorForNoD()
extern void Growth_Menu_ChangeColorForNoD_mF192296DF26FF74EC55658EEF7009325D44AC069 (void);
// 0x000001A2 System.Void Growth_Menu::answer()
extern void Growth_Menu_answer_m9248041B9D2C21692F3F39E491BE4949604B15DA (void);
// 0x000001A3 System.Void Growth_Menu::BtnHelpBig()
extern void Growth_Menu_BtnHelpBig_m8A2B0A77DE20966DE22342C198C067A8F7598A5D (void);
// 0x000001A4 System.Void Growth_Menu::BtnForHelpA()
extern void Growth_Menu_BtnForHelpA_mBFA289F7265C12241714C7BF7AA03B14E89BDFF3 (void);
// 0x000001A5 System.Void Growth_Menu::BtnForHelpA1()
extern void Growth_Menu_BtnForHelpA1_mA3EB874CA2332561963F3385455951D7B4C4A2E5 (void);
// 0x000001A6 System.Void Growth_Menu::BtnForHelpB()
extern void Growth_Menu_BtnForHelpB_mDD5454E7A8E966686ED687B784ADC9D9B59BC3FF (void);
// 0x000001A7 System.Void Growth_Menu::BtnForHelpB1()
extern void Growth_Menu_BtnForHelpB1_m1395C5771BE0BE27BAE8BEA57886EDBE02EB1421 (void);
// 0x000001A8 System.Void Growth_Menu::BtnForHelpC()
extern void Growth_Menu_BtnForHelpC_m3BC08A645A3A2F9DFC7307CDBF4C6CC9F22A3EFA (void);
// 0x000001A9 System.Void Growth_Menu::BtnForHelpC1()
extern void Growth_Menu_BtnForHelpC1_m0289494A20BBCE4AA1FEFB0717F33246A782CE40 (void);
// 0x000001AA System.Void Growth_Menu::BtnForHelpD()
extern void Growth_Menu_BtnForHelpD_m7BC333394DDE8D84B35840A2F5DFC281E2F21118 (void);
// 0x000001AB System.Void Growth_Menu::BtnForHelpD1()
extern void Growth_Menu_BtnForHelpD1_m6F13A6854F26F5B940A1EE48F0C6B1A1869C6197 (void);
// 0x000001AC System.Void Growth_Menu::ForBtnOKad()
extern void Growth_Menu_ForBtnOKad_m9DC2299B3309FC5ED9D731E05B14215D165F146E (void);
// 0x000001AD System.Void Growth_Menu::showstatistics()
extern void Growth_Menu_showstatistics_m9A2740F18A101613AFB48ED4C71EF3278696FC23 (void);
// 0x000001AE System.Void Growth_Menu::notshowar()
extern void Growth_Menu_notshowar_mA977602AF772B62B37D90897D6EC98A5E6E0BEBE (void);
// 0x000001AF System.Void Growth_Menu::notshowver()
extern void Growth_Menu_notshowver_m5917AC51417687872696FB2DEB60BC7A7603B475 (void);
// 0x000001B0 System.Void Growth_Menu::.ctor()
extern void Growth_Menu__ctor_m6DF50EED17A3ACA94D920E21E60DBA47F4A0EE31 (void);
// 0x000001B1 System.Void Growth_Menu/<stopwatch>d__89::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__89__ctor_m501F56E1C4172B9FDD76399C097A2D1C961CE401 (void);
// 0x000001B2 System.Void Growth_Menu/<stopwatch>d__89::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__89_System_IDisposable_Dispose_mCAA189524DF4478AE19409A5A44702805A223AFC (void);
// 0x000001B3 System.Boolean Growth_Menu/<stopwatch>d__89::MoveNext()
extern void U3CstopwatchU3Ed__89_MoveNext_mAA6962B9EE57E075023054D41FE31EE0F0901BE5 (void);
// 0x000001B4 System.Object Growth_Menu/<stopwatch>d__89::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB12092191FCC417B85E6D13C38F06E3CA2C4F11 (void);
// 0x000001B5 System.Void Growth_Menu/<stopwatch>d__89::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__89_System_Collections_IEnumerator_Reset_m7021A424310BDF3F5810B457A25C99FCC22AC91C (void);
// 0x000001B6 System.Object Growth_Menu/<stopwatch>d__89::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__89_System_Collections_IEnumerator_get_Current_mD7A472DCB5A7C94F8F0C339FD81B4D4942CE2B60 (void);
// 0x000001B7 System.Void task10::Start()
extern void task10_Start_m881E5E8DBDA0422AB51A87880988BED1C1646A83 (void);
// 0x000001B8 System.Collections.IEnumerator task10::stopwatch()
extern void task10_stopwatch_mC1E54BEF6E6D680A676099BC00F6CB0FFDE8D2EF (void);
// 0x000001B9 System.Void task10::answer()
extern void task10_answer_m2714F5DEF614520D613DA5388F86E335E1C07FF4 (void);
// 0x000001BA System.Void task10::showhint1()
extern void task10_showhint1_m016FDB31177588D8017CBC318EAEDF9ED49B6B1C (void);
// 0x000001BB System.Void task10::notshowhint1()
extern void task10_notshowhint1_m1E83AC7BCD63D2B8D78BCE3F2EE82DC2A2DA1E1A (void);
// 0x000001BC System.Void task10::showhint2()
extern void task10_showhint2_m85A01E4C96873825FADE814890F2E9746B7F99C3 (void);
// 0x000001BD System.Void task10::notshowhint2()
extern void task10_notshowhint2_mB19713F87318AEB083D8887473DE12A5A62AB681 (void);
// 0x000001BE System.Void task10::showhint3()
extern void task10_showhint3_mCCBB67DF9DF31B118F4F64CB1A5CCA4E8BE8B955 (void);
// 0x000001BF System.Void task10::notshowhint3()
extern void task10_notshowhint3_m276ACA285A05741EC0F44444D1DEF98A7AB50CD4 (void);
// 0x000001C0 System.Void task10::notshowver()
extern void task10_notshowver_m469F61FCCD2F2E0ED2C423A8954D610C081D0428 (void);
// 0x000001C1 System.Void task10::showstatistics()
extern void task10_showstatistics_m803B3B5FAF53D7F7700B6D94DD2D0F79D0855389 (void);
// 0x000001C2 System.Void task10::notshowstatistics()
extern void task10_notshowstatistics_m29821EB3DB8376DB483A0E15E250B22753147856 (void);
// 0x000001C3 System.Void task10::showar()
extern void task10_showar_m59E636D6CACBE026F2D1A96AC91A71A3F9B63D9A (void);
// 0x000001C4 System.Void task10::notshowar()
extern void task10_notshowar_m839EEFF3495CABC264E871F2DE1E55F22747417F (void);
// 0x000001C5 System.Void task10::.ctor()
extern void task10__ctor_m56565A0D78CD005D9800EAAE57F8F997D2A68B52 (void);
// 0x000001C6 System.Void task10/<stopwatch>d__30::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__30__ctor_m67EEB770039EA89A16B5BE37CD3B2B4A0E1380EE (void);
// 0x000001C7 System.Void task10/<stopwatch>d__30::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__30_System_IDisposable_Dispose_m83A03F429A791E0CA6455A76F3A3DE23B3927443 (void);
// 0x000001C8 System.Boolean task10/<stopwatch>d__30::MoveNext()
extern void U3CstopwatchU3Ed__30_MoveNext_m50FFCCCA93FA8AD29B213688A13F6770A0364938 (void);
// 0x000001C9 System.Object task10/<stopwatch>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4A896137509D01794355AE9276BC4B4A1703102 (void);
// 0x000001CA System.Void task10/<stopwatch>d__30::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__30_System_Collections_IEnumerator_Reset_m5E7FA5648CD08E9E609369D53888336719995879 (void);
// 0x000001CB System.Object task10/<stopwatch>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__30_System_Collections_IEnumerator_get_Current_m77EF3C48D26D8097CC84976ECEF616B5F6B40478 (void);
// 0x000001CC System.Void task8::Start()
extern void task8_Start_m151B9EC6287C8B230F68D4250E17BF845CF8114B (void);
// 0x000001CD System.Collections.IEnumerator task8::stopwatch()
extern void task8_stopwatch_m792AAF04C893B1D23E2967D06C904366B49545AA (void);
// 0x000001CE System.Void task8::answer()
extern void task8_answer_m77D1CD75AF48DD0420B7EED302B1156548C7045B (void);
// 0x000001CF System.Void task8::showhint1()
extern void task8_showhint1_mA14CDB363E25AFEAEA22031658201A7EA6E4F13E (void);
// 0x000001D0 System.Void task8::notshowhint1()
extern void task8_notshowhint1_m57B0BBD2E2B7CECF50B1E450615C95FECDD4E24E (void);
// 0x000001D1 System.Void task8::showhint2()
extern void task8_showhint2_mC544FED42165AFBCE089988EAE2A7619E435FFEE (void);
// 0x000001D2 System.Void task8::notshowhint2()
extern void task8_notshowhint2_m30584DCE942668BF0B85CE5B5FDBB68E3700696B (void);
// 0x000001D3 System.Void task8::notshowver()
extern void task8_notshowver_m4321DD9DB476DFB7861ECD347F76FC47B9AF0E3B (void);
// 0x000001D4 System.Void task8::showstatistics()
extern void task8_showstatistics_m729627A2779BE7B78A079DAB4A2E370CD656751B (void);
// 0x000001D5 System.Void task8::notshowstatistics()
extern void task8_notshowstatistics_mF21D153ECFE1D7846025279475326E493B0638DD (void);
// 0x000001D6 System.Void task8::.ctor()
extern void task8__ctor_mAC9B9EF8027C9FB1386EBF64DE5352E2BD98921F (void);
// 0x000001D7 System.Void task8/<stopwatch>d__23::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__23__ctor_m5B9E8360E8BACEF61B8A182C111973FD1348B85A (void);
// 0x000001D8 System.Void task8/<stopwatch>d__23::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__23_System_IDisposable_Dispose_mC5B5F01A01B2C74E8F59E85D83BEE98A0A848C71 (void);
// 0x000001D9 System.Boolean task8/<stopwatch>d__23::MoveNext()
extern void U3CstopwatchU3Ed__23_MoveNext_m387CFACA3B4E273FDE11DBBA5841FF7E45316815 (void);
// 0x000001DA System.Object task8/<stopwatch>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DC2068FC9E25C3F02C41FD773CDD13A2A496AF9 (void);
// 0x000001DB System.Void task8/<stopwatch>d__23::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m3DD586B5B64D48A3E26DE70E99D865E74BC6B68E (void);
// 0x000001DC System.Object task8/<stopwatch>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_mFC8554F3B08E90BC922E05C865A02425975775C8 (void);
// 0x000001DD System.Void task5::Start()
extern void task5_Start_mF7BA4D594AE21B29261701C1CC98C1766AC7ED09 (void);
// 0x000001DE System.Collections.IEnumerator task5::stopwatch()
extern void task5_stopwatch_m8F9D0516FDC32240E288A60CEBD9F88F6BC8CB9D (void);
// 0x000001DF System.Void task5::answer()
extern void task5_answer_mAD95124A6748BAAC990A8F2958F3AAC3526C869D (void);
// 0x000001E0 System.Void task5::showhint1()
extern void task5_showhint1_m136196EEAD1ABC9EBBBCF5ED516581D5C752FE86 (void);
// 0x000001E1 System.Void task5::notshowhint1()
extern void task5_notshowhint1_m10BB18B54F72282A630AC511AE59DF67D30CFEA6 (void);
// 0x000001E2 System.Void task5::showhint2()
extern void task5_showhint2_m8D36872BDA39EA1CF59F751AF65B912EE6F7B822 (void);
// 0x000001E3 System.Void task5::notshowhint2()
extern void task5_notshowhint2_mEAF4E26E38C44F1AA8C26D98E199A7535F1E56E0 (void);
// 0x000001E4 System.Void task5::showhint3()
extern void task5_showhint3_m86702356C7F32003C0C04C5F533A4EE183F930AB (void);
// 0x000001E5 System.Void task5::notshowhint3()
extern void task5_notshowhint3_m012E12286EE4661FC2681BD038D61F359592A372 (void);
// 0x000001E6 System.Void task5::notshowver()
extern void task5_notshowver_mE5CDA86BAF719935FEDCA564D6EA213B1EC3F626 (void);
// 0x000001E7 System.Void task5::showstatistics()
extern void task5_showstatistics_m8631BD548F01336041D16CFBDBB8918333D64365 (void);
// 0x000001E8 System.Void task5::notshowstatistics()
extern void task5_notshowstatistics_mD8FE95CFAAF4317CEA4C1FEB8F2F28C49D3826E6 (void);
// 0x000001E9 System.Void task5::.ctor()
extern void task5__ctor_m0DFF264E71F3228A76283FF05DB5204F5AFBD7A1 (void);
// 0x000001EA System.Void task5/<stopwatch>d__28::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__28__ctor_mD77B2A6660FF72176CB31E51282D3F5FB5D3E06B (void);
// 0x000001EB System.Void task5/<stopwatch>d__28::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mEBE25A514B0A74F3AA2E84D18F4DB811D942CF11 (void);
// 0x000001EC System.Boolean task5/<stopwatch>d__28::MoveNext()
extern void U3CstopwatchU3Ed__28_MoveNext_m2461C28DD63283CDBA86482258B75733CA4DD368 (void);
// 0x000001ED System.Object task5/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8217D2CBE113C96D6D5B16660EE4C488BDAD3F2 (void);
// 0x000001EE System.Void task5/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m740F6D1D83CC0F796D56F666CA6391FE4AE06534 (void);
// 0x000001EF System.Object task5/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mCFA409562E12000D407977266C8AEF134C99D96D (void);
// 0x000001F0 System.Void task14::Start()
extern void task14_Start_m01BA63D8B9C26ACB047CB973287A3DCFAB968A8F (void);
// 0x000001F1 System.Collections.IEnumerator task14::stopwatch()
extern void task14_stopwatch_m7A1EB79B71C66BC46A7251CCA33C672FAC4E0E3A (void);
// 0x000001F2 System.Void task14::answer()
extern void task14_answer_m76107A10607B307E10B6937892F41816F5E364F9 (void);
// 0x000001F3 System.Void task14::showhint1()
extern void task14_showhint1_mBA71C197F6D96368FA0A4106E652BEF68337A1E8 (void);
// 0x000001F4 System.Void task14::notshowhint1()
extern void task14_notshowhint1_m943C3524A427E15F40676054452E82D6FD890191 (void);
// 0x000001F5 System.Void task14::showhint2()
extern void task14_showhint2_m2315F70F697DAE2862B1E5241DA434DE1FB16026 (void);
// 0x000001F6 System.Void task14::notshowhint2()
extern void task14_notshowhint2_mB16F7E4C814D8771256563D27C3666F895561987 (void);
// 0x000001F7 System.Void task14::showhint3()
extern void task14_showhint3_m195D10323A853E5A476EE2A2B0A169EFF1DE9BD9 (void);
// 0x000001F8 System.Void task14::notshowhint3()
extern void task14_notshowhint3_mBFF62C2C4B7867A7264D15A1C250617F8663B6A5 (void);
// 0x000001F9 System.Void task14::notshowver()
extern void task14_notshowver_m13F63153519845147808E2F5E859428691F3DDF6 (void);
// 0x000001FA System.Void task14::showstatistics()
extern void task14_showstatistics_mFA59E9B9A7680DFFF10712B144B7201D3E93E07C (void);
// 0x000001FB System.Void task14::notshowstatistics()
extern void task14_notshowstatistics_m6AA71A73D89E0A0A2A690C14B7DC3FA149B659AE (void);
// 0x000001FC System.Void task14::.ctor()
extern void task14__ctor_m4B1A2B0494D1DAEDC28E0626377D64245892F154 (void);
// 0x000001FD System.Void task14/<stopwatch>d__28::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__28__ctor_mADF9EB2286862B8704555364FEF28EFEDA4C7131 (void);
// 0x000001FE System.Void task14/<stopwatch>d__28::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mE47CABED9599CAAB2F95B4412B72841BC3ADB12D (void);
// 0x000001FF System.Boolean task14/<stopwatch>d__28::MoveNext()
extern void U3CstopwatchU3Ed__28_MoveNext_m9EAFDC895E5B4C279FC416ED0314E9C0FBC5F697 (void);
// 0x00000200 System.Object task14/<stopwatch>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30D1FD80706BDAC139576BE4056F3953BBA85FD2 (void);
// 0x00000201 System.Void task14/<stopwatch>d__28::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mB27C2CB5C3F413507FB1A24EAD9508754AFE788A (void);
// 0x00000202 System.Object task14/<stopwatch>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_m361F1862F46FA07DCF1004AE2E3B02A769B517E6 (void);
// 0x00000203 System.Void perehodmodul3::Show_Z1()
extern void perehodmodul3_Show_Z1_m4D444A82BFEA76843683EE2C22ABB671393B4E7B (void);
// 0x00000204 System.Void perehodmodul3::Show_Z2()
extern void perehodmodul3_Show_Z2_mEF7A643E2B5392A8B61D3C7B0560D997B6AA40CF (void);
// 0x00000205 System.Void perehodmodul3::Show_Z3()
extern void perehodmodul3_Show_Z3_m5C90AC0970D2C8B85293CD75FDC41323930DCD5F (void);
// 0x00000206 System.Void perehodmodul3::Show_Z4()
extern void perehodmodul3_Show_Z4_mEFDEDDB16FBE3E6FD449E80279BA1DF450A23D12 (void);
// 0x00000207 System.Void perehodmodul3::Show_Module()
extern void perehodmodul3_Show_Module_m22500D27239F7D451D1B4EB707CF7ED993568539 (void);
// 0x00000208 System.Void perehodmodul3::Show_MainMenu()
extern void perehodmodul3_Show_MainMenu_mED406BD0428EB1B2351D3831B6778A397BDA1C39 (void);
// 0x00000209 System.Void perehodmodul3::Show_zadmodule()
extern void perehodmodul3_Show_zadmodule_mD5486C2751B18CA9F14284C495341CB9BA019FCF (void);
// 0x0000020A System.Void perehodmodul3::.ctor()
extern void perehodmodul3__ctor_m58A2A8CB16ECF566EE23BAB00FE0D23D9EDC5A40 (void);
// 0x0000020B System.Void perehodmodul3::.cctor()
extern void perehodmodul3__cctor_mB240E395DAAC574BA21848EF219A566C3DC79EF8 (void);
// 0x0000020C System.Void task13::Start()
extern void task13_Start_mC0B57EB527A41FC5574F0C7A1D25E8DE9F2BCE79 (void);
// 0x0000020D System.Collections.IEnumerator task13::stopwatch()
extern void task13_stopwatch_m77B093B433CBAD7202592DDC74C023A2D3D70AC9 (void);
// 0x0000020E System.Void task13::answer()
extern void task13_answer_mDBC8B9CB04D2B93214E59E40BD094CF8DE49F66F (void);
// 0x0000020F System.Void task13::showhint1()
extern void task13_showhint1_m7B690B0546FA82150F9CDC0B225663B1A7FF9BDD (void);
// 0x00000210 System.Void task13::notshowhint1()
extern void task13_notshowhint1_mB804858E231D81551F83C686A1AA793222BC9A44 (void);
// 0x00000211 System.Void task13::notshowver()
extern void task13_notshowver_m252C6FD269B27C7410C6194D9AABF4FA8FEBEBBB (void);
// 0x00000212 System.Void task13::showstatistics()
extern void task13_showstatistics_m2BA1FB8A6CE907738CE9C378DCA2CDFFFED1F6C7 (void);
// 0x00000213 System.Void task13::notshowstatistics()
extern void task13_notshowstatistics_mD25ACC5E8377B00CD0651F92F35A24B9760FEDA0 (void);
// 0x00000214 System.Void task13::nextuslzad()
extern void task13_nextuslzad_m09545BAF591FF70A7629A8E1900B3A192EBF8130 (void);
// 0x00000215 System.Void task13::backuslzad()
extern void task13_backuslzad_m973994419D9154085DC32856469FDC25897B664F (void);
// 0x00000216 System.Void task13::.ctor()
extern void task13__ctor_mEC564BE2E35F72C6AC878B6497A728545619623A (void);
// 0x00000217 System.Void task13/<stopwatch>d__20::.ctor(System.Int32)
extern void U3CstopwatchU3Ed__20__ctor_m5097B750882C8C6F075C006F7B905F6D84E8192E (void);
// 0x00000218 System.Void task13/<stopwatch>d__20::System.IDisposable.Dispose()
extern void U3CstopwatchU3Ed__20_System_IDisposable_Dispose_mF6E0CFC6F4701037477F2D596E241506C92B58B8 (void);
// 0x00000219 System.Boolean task13/<stopwatch>d__20::MoveNext()
extern void U3CstopwatchU3Ed__20_MoveNext_m68163B3E3F1915D214889414AF4D7603F0C2D31B (void);
// 0x0000021A System.Object task13/<stopwatch>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopwatchU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m491ECAB186ECE85BF8E47174E1A904BB3E7006F5 (void);
// 0x0000021B System.Void task13/<stopwatch>d__20::System.Collections.IEnumerator.Reset()
extern void U3CstopwatchU3Ed__20_System_Collections_IEnumerator_Reset_m530955FABF01848A268C5DD8435F802D7CC574DA (void);
// 0x0000021C System.Object task13/<stopwatch>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CstopwatchU3Ed__20_System_Collections_IEnumerator_get_Current_mC0EBDF136A2968A7187D73F079B37FA79CF3D726 (void);
// 0x0000021D System.Void TableUIExample::OnChangeTextValue()
extern void TableUIExample_OnChangeTextValue_m9F3863D4C8B0408AFBFE73567781702C1193CBBF (void);
// 0x0000021E System.Void TableUIExample::OnAddNewRowClick()
extern void TableUIExample_OnAddNewRowClick_m338F19CC09329ED6FC5D94EB3A4C346E5D6BBB4F (void);
// 0x0000021F System.Void TableUIExample::OnAddNewColumnClick()
extern void TableUIExample_OnAddNewColumnClick_m627102EC50EAB214AEDEA80306E366A0000E58A1 (void);
// 0x00000220 System.Void TableUIExample::OnRemoveLastColumn()
extern void TableUIExample_OnRemoveLastColumn_m5A5E4E04EDC9764083B32725CEF002642CB8E18E (void);
// 0x00000221 System.Void TableUIExample::OnRemoveLastRow()
extern void TableUIExample_OnRemoveLastRow_m70C5718BF00FBF3D58EEAC662897569D159A2536 (void);
// 0x00000222 System.Void TableUIExample::.ctor()
extern void TableUIExample__ctor_m0D49A5BCB167C398D1A3229E726FEAB189326733 (void);
static Il2CppMethodPointer s_methodPointers[546] = 
{
	task18_Start_m1AAE1EFFA5315271B1197642276249E743BF3C70,
	task18_stopwatch_mEA8809D2BEF7BC2CE791218486EE678C51AC41BA,
	task18_answer_m30E40F26D9BAC45E527BE61F623C1F1D2A571C43,
	task18_showhint1_mB1C3D2D881AE78E4D5A55D7602937E52116D0D20,
	task18_notshowhint1_m6E9DD3A71239F426A8383961B87C3D52F9F6453E,
	task18_notshowver_mFD72CD1A5C4BB5913AAA58AAD9FC8401C5F516C5,
	task18_showstatistics_m5650CA72DE99959FAE28FB35712EF51F25181E0F,
	task18_notshowstatistics_mBD2FC50E4562BB95666567B0F226BEF0F9C5D5A9,
	task18__ctor_mEF834A6BC1981BA55DD06B3CD1A094F22EC4A6E2,
	U3CstopwatchU3Ed__18__ctor_m9C98BAF99D3194A475B10F43E12EC1467D6EF023,
	U3CstopwatchU3Ed__18_System_IDisposable_Dispose_m03C6C59F8C3C0FF5A36144D3BDC693CFA69061C4,
	U3CstopwatchU3Ed__18_MoveNext_m42F5DFE4E1380ADEACAD6EEC6699B18FED7C83EC,
	U3CstopwatchU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E33CCBBE0AD7F1388E75C63050C728C23B1329B,
	U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m4A765E673B465AB278707B6FF492C7595152324E,
	U3CstopwatchU3Ed__18_System_Collections_IEnumerator_get_Current_m3E57CA1A69F2ED7DA1C3B19DD22899A8C13ECB93,
	task1_Start_m3CDDCAABF1E9FCDF93D124E6F74AB1B5DDC0A55A,
	task1_stopwatch_mAC140E804336901EE2D25D9A2D6C6CBE8A96CB4E,
	task1_answer_m03F536155A966C9965198798BCCD295C25E3B687,
	task1_showhint1_m4C4905A5980A8F60285FD3937709A023A990762B,
	task1_notshowhint1_m58B495C81C4CF335F65500D9336CC32FA14DA61F,
	task1_showhint2_m189F7CF98EA1C9A47263D3EE311530951C3B790F,
	task1_notshowhint2_m745C6BE79B610C92FE18E83DCD2923D3F6A680ED,
	task1_notshowver_mBD486C906F67837E25A8836358EB6D8691D6006C,
	task1_showstatistics_m9425EFA1B3BFBE3369ADB102FE515E869153C96A,
	task1_notshowstatistics_m97C72FEA1BDB8AB2B05F178ED137E45C89F06598,
	task1_showar_m3156D703A467152E915900D8F55103D2884093D0,
	task1_notshowar_mF3D02CFDB8679942256A373796F833F63F30C749,
	task1__ctor_m6BB42C0D3B2B26637FD35AA11D1868EAB472A1A5,
	U3CstopwatchU3Ed__25__ctor_mF4FF86AF458829AF5663B0E71B4745CC95329BCD,
	U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m58A3826412DA336685BCA3CD51C27470C8FD760F,
	U3CstopwatchU3Ed__25_MoveNext_mB49BCAFD4060480C1F2071848D3918B43DFF9D95,
	U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m958693B16603CEF13E2250BFE3595E3EA6A9884F,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_mBCC6462A07E23707896D3C34E923E1ED21704053,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_mB2C5972D09EA4D0BD26BB7F009188AC2D95626E0,
	task4_Start_m0B1D772E15AB95915391247131C17D5067FED8D8,
	task4_stopwatch_m95DDCF773F9417486F4A3B7DFD7F9304044095C9,
	task4_answer_m4DE52F25C330730E1191012EC4F210C871D0D333,
	task4_notshowver_m1413B9D28D4E264B82631BFD301BF6D23E0BB28D,
	task4_showstatistics_m1EE69CE4A686DEB76C973DD61926BCBAF48188BB,
	task4_notshowstatistics_mB117841FA16A32F8CE605E9BC01FA86B31D364AD,
	task4_showhint1_m41D33F7070217066E07F6B43AFF81A2B5A761EE1,
	task4_notshowhint1_m5032296A1C5864BF54B9C840D5D2D3842E4697CD,
	task4_showar_m37988850128214C0E8685CB89044779FD5FC01CC,
	task4_notshowar_mB09DBBFE50531517979CAE5A298A275120CD462B,
	task4_nextuslzad_mD8368986A60FA50E94965FD5B3D67587A43928EC,
	task4_backuslzad_m5DB41551643432B8608F84D9A0D0B9F8B7C0FAF7,
	task4__ctor_mA47FE455EA13015141BBE3690DB9CC612842C58B,
	U3CstopwatchU3Ed__25__ctor_m409BDAEB39E1686BFF71A2B46CFF8E553453EF8C,
	U3CstopwatchU3Ed__25_System_IDisposable_Dispose_mC9B982DB50D40262AEFB26036F95AD5864144C49,
	U3CstopwatchU3Ed__25_MoveNext_m85115A36EDAA0AC8AC668128539790BE3606EAE4,
	U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE196CC004A5D105E636406B83FC98CF574D25241,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m2E2D4AA52F089E0BD63A0C14067D282CAB53530B,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m2B920A2072A94FABED0614800B71C1232E07D828,
	task16_Start_m317742F3C73D2B7672E7812FFCFCE805D1A2CC6C,
	task16_stopwatch_m2A2DB95C40486C258C29A239F25E038055F058F7,
	task16_answer_m79F86560D6BDC6059C14154FE39871EDAA94560F,
	task16_showhint1_mC4858AB59D368A069B52F03A0456BD97F0A4A301,
	task16_notshowhint1_m955FDC707BCD8878FA96F119FD8CD63F13A5F3B4,
	task16_showhint2_m802C46B34FA7C32AFADF71D073CEAC045EE2D837,
	task16_notshowhint2_m707CA7778ADBBC07638B93B6272732970F4A5E7A,
	task16_showhint3_m7CC7237E99791ADD407B8349E3316F9807BCBE3C,
	task16_notshowhint3_m734B422B1420EE174E2DA333D103F22C043896EF,
	task16_notshowver_m8B274DFEC93ACA4E896B4A7D34E2886E3C059849,
	task16_showstatistics_m8631ECBD50838FBDB934B13E98C78CAC08551DA5,
	task16_notshowstatistics_m856B11F8CE23FD0E58CA027494D3C4653C006677,
	task16_nextuslzad_mA238B4AAA6E54F1CAF82372B790ECBD667579F3D,
	task16_backuslzad_mCBD3E2E86CD703E6018732874431C4FB29AFCE6B,
	task16_ForDropdown_m32DAC402A1398DA24AF07E31CCE3AA8DDDCF9236,
	task16__ctor_mBABF3C4619737B84F809810132CD363326569D31,
	U3CstopwatchU3Ed__36__ctor_mC91A3522F3CCBEAA4251614DF2EC589EA21268DA,
	U3CstopwatchU3Ed__36_System_IDisposable_Dispose_mC56EACFB8272315F17AEC51D52E44D929EBDBDB9,
	U3CstopwatchU3Ed__36_MoveNext_m8FA85B029768A5A1E1E96A348237F403EB9CCDEA,
	U3CstopwatchU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF8028E7E338FB86B13B67CFB381A07ACB18A1F9,
	U3CstopwatchU3Ed__36_System_Collections_IEnumerator_Reset_m7DD2F20B5E3227FC44D10C623A4A8AD250F13DD4,
	U3CstopwatchU3Ed__36_System_Collections_IEnumerator_get_Current_mD652CCD1701967AA887203CE9237F53EA0966A08,
	task9_Start_m1346924DC336947D32338D7C98AA89A90E3F34B8,
	task9_stopwatch_mA46BEEFEE49A0E9B29FD7D2AB9077C88D398B66F,
	task9_answer_mC8C3825F296DACFD1F437C2309A82B84D1911DF7,
	task9_showhint1_m01431923ED781182C2741837D584361D4E29A334,
	task9_notshowhint1_mFF25165362A6516466316A300C365C877344C096,
	task9_showhint2_m449E4E5B405A3E4FF96D815D751923D6E396725E,
	task9_notshowhint2_m2000D9FE732292BD834BF8FFF15B49EB5B9633DF,
	task9_showhint3_mA5B2DDBF1A42E6ACB46DA5ABD453FCB37282D4D1,
	task9_notshowhint3_m19EAC40B6306519BA8B5631DE19A622C89A077F2,
	task9_notshowver_m645F024609592C8484F4C96ED7DEE30B83B58F0F,
	task9_showstatistics_m0124FBE9B774C311F79DDEE10E77438D74D2B21F,
	task9_notshowstatistics_mBC94AB312464C30BC7CC9A23ED041EC8F84046A7,
	task9__ctor_mA1FBA6261F9771C8D715EAD93286601F4A15E812,
	U3CstopwatchU3Ed__28__ctor_mAF135CD78100517B8A047C1CC0CC7FBF7D721E99,
	U3CstopwatchU3Ed__28_System_IDisposable_Dispose_m968F8B16096B885F620B0C59DD55845E2A2C4DC5,
	U3CstopwatchU3Ed__28_MoveNext_m100E4BFAD41A0C25C2FBB6B9B85E4F97FCD56F1A,
	U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0FB3F188DE6B33A3AA00CEB99D557C86D745987,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m0EABDE0C4FBC3676A84E8E420FF9CBE70274ED20,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_m471CA932302E91AA3E2CA207064D99F4926A2BE3,
	task12_Start_m12F3B82FC31A306AA7F2AEFDB1A73DF38C645801,
	task12_stopwatch_m97E6C7D2705FF64D3031D1398AC66FD15D2EBCBA,
	task12_answer_m8F3E12E11B29812C5C337A5370A521A0EF646172,
	task12_showhint1_mFE34419C0088A5F8349A00AF44E44F197291A4A8,
	task12_notshowhint1_m91EC270DCE1562236BF9966E0A46CB7A560098ED,
	task12_showhint2_m32F9D47FA88DB394A6C5D7BC0D2D2EE7F81C4FCC,
	task12_notshowhint2_mECD99D666EE7C93E16F065B63A0C119804C51112,
	task12_notshowver_m744582D246AEFE64474182F8DCC05DF12002C5ED,
	task12_showstatistics_m70353CAC3CD7F6C59A2C61CAA2673C4438E47C8F,
	task12_notshowstatistics_m0D59F1CABFECCAD8913D4B5125C3FBCF85C1547D,
	task12_showar_m48FF7FFAF93DDFA744ABAC9B1EDCC34A003E0FEF,
	task12_notshowar_mA63527CEC7A87103E5554320AF1D6EAD1DEF4C6C,
	task12_nextuslzad_mD83718B7F670D3225A5DDEDA82AB7348FC08AC51,
	task12_backuslzad_mCF42045F3BE86A0B8244A5B2ABDD33F554D9EF65,
	task12__ctor_m4EF394A45514CA1BEF91BA0D200F1685DBF4C442,
	U3CstopwatchU3Ed__29__ctor_m1B84C54EC9B518E9CECE83E837776F3276385838,
	U3CstopwatchU3Ed__29_System_IDisposable_Dispose_m9D3E8528D9D30B0C5D2E52E3C4DBE0D9E1CEFB71,
	U3CstopwatchU3Ed__29_MoveNext_m343E5682D5AB7AF38E83737EF657C6D6D5500F87,
	U3CstopwatchU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m972587A5720ABA5F6F3CA4A8EC5CA5508561F00C,
	U3CstopwatchU3Ed__29_System_Collections_IEnumerator_Reset_m1E65CC4AC36D6F6B3E4A565402C9059B28CFCC56,
	U3CstopwatchU3Ed__29_System_Collections_IEnumerator_get_Current_m1E936383D0787D7C393CC2B8F592717EB3257F92,
	perehodmodul2_Show_Z1_m48D40224BA4C5FFBEB8BCC6062679C9E22DC2C96,
	perehodmodul2_Show_Z2_mA53C0CDBCCA4A674440C56A563C6D35392322AF9,
	perehodmodul2_Show_Z3_m585561BFF2A2B80C5360620B91BDD8EC373DC644,
	perehodmodul2_Show_Z4_mE475D1889E226DFE1FDEDFCA53CCFE17E991EA65,
	perehodmodul2_Show_Z5_m09074BC5D8C36079E0D17A6A37FAAAA682816779,
	perehodmodul2_Show_Z6_mBB8C9A35428D6A927664AE0F3E7E1305DCCFCB87,
	perehodmodul2_Show_Module_m77AACA54583ED62737DD681DEDCC5C16824B2355,
	perehodmodul2_Show_MainMenu_mFEDE69C68D3917E0F1FD7BD0D961D43008C62E44,
	perehodmodul2_Show_zadmodule_m21211DBCD5BBB387F31F7822321244F070574968,
	perehodmodul2__ctor_mDAA3949A688F12FF38836395463D1D86410FD643,
	perehodmodul2__cctor_m9D0CBAEEA949EF4F49098C09B97F693E6337D8BC,
	moduli_Start_mFE89B051CA2549C6BE4EDCDB82D950A04D3D1231,
	moduli_Show_Module_m2559DDF768ECA4FF64E654545F2970D4E5AE53DF,
	moduli_Show_Module1_m33831E350B0B6FF193AEA728A1E6E373B9FE3533,
	moduli_Show_Module2_m0476848E7788E818995ADCFCCE53652508D3E04E,
	moduli_Show_Module3_m93A2948617CBADDF29C79648DD0F47AA6D86989C,
	moduli_Show_Module4_mB7E3F120B9710790D05C76821ACDB919EF04E9B8,
	moduli_showhinfo_mEB85AB45CF79708B67A5403FEEA502CA2B2577E6,
	moduli_notshowhinfo_mA83845B83F2C39837D655F83D51F5183AC4EE01D,
	moduli__ctor_m3308BDC509003DF6019B20F9C617D84439CC2F2C,
	perehodMain_Show_begin_m28585DB2D7EDFFC5976D3062CB260D868CFC515F,
	perehodMain_Show_avtor_mF2DF10E84825192A855847EF76370B3E504F7B99,
	perehodMain_Vs_m51038803483A3CA815C57310A8F450888AE632D2,
	perehodMain_Show_MainMenu_mA1217E463C0A60282CD5F8C7103311A27D6708D1,
	perehodMain__ctor_m78B2C53711CF4CDE5E6F9607D93FC6C3E2E54B4C,
	task21_Start_mC02A24F02E0C44F76E1C6088425529C956755DD2,
	task21_stopwatch_mD216EDD0C1860FF19A24B72F9B5865BE6722F35D,
	task21_answer_m771D30DFF6B2BC45FBC0F08105BF1D2208EC0F17,
	task21_showhint1_m0F0E10E03EFB07A91E47C98529C41CC2950B3EC3,
	task21_notshowhint1_m3F620BE3D3479594B9700046048931D66529BC7B,
	task21_notshowver_mF5208B348997E2A48802E7244FA4E4FF5421C807,
	task21_showstatistics_m4FD072E97700A8CD5F4F94DCD4EFE75ACAC19C3D,
	task21_notshowstatistics_m208D024064A6A5A81E324CB076891F576069BD8D,
	task21__ctor_m8D95BED48C1F0816EB383E854D075070F1EEF41F,
	U3CstopwatchU3Ed__18__ctor_mC5DF3C8476F781C48C8C06C1F8FE1C134F0AD286,
	U3CstopwatchU3Ed__18_System_IDisposable_Dispose_m0267A6292474A75D1A4D0E2C18AA5AE28A9B4A73,
	U3CstopwatchU3Ed__18_MoveNext_mD4F5172969BAEA7BC62C0D21BC583263D6F81538,
	U3CstopwatchU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC82D427C0767A5292B87960F2E0ADA4B0C5EFB3,
	U3CstopwatchU3Ed__18_System_Collections_IEnumerator_Reset_m0282CAB192F651508DDBBFFA11D8BD5B9B2FCBA7,
	U3CstopwatchU3Ed__18_System_Collections_IEnumerator_get_Current_mA337C9225455F73B492AC4001B77EA49CD7DE6AD,
	task7_Start_m8ACF65834D00626B9F816B24C3D586DA40D5C925,
	task7_stopwatch_m2F4090704D490696CD3566C8DEDBF59931378068,
	task7_answer_mA837318B090BD474226E8228CCD10BA282AE3855,
	task7_showhint1_m1C967BBF52730364ECFDC5324A10475894C2A8E4,
	task7_notshowhint1_mCA2C39E5BEFDF259B03AB5D482331285E5CD7024,
	task7_showhint2_mA5567332F08A29B24C5423DE128A05FACAEDD739,
	task7_notshowhint2_mEB289CA39A752CF12D675D206795B8004EC53F65,
	task7_notshowver_m36B5C6076250C06796C6119993FDAEBC2CF6B2D6,
	task7_showstatistics_m4881D345BF29881F9A071B58C008831F140E9C06,
	task7_notshowstatistics_mF9E2E334A2DECF943F03505CCB1C315B3C32BDCB,
	task7__ctor_mCD393C645BEC915987B1302E147A69FD0F186F68,
	U3CstopwatchU3Ed__23__ctor_m1665459298850446F9749CA3A9D77E111D5C1874,
	U3CstopwatchU3Ed__23_System_IDisposable_Dispose_mA4BFB0E57050964E1A5B3804022C6012B1279CED,
	U3CstopwatchU3Ed__23_MoveNext_m594BDE2FC7552239BD3C27C7D8AAB496FCEFE196,
	U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7416789C781BCA7777273FBAB6AB3C544737DD2A,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_mBC582C5957C3035D3103ECE4C8A6EA16D5FDBB30,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_m521B0BF4CF9B3D14C05C896005F3AFC5A6DEB90E,
	task24_Start_m3A79A1745E191FF4A5BD6BEE61F643A73EBCE23E,
	task24_stopwatch_m8AB0569BCD18CFB04A79F428EC61DEDDDC71EEB5,
	task24_answer_mC791A3F462466B4A712A2C0A706FABBF3674BF3F,
	task24_showhint1_m2F430E83330DFDF98A174B574C559036DF9B22D8,
	task24_notshowhint1_m6CC526316584A94F1E3CF01FDC3AD0019235E116,
	task24_showhint2_mFC66D2C7E43B55789664CC9632FE0AA516A9F3EB,
	task24_notshowhint2_mEEF8EC41480EA2E5A9D32388686C45BD0970EBA1,
	task24_notshowver_mA7CE98DA19B53B118C77107435EF049211ECB722,
	task24_showstatistics_m0AC4C2A771E18B45491EFFF12B86B198E839DF25,
	task24_notshowstatistics_m40B2C82F3BE67D116AA4AC90BAC0114234D05287,
	task24__ctor_m71840F19BCED7C1106B466FBC7E3104C72155C3F,
	U3CstopwatchU3Ed__23__ctor_m4967F2F96916BB941EABA3D4B61599D03E007EC6,
	U3CstopwatchU3Ed__23_System_IDisposable_Dispose_m75ED6D4AD626186ADEE51E5878C872341053ED83,
	U3CstopwatchU3Ed__23_MoveNext_mC9A000339EC7785AE48935C671A1524C59E071D9,
	U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE4984AA6593344450A997E75276209C91B110D,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m87A3860173B5A6BF3C96B25BF9FD502FBC02953F,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_m3FF3FFB2AD9B752EF58B3E26095694374026412A,
	task11_Start_m1DA2920E46B912D2F00ABFD951E55C27BF151997,
	task11_stopwatch_mD52F69B056E4BF9C01104BB9249D0F65ED326313,
	task11_answer_mEF69DA0579065EE2C880FD31FAFDA5CE98CD9F5A,
	task11_showhint1_m06D8BA120D6B837BBA7D438EAD8ADB8635114660,
	task11_notshowhint1_mADCC045880BBBA3F4E47DFD3396AF4E59A7BF2C2,
	task11_showhint2_mB91B9420F48ABABE155753BECE6B7AE76DE18F47,
	task11_notshowhint2_m94C3C451FA6BA877AACBEEBCF7166F686B00280F,
	task11_showhint3_m52D5C3B1B30FE5EAAB050A356977E03431A87930,
	task11_notshowhint3_m7B23D99A26F698E790F35C6825AAA032914DE195,
	task11_notshowver_m83CFDECF9E1ECB2C1C8924FC52B7211FA9672A75,
	task11_showstatistics_m30BFA103663833A6541D89EE8E9D6B3EB2144594,
	task11_notshowstatistics_m70A822C5FDC7B75E13C5AD0FE964ABF3020CAD07,
	task11__ctor_m1A154E4B49711116A93674070D463B30A6A5E3B6,
	U3CstopwatchU3Ed__28__ctor_mAD20EA5F58F53CE11B28B998BAF88A97D3995725,
	U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mDFC70DC1B153CC1D99BAC77E5A8BFF8C41102385,
	U3CstopwatchU3Ed__28_MoveNext_m9C0CE3864B86C47D4209A7F6E4BB3E2DFCACA074,
	U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE24B00F4E674A2007FD5822A53FF45A4D1B8CB26,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mF94B5912F0DE9CE7169D2D881F14DE8DAB775B8D,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mE753FB07CB7582FE9A0DB6C25BCD75CB0FF0B069,
	perehodmodul4_Show_Z1_m1108703CB2B696AD0B8F5EAAF51CCC7565E63031,
	perehodmodul4_Show_Z2_m625B2F863DFB930EED5D6F73E4FF4A663D936C1D,
	perehodmodul4_Show_Z3_mAA58DE4A85D889C0E2A9C4A8FB94C4932A47239A,
	perehodmodul4_Show_Z4_m1E4B23FFBF319A6607ECD09994B502F35B223EEE,
	perehodmodul4_Show_Z5_m06B299B930C53B3FA03E50B74BCC3CAFD049EF3F,
	perehodmodul4_Show_Z6_m9B7DD3E4612274933461889E12EC2A2BDBB97C4E,
	perehodmodul4_Show_Module_m64FD8ED53471864AEAF5D5D531D18C8478332256,
	perehodmodul4_Show_MainMenu_mC4875713425DAE3AC1D5E93C5994C2BC2EF25C64,
	perehodmodul4_Show_zadmodule_m2B7D3D28C0CB69338EC915C3D84AEA7D8AB39F7A,
	perehodmodul4__ctor_m1937A637E8EF20C5D111A1BC82CDD8C5F66CDC81,
	perehodmodul4__cctor_m31086EB0D7B9B181FBDD1A450FCF9F290B013E32,
	task23_Start_m3DF5B0089FA92D710D4859DD099911F9FE8DE92B,
	task23_stopwatch_mE975F1CE89B92638B3883B1106EA88175E41D1FB,
	task23_VisibleRotate_m907DCE84B6FFD087E73261817722805381B71114,
	task23_answer_m5FB41B7B4530BFC677F5DD6D03DA7DD661754CD1,
	task23_notshowver_m25BDB45D4AE3239F8D755DE971AC2CD1AF2C9BA5,
	task23_showstatistics_m62AD2C5237C18A30607E97E3C6124A4764D90A90,
	task23_showhint_mF6F9DE22D73ADDCC40A91952DB30759B14D13E5E,
	task23_notshowstatistics_m4F8098A3E10F4CA1AFDE7CF9095F8F531D6FBF35,
	task23_showar_m9A2C0877643E12C6F011B32CFDFE9C69905638AF,
	task23_notshowar_mAF38B64C8AABFCF744047C33D0C02D1B759045A5,
	task23_tog_activePeople_m2906252B9A3AA6BD326FAC30CA8180DBAE28B955,
	task23_VisiblePeople_m8AD7861251C1838E8C6B9B263FB45DBC2BB88E2F,
	task23_tog_activeRotate_m8C37A2D5818BF1517C760C3D716E83A42FB25DE7,
	task23_notshowhint_mBB05ACEF9AAAD8B6D2B3A48B648319BD2640002E,
	task23__ctor_m1624DB91DBA641E4BDC0A2B73C6812A169588E8F,
	U3CstopwatchU3Ed__28__ctor_m20ACD953B1BBACC38DDA747FCE0ED1E7EF173B7A,
	U3CstopwatchU3Ed__28_System_IDisposable_Dispose_m7E24741B924CF192799351F26AEF913EBC0D8007,
	U3CstopwatchU3Ed__28_MoveNext_mDBBBC8C2A2240F7B135217ABB7C6920D4ED2A292,
	U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DE1E234065C895734C58CEE233C3B9DF8A732B4,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m96D72C34051F787294468EB7E08BA0C90BE22675,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mEE7F5DD2B78F2C4F3C7EDAEA620DA686B246ED76,
	U3CVisibleRotateU3Ed__29__ctor_m32FBBD56576C854E4E4A54DF1CB66A92DC28C367,
	U3CVisibleRotateU3Ed__29_System_IDisposable_Dispose_mBA331491978040189D256BEF30F5890E7FF8CF1A,
	U3CVisibleRotateU3Ed__29_MoveNext_mE9611179BF7AEBE7E18A8CCF892CA69FCC5E1601,
	U3CVisibleRotateU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF8E4E209E96AC964B05634BA177DD9E1DA4E81E,
	U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_Reset_m7CBDF83FC94C799459737385FF0C2E1CFFC5876D,
	U3CVisibleRotateU3Ed__29_System_Collections_IEnumerator_get_Current_m8E76273C216A8F4E4694A4F34E0D6B9270319218,
	U3CVisiblePeopleU3Ed__38__ctor_m561B71216B1355DF5FAD403B70C373D043A0E5F1,
	U3CVisiblePeopleU3Ed__38_System_IDisposable_Dispose_mC336EE931B19EB00A5636EABF1920B5395C98EF7,
	U3CVisiblePeopleU3Ed__38_MoveNext_mF8AD53C09E7A8DBDCB840D2B91980DBE665777D4,
	U3CVisiblePeopleU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9127517308D43B42DC64A6E6A94348B816BD5A2,
	U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_Reset_m4246B29506244980BE7A9236CCDF52291E841A50,
	U3CVisiblePeopleU3Ed__38_System_Collections_IEnumerator_get_Current_mBE1B58715B37AE1491A81FC814DBA37FBB200601,
	Task_Door_manager_Start_m61E86AC6B10DE353D02989A16FE369ABEC17A380,
	Task_Door_manager_btn_Hints_mAEBAD32B6C3F4F5E9549A3855D00A58DE05F1CBC,
	Task_Door_manager_tog_activePeople_mE862D6F9E2A5B84C3E127C3ABB3767BC7FF21587,
	Task_Door_manager_VisiblePeople_m56759FF90D89ABB1C1CCCFB9D8C1EFE99191541B,
	Task_Door_manager_tog_activeRotate_m1C759AF11EF24F052DBB46DAD0021883BB968BD6,
	Task_Door_manager_VisibleRotate_m9D42F74CEC4772F7B045976DE6F62991466B5583,
	Task_Door_manager_btn_Back_mE9C2821DAF9BDE683134BF8C8C28CA6E95EB3A99,
	Task_Door_manager_btn_Verify_m8B6E07029E2376B971E38049D5337731618E3447,
	Task_Door_manager_btn_Statistics_m38A49BAF95DCF97E9FB002DD404348FA244280DE,
	Task_Door_manager_notshowver_m556B0BDABE82C3B630DB06A59D1437674A7CDFE3,
	Task_Door_manager_showhint_m9BCA6A61BC831F7E5E292A2937891EBCCC57290A,
	Task_Door_manager_notshowstatistics_m4C1680309C60FBA1DE36DCB55670BE03D5F58DDA,
	Task_Door_manager_showar_m44CBA529E9459221D59B166B99E61D1D1FB22F69,
	Task_Door_manager_notshowar_m056D22FDCEDBDED9FF8C26CBC250D22E090876F0,
	Task_Door_manager_notinfo_m4F9A478F9DB50ED6B4431289CB27B98F66DD7273,
	Task_Door_manager__ctor_m2592C61D9BEB9179787DCBDC9DFEF63329325E60,
	U3CVisiblePeopleU3Ed__29__ctor_m4C06D63C1CC803EC17AC6A2252086760283F82AD,
	U3CVisiblePeopleU3Ed__29_System_IDisposable_Dispose_m9E8A5E3ED2027C01EABD58F8715DC6D64E9A2191,
	U3CVisiblePeopleU3Ed__29_MoveNext_mC1F19694A09408545418DC076BC23202FE7069E3,
	U3CVisiblePeopleU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02EB33F95B66F03FDCB145E76E542F5BBC14CCA0,
	U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_Reset_mF3D061165A076653B3044E1EEC3FE34959CDBBF8,
	U3CVisiblePeopleU3Ed__29_System_Collections_IEnumerator_get_Current_mA45ACC0C1443F361432F420CF4A5E040594DE296,
	U3CVisibleRotateU3Ed__31__ctor_m34B001F2552F17A0BD062867405FE3807320C9C7,
	U3CVisibleRotateU3Ed__31_System_IDisposable_Dispose_m7571664E714301A3F86C18A1948807192D09683B,
	U3CVisibleRotateU3Ed__31_MoveNext_m2A3118E4C49E683947C2FDD0B55A7D3620010364,
	U3CVisibleRotateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91DAAEAF951EB100F86F0C31704D80E325832865,
	U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_Reset_m3DEA0136564A7FBE1C91EF8659AD71DEA872B57C,
	U3CVisibleRotateU3Ed__31_System_Collections_IEnumerator_get_Current_m0A32639DB93E11597F23E763E9DCED2A594BAF92,
	task20_Start_mF533685D9A82BAC636B28DF5312F3BF696B8AADB,
	task20_stopwatch_m93DC59FD5E751D67252AC0E3BE7247D598206434,
	task20_answer_m554812542D41A02AF2D4D19EF73F792867CCFC09,
	task20_showhint1_mDFA29F24DF2D4D294648F3C3CF09BFB2B949E263,
	task20_notshowhint1_mA6846CBF26528EDF711411AFEC41CED9D7110AF3,
	task20_notshowver_m5674F9846E50B98BFEF6FF6A2CD13C30FC70313B,
	task20_showstatistics_m1B00594C5438989690DD40E5D09C71D71630FCF2,
	task20_notshowstatistics_m18F42B20B20B7D828C84050E53CD1A0CD552DC89,
	task20_showar_m5A5A1F7AB7661957EE90F0BCC6F677A5FA146F74,
	task20_notshowar_mD282D8948220BC9DC56437FCF17C6A53F21A9551,
	task20__ctor_m370415A6C3F20DD11C23C169FF3F567266379B09,
	U3CstopwatchU3Ed__22__ctor_m6FF962E670F0354B1BF097A4F6D365ABD8A8CEF5,
	U3CstopwatchU3Ed__22_System_IDisposable_Dispose_m10B0A8D9B1524330A1E820ED4E74F6B8ED334C43,
	U3CstopwatchU3Ed__22_MoveNext_m755FD9840826CBDD979FA6E3BAB0BA7181C0F05E,
	U3CstopwatchU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE8869F1C1CB3A564CFA22DD79C4BA855B8817C,
	U3CstopwatchU3Ed__22_System_Collections_IEnumerator_Reset_m3ECCA74D5197F50D38F45FEB9DF44710AA48C65D,
	U3CstopwatchU3Ed__22_System_Collections_IEnumerator_get_Current_m99C2E8CA3B5BD12A840488A4C2566B4AF44297EA,
	task15_Start_m6EB0BC04176274907071B5F00EDC646CE459ED09,
	task15_stopwatch_m857B335C8086453B6354F2D3F0BE344DCD93BD4B,
	task15_answer_mAAD9D66C4D1BB53B343E77993156C68E626F767F,
	task15_showhint1_m926551F5466C175BD8491C5CF6A5E7A0EAA76790,
	task15_notshowhint1_m92C2929AA61E1480014F708D7874E3501DB4BC93,
	task15_showhint2_mD12453AAE226EB208500330C872BC24B9507C4A5,
	task15_notshowhint2_mF38918D67803F3BE629461BCE3A83A6789971902,
	task15_notshowver_mAD2841186D6D793181CC894A00111EA040F63C7E,
	task15_showstatistics_mEFC8CEE07759B268371CC8C570F0AB43F493C001,
	task15_notshowstatistics_m33DB1025BC0687FB342E35DEA2E67789ADF71225,
	task15_showar_m84D9FBA12BA90E39F1FCC4FB8634FE671A4D6FD1,
	task15_notshowar_m66FC2733997F7E8C9443376EA9080106028FB989,
	task15_nextuslzad_m93F7F754A2A90EF8BEBBFA46F4A0593F8F435BF7,
	task15_backuslzad_mCD089CDF8822FD7E70BD984183FD591140E4B1AB,
	task15__ctor_m15D7CD3FDA4992323F534948AE1F3348277ED97E,
	U3CstopwatchU3Ed__27__ctor_m2F5D907BB12AB98E504E9D63C163FFDE0FFF6838,
	U3CstopwatchU3Ed__27_System_IDisposable_Dispose_m195C6BFB6CB1125CE2DBAFFC4D66754F377CED92,
	U3CstopwatchU3Ed__27_MoveNext_mF0249B2C4FB9D3C1D42A56B682ED56258B279ADD,
	U3CstopwatchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19807947C7AFE56A572BD94EEDDD297A4E9C6186,
	U3CstopwatchU3Ed__27_System_Collections_IEnumerator_Reset_mBA30D813137118067E8CCF6E4B421B60764FAB4A,
	U3CstopwatchU3Ed__27_System_Collections_IEnumerator_get_Current_m30B7BFAA601AAF6027028C4B2ECAC126BA82D405,
	CubeRotate_Update_m5329A8708151CE19E112FC96D731DADDE6E78748,
	CubeRotate__ctor_m8FABF7E0E452666796420B4BE9DCA4E64BD70767,
	task2_Start_mA76252C3325EBE77E36F02340536F5C1FCF3C0DD,
	task2_stopwatch_mE2A3C1694CCA003374884201048E08DF0EE56BA5,
	task2_answer_mFC72D92CE1238B594899EC1E09FC146B76B25F73,
	task2_notshowver_m14633FA86BD512BB228A0777BE07AFB1691CD9C6,
	task2_showstatistics_m87E64E1632DE256166AE6AEABA238B2F78986291,
	task2_notshowstatistics_m1B5C4DC751E25CE24C776B87A14A79A9593908F9,
	task2_showar_mCEF19E2CBBDD2EEB8E2E58D4B9F7295F3CA3178D,
	task2_notshowar_m349E31545F60799DFFB362C8D6BBBAFD5286937B,
	task2_showhint1_m7EA5948CCE3ABD87BB49F47CBF420382D676F5A9,
	task2_notshowhint1_m199BFBB9B5F4CB10986E084E8F4AA8724D8D922C,
	task2_showhint2_mEF8DC2FED820C4E160939C37493C65C4F9CD515E,
	task2_notshowhint2_m59FEEA9B1E29DD3E027FAF21890F4C198801DC8A,
	task2__ctor_m509B319AF9AE752692253595723EA2CB736742FB,
	U3CstopwatchU3Ed__23__ctor_m6A763E528C337619B93BA87550784E8CD7AEB70D,
	U3CstopwatchU3Ed__23_System_IDisposable_Dispose_m05F05ECBC6DDBB6F840407617AE050983215232A,
	U3CstopwatchU3Ed__23_MoveNext_mB8EE0A685B880CD5249656BCD077B5B30A62CDBB,
	U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD11EA88F3846CE46262CFB25D589596F212C0C,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m7687E13C63EB80EA50D717667B6A601259F2C817,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_mC1C7451693935FBC3A16C1529A24063300610D88,
	task6_Start_m0153FB064AEAA594FEEDF93D7206E57E6F022A17,
	task6_stopwatch_mE1D169201A449AE4106C2A25BAFD87D0E64FEB64,
	task6_answer_mBC1716119E4FAB35C676C654C523867B5B55DDEC,
	task6_notshowver_m17AA4B30E03D8B9C0A440B859B35475AFEEEA476,
	task6_showstatistics_m2A895869CDF30E62C90B700B37EE408E7E7C7E32,
	task6_notshowstatistics_m204E489F3604C7454F61EB21CDAD8707FA134ED2,
	task6_showhint1_m10B5A77D70D20ACBD5E2A56163F9440639C2D2E8,
	task6_notshowhint1_m6BD59DCD61198571580BB6F36EBF557767C7E1BC,
	task6_nextuslzad_m3D39D538A4A3BF92A1E04C3D79AF7465B7ECA010,
	task6_backuslzad_mD72CFE1AEAD9A6C161693C2673F846BFEA662F2B,
	task6__ctor_mDCB2060F7ED7DA50A69019BD6AC07D994307FDD8,
	U3CstopwatchU3Ed__25__ctor_mBE7488CED855751FE6A9873967B29D948A39278E,
	U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m53B282F84A7D3076991C9F51A5D8D05E510E1CC8,
	U3CstopwatchU3Ed__25_MoveNext_m1F504BD1F2F28B3C7A3AF309466FD7C96B5F1BB8,
	U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F3AEEC752291A571E12EB2CC7CE72B53A8958E9,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m896AFD90834C83785294BC43E7A161D3966D3DC6,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m429B23341905A2972FA9BCC40F1FD9F27285CBD3,
	task17_Start_m8D9C457CCE8CE04DDA700238DA4E4BE8B60DAB2F,
	task17_stopwatch_m250A4B51D7AC3DE3436B2380FC038A6577BBC735,
	task17_answer_mCD958FCA676848081930963384E4571EC89FD715,
	task17_showhint1_m2B3B0C12A9D2F6E7A2FC8FC8850428224749B5C7,
	task17_notshowhint1_mEFDD70AB171DE2AFA5D22E1B788B72C69525571C,
	task17_showhint2_m154361BF007D60D0AD8868383F2A25A3C9495352,
	task17_notshowhint2_mF659745FE8D2F702CC458EAFF6D855AE24B94091,
	task17_notshowver_m0B2A39583BBAD2DFA42B1921C4430C5FA3BCF1FB,
	task17_showstatistics_mFBA0CAF6F359C78DBB076464B37AF371BBC01BA5,
	task17_notshowstatistics_m5AE3EE321D44CAC645AEAD9A3858077F7F3CDBCE,
	task17_showar_m8D87DBD8DA7AEEFD8998449ADF4AA0F81F809815,
	task17_notshowar_m55B28011E60D5617F632116007C6F4683AA15C44,
	task17__ctor_mE62EC7FC104AE9BEB8F26EFF80CFC2B7315CCE67,
	U3CstopwatchU3Ed__25__ctor_mF47A706BF25A5698C8E4C2445AE0B63B9F7E7ABB,
	U3CstopwatchU3Ed__25_System_IDisposable_Dispose_mABF71C27E50CE6F620863B20B0FBE34600A93176,
	U3CstopwatchU3Ed__25_MoveNext_m5C32301722B7469C5589ADC687B1A513BDCD8915,
	U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD660F032C4E2C84CABEB86CAF8D6E7F12295F5F,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m93082B657640111769C616B95E39BA4DEB00CA36,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m76EF3B4BB6FFE23805A22FEB0DDE76A133536635,
	perehodmodul1_Show_Z1_mE89DBDFC63B7DFC79F7757B801AC59C19B5827E3,
	perehodmodul1_Show_Z2_m6F736CBF216742E3E8206BE84D1770F374B244A1,
	perehodmodul1_Show_Z3_m6DC752D9C486992F2461125FAB37A05A77AFC063,
	perehodmodul1_Show_Z4_m34B54AF42B189AB40555F42C25E7A7556E9AC0C0,
	perehodmodul1_Show_Z5_mEEBDB8AF0F3849C2D586CD2352D8B7FC3B7EE64E,
	perehodmodul1_Show_Z6_mB2DF05C533B21235F041F0B04578AB50CC695486,
	perehodmodul1_Show_Z7_mF5832523BC3C4637B12A71C88A2957303313FD7B,
	perehodmodul1_Show_Module_mEB554CB9CB8BBB884C20065CFFE1A4EE26BB91B3,
	perehodmodul1_Show_MainMenu_m614162B8305AFF8D9083839F99771620569FB6E3,
	perehodmodul1_Show_zadmodule_m175CF2CEBF36AD62B491E05DFCB8C13E83E416C7,
	perehodmodul1__ctor_mDE0461E54757B9DF125732C219BF22C48CBD0BFD,
	perehodmodul1__cctor_m81F7961CF0BA9A386B5D15F2E47D6F6676699EDE,
	task19_Start_m578957EBD321FD3BB4A0F4D33866020F82CED659,
	task19_stopwatch_m31B69CC2CBA23466BBFA045D897E57E049C20EA2,
	task19_answer_m51AB5D299521D313519AE438D9139F52FA59E02B,
	task19_showhint1_m95DD7DD077C362A5A4C655ACCB5887CDE9B5A17B,
	task19_notshowhint1_mAD4C6C8D04561767B5863F44E1343D26EE1635E3,
	task19_showhint2_m04A63664817B3B9523C2F3350067880CFEC2C892,
	task19_notshowhint2_m6F0E9BA917911CF4EF15C2D206269CDE6D9BAC8B,
	task19_notshowver_m2C4058021D44679D93BB0F5C2E910B9E9697DD7E,
	task19_showstatistics_m726AEE438FBC63FEF71968EBF1D104290C6C2432,
	task19_notshowstatistics_m27B568E87D6115A37BF995E53D3A42724F010F17,
	task19_nextuslzad_m149390164F4FEC7D402BC16D599C0A6525E2A9E7,
	task19_backuslzad_mF7E1CEABD221792521E926961A94201B68BAADDB,
	task19__ctor_m4914C174FB30C944160EDC9CAA4A07AB799833C3,
	U3CstopwatchU3Ed__25__ctor_mACB511D54A1CF1255DE5F80BD47A29E5DDA01A24,
	U3CstopwatchU3Ed__25_System_IDisposable_Dispose_m2DA8529AC2DCD42A557306FC5E0851194E493C31,
	U3CstopwatchU3Ed__25_MoveNext_m9330F7700F985C7D2A90AF73453E22C31320A1E5,
	U3CstopwatchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4906CE9EDBDEB222D97A4DC80E1A19B0BB6E09A7,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_Reset_m81FC0300E7EA90226FEF616DB832873969C16A32,
	U3CstopwatchU3Ed__25_System_Collections_IEnumerator_get_Current_m6FEAAF4424650347E2B7B41D23D7579F986CEC3A,
	Growth_Menu_Start_mEADF62E543FF2798C0E8B9A25A027EB1D925A5E6,
	Growth_Menu_stopwatch_mCBEF9E4055584D62459871F29F8440B11E27DB38,
	Growth_Menu_showar_m5CC26B8EBC609D737588DACC38C2AE1165DC58C6,
	Growth_Menu_ChangeColorForYesA_mC9159BABB08FBC512E44271E982F6CC859EE6FAA,
	Growth_Menu_ChangeColorForNoA_m063A46B1352462F3348389A05EDC1825F44F51E5,
	Growth_Menu_ChangeColorForYesB_m3A99D4C1B2570AE7DDECDB44E42C1EF174342963,
	Growth_Menu_ChangeColorForNoB_m2457BCAC4BC5E78B0E1ABD8B7E9E78C21D84B0D2,
	Growth_Menu_ChangeColorForYesC_m52434244BDFAC42A3A1CC74C7DFEA3F318CA1385,
	Growth_Menu_ChangeColorForNoC_mAA0B48C321A261A82BAD671BD475F91E36CA34A3,
	Growth_Menu_ChangeColorForYesD_m09017F7CA9490B48E89B2B2CE4BAB4D352A54A34,
	Growth_Menu_ChangeColorForNoD_mF192296DF26FF74EC55658EEF7009325D44AC069,
	Growth_Menu_answer_m9248041B9D2C21692F3F39E491BE4949604B15DA,
	Growth_Menu_BtnHelpBig_m8A2B0A77DE20966DE22342C198C067A8F7598A5D,
	Growth_Menu_BtnForHelpA_mBFA289F7265C12241714C7BF7AA03B14E89BDFF3,
	Growth_Menu_BtnForHelpA1_mA3EB874CA2332561963F3385455951D7B4C4A2E5,
	Growth_Menu_BtnForHelpB_mDD5454E7A8E966686ED687B784ADC9D9B59BC3FF,
	Growth_Menu_BtnForHelpB1_m1395C5771BE0BE27BAE8BEA57886EDBE02EB1421,
	Growth_Menu_BtnForHelpC_m3BC08A645A3A2F9DFC7307CDBF4C6CC9F22A3EFA,
	Growth_Menu_BtnForHelpC1_m0289494A20BBCE4AA1FEFB0717F33246A782CE40,
	Growth_Menu_BtnForHelpD_m7BC333394DDE8D84B35840A2F5DFC281E2F21118,
	Growth_Menu_BtnForHelpD1_m6F13A6854F26F5B940A1EE48F0C6B1A1869C6197,
	Growth_Menu_ForBtnOKad_m9DC2299B3309FC5ED9D731E05B14215D165F146E,
	Growth_Menu_showstatistics_m9A2740F18A101613AFB48ED4C71EF3278696FC23,
	Growth_Menu_notshowar_mA977602AF772B62B37D90897D6EC98A5E6E0BEBE,
	Growth_Menu_notshowver_m5917AC51417687872696FB2DEB60BC7A7603B475,
	Growth_Menu__ctor_m6DF50EED17A3ACA94D920E21E60DBA47F4A0EE31,
	U3CstopwatchU3Ed__89__ctor_m501F56E1C4172B9FDD76399C097A2D1C961CE401,
	U3CstopwatchU3Ed__89_System_IDisposable_Dispose_mCAA189524DF4478AE19409A5A44702805A223AFC,
	U3CstopwatchU3Ed__89_MoveNext_mAA6962B9EE57E075023054D41FE31EE0F0901BE5,
	U3CstopwatchU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB12092191FCC417B85E6D13C38F06E3CA2C4F11,
	U3CstopwatchU3Ed__89_System_Collections_IEnumerator_Reset_m7021A424310BDF3F5810B457A25C99FCC22AC91C,
	U3CstopwatchU3Ed__89_System_Collections_IEnumerator_get_Current_mD7A472DCB5A7C94F8F0C339FD81B4D4942CE2B60,
	task10_Start_m881E5E8DBDA0422AB51A87880988BED1C1646A83,
	task10_stopwatch_mC1E54BEF6E6D680A676099BC00F6CB0FFDE8D2EF,
	task10_answer_m2714F5DEF614520D613DA5388F86E335E1C07FF4,
	task10_showhint1_m016FDB31177588D8017CBC318EAEDF9ED49B6B1C,
	task10_notshowhint1_m1E83AC7BCD63D2B8D78BCE3F2EE82DC2A2DA1E1A,
	task10_showhint2_m85A01E4C96873825FADE814890F2E9746B7F99C3,
	task10_notshowhint2_mB19713F87318AEB083D8887473DE12A5A62AB681,
	task10_showhint3_mCCBB67DF9DF31B118F4F64CB1A5CCA4E8BE8B955,
	task10_notshowhint3_m276ACA285A05741EC0F44444D1DEF98A7AB50CD4,
	task10_notshowver_m469F61FCCD2F2E0ED2C423A8954D610C081D0428,
	task10_showstatistics_m803B3B5FAF53D7F7700B6D94DD2D0F79D0855389,
	task10_notshowstatistics_m29821EB3DB8376DB483A0E15E250B22753147856,
	task10_showar_m59E636D6CACBE026F2D1A96AC91A71A3F9B63D9A,
	task10_notshowar_m839EEFF3495CABC264E871F2DE1E55F22747417F,
	task10__ctor_m56565A0D78CD005D9800EAAE57F8F997D2A68B52,
	U3CstopwatchU3Ed__30__ctor_m67EEB770039EA89A16B5BE37CD3B2B4A0E1380EE,
	U3CstopwatchU3Ed__30_System_IDisposable_Dispose_m83A03F429A791E0CA6455A76F3A3DE23B3927443,
	U3CstopwatchU3Ed__30_MoveNext_m50FFCCCA93FA8AD29B213688A13F6770A0364938,
	U3CstopwatchU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4A896137509D01794355AE9276BC4B4A1703102,
	U3CstopwatchU3Ed__30_System_Collections_IEnumerator_Reset_m5E7FA5648CD08E9E609369D53888336719995879,
	U3CstopwatchU3Ed__30_System_Collections_IEnumerator_get_Current_m77EF3C48D26D8097CC84976ECEF616B5F6B40478,
	task8_Start_m151B9EC6287C8B230F68D4250E17BF845CF8114B,
	task8_stopwatch_m792AAF04C893B1D23E2967D06C904366B49545AA,
	task8_answer_m77D1CD75AF48DD0420B7EED302B1156548C7045B,
	task8_showhint1_mA14CDB363E25AFEAEA22031658201A7EA6E4F13E,
	task8_notshowhint1_m57B0BBD2E2B7CECF50B1E450615C95FECDD4E24E,
	task8_showhint2_mC544FED42165AFBCE089988EAE2A7619E435FFEE,
	task8_notshowhint2_m30584DCE942668BF0B85CE5B5FDBB68E3700696B,
	task8_notshowver_m4321DD9DB476DFB7861ECD347F76FC47B9AF0E3B,
	task8_showstatistics_m729627A2779BE7B78A079DAB4A2E370CD656751B,
	task8_notshowstatistics_mF21D153ECFE1D7846025279475326E493B0638DD,
	task8__ctor_mAC9B9EF8027C9FB1386EBF64DE5352E2BD98921F,
	U3CstopwatchU3Ed__23__ctor_m5B9E8360E8BACEF61B8A182C111973FD1348B85A,
	U3CstopwatchU3Ed__23_System_IDisposable_Dispose_mC5B5F01A01B2C74E8F59E85D83BEE98A0A848C71,
	U3CstopwatchU3Ed__23_MoveNext_m387CFACA3B4E273FDE11DBBA5841FF7E45316815,
	U3CstopwatchU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DC2068FC9E25C3F02C41FD773CDD13A2A496AF9,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_Reset_m3DD586B5B64D48A3E26DE70E99D865E74BC6B68E,
	U3CstopwatchU3Ed__23_System_Collections_IEnumerator_get_Current_mFC8554F3B08E90BC922E05C865A02425975775C8,
	task5_Start_mF7BA4D594AE21B29261701C1CC98C1766AC7ED09,
	task5_stopwatch_m8F9D0516FDC32240E288A60CEBD9F88F6BC8CB9D,
	task5_answer_mAD95124A6748BAAC990A8F2958F3AAC3526C869D,
	task5_showhint1_m136196EEAD1ABC9EBBBCF5ED516581D5C752FE86,
	task5_notshowhint1_m10BB18B54F72282A630AC511AE59DF67D30CFEA6,
	task5_showhint2_m8D36872BDA39EA1CF59F751AF65B912EE6F7B822,
	task5_notshowhint2_mEAF4E26E38C44F1AA8C26D98E199A7535F1E56E0,
	task5_showhint3_m86702356C7F32003C0C04C5F533A4EE183F930AB,
	task5_notshowhint3_m012E12286EE4661FC2681BD038D61F359592A372,
	task5_notshowver_mE5CDA86BAF719935FEDCA564D6EA213B1EC3F626,
	task5_showstatistics_m8631BD548F01336041D16CFBDBB8918333D64365,
	task5_notshowstatistics_mD8FE95CFAAF4317CEA4C1FEB8F2F28C49D3826E6,
	task5__ctor_m0DFF264E71F3228A76283FF05DB5204F5AFBD7A1,
	U3CstopwatchU3Ed__28__ctor_mD77B2A6660FF72176CB31E51282D3F5FB5D3E06B,
	U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mEBE25A514B0A74F3AA2E84D18F4DB811D942CF11,
	U3CstopwatchU3Ed__28_MoveNext_m2461C28DD63283CDBA86482258B75733CA4DD368,
	U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8217D2CBE113C96D6D5B16660EE4C488BDAD3F2,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_m740F6D1D83CC0F796D56F666CA6391FE4AE06534,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_mCFA409562E12000D407977266C8AEF134C99D96D,
	task14_Start_m01BA63D8B9C26ACB047CB973287A3DCFAB968A8F,
	task14_stopwatch_m7A1EB79B71C66BC46A7251CCA33C672FAC4E0E3A,
	task14_answer_m76107A10607B307E10B6937892F41816F5E364F9,
	task14_showhint1_mBA71C197F6D96368FA0A4106E652BEF68337A1E8,
	task14_notshowhint1_m943C3524A427E15F40676054452E82D6FD890191,
	task14_showhint2_m2315F70F697DAE2862B1E5241DA434DE1FB16026,
	task14_notshowhint2_mB16F7E4C814D8771256563D27C3666F895561987,
	task14_showhint3_m195D10323A853E5A476EE2A2B0A169EFF1DE9BD9,
	task14_notshowhint3_mBFF62C2C4B7867A7264D15A1C250617F8663B6A5,
	task14_notshowver_m13F63153519845147808E2F5E859428691F3DDF6,
	task14_showstatistics_mFA59E9B9A7680DFFF10712B144B7201D3E93E07C,
	task14_notshowstatistics_m6AA71A73D89E0A0A2A690C14B7DC3FA149B659AE,
	task14__ctor_m4B1A2B0494D1DAEDC28E0626377D64245892F154,
	U3CstopwatchU3Ed__28__ctor_mADF9EB2286862B8704555364FEF28EFEDA4C7131,
	U3CstopwatchU3Ed__28_System_IDisposable_Dispose_mE47CABED9599CAAB2F95B4412B72841BC3ADB12D,
	U3CstopwatchU3Ed__28_MoveNext_m9EAFDC895E5B4C279FC416ED0314E9C0FBC5F697,
	U3CstopwatchU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30D1FD80706BDAC139576BE4056F3953BBA85FD2,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_Reset_mB27C2CB5C3F413507FB1A24EAD9508754AFE788A,
	U3CstopwatchU3Ed__28_System_Collections_IEnumerator_get_Current_m361F1862F46FA07DCF1004AE2E3B02A769B517E6,
	perehodmodul3_Show_Z1_m4D444A82BFEA76843683EE2C22ABB671393B4E7B,
	perehodmodul3_Show_Z2_mEF7A643E2B5392A8B61D3C7B0560D997B6AA40CF,
	perehodmodul3_Show_Z3_m5C90AC0970D2C8B85293CD75FDC41323930DCD5F,
	perehodmodul3_Show_Z4_mEFDEDDB16FBE3E6FD449E80279BA1DF450A23D12,
	perehodmodul3_Show_Module_m22500D27239F7D451D1B4EB707CF7ED993568539,
	perehodmodul3_Show_MainMenu_mED406BD0428EB1B2351D3831B6778A397BDA1C39,
	perehodmodul3_Show_zadmodule_mD5486C2751B18CA9F14284C495341CB9BA019FCF,
	perehodmodul3__ctor_m58A2A8CB16ECF566EE23BAB00FE0D23D9EDC5A40,
	perehodmodul3__cctor_mB240E395DAAC574BA21848EF219A566C3DC79EF8,
	task13_Start_mC0B57EB527A41FC5574F0C7A1D25E8DE9F2BCE79,
	task13_stopwatch_m77B093B433CBAD7202592DDC74C023A2D3D70AC9,
	task13_answer_mDBC8B9CB04D2B93214E59E40BD094CF8DE49F66F,
	task13_showhint1_m7B690B0546FA82150F9CDC0B225663B1A7FF9BDD,
	task13_notshowhint1_mB804858E231D81551F83C686A1AA793222BC9A44,
	task13_notshowver_m252C6FD269B27C7410C6194D9AABF4FA8FEBEBBB,
	task13_showstatistics_m2BA1FB8A6CE907738CE9C378DCA2CDFFFED1F6C7,
	task13_notshowstatistics_mD25ACC5E8377B00CD0651F92F35A24B9760FEDA0,
	task13_nextuslzad_m09545BAF591FF70A7629A8E1900B3A192EBF8130,
	task13_backuslzad_m973994419D9154085DC32856469FDC25897B664F,
	task13__ctor_mEC564BE2E35F72C6AC878B6497A728545619623A,
	U3CstopwatchU3Ed__20__ctor_m5097B750882C8C6F075C006F7B905F6D84E8192E,
	U3CstopwatchU3Ed__20_System_IDisposable_Dispose_mF6E0CFC6F4701037477F2D596E241506C92B58B8,
	U3CstopwatchU3Ed__20_MoveNext_m68163B3E3F1915D214889414AF4D7603F0C2D31B,
	U3CstopwatchU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m491ECAB186ECE85BF8E47174E1A904BB3E7006F5,
	U3CstopwatchU3Ed__20_System_Collections_IEnumerator_Reset_m530955FABF01848A268C5DD8435F802D7CC574DA,
	U3CstopwatchU3Ed__20_System_Collections_IEnumerator_get_Current_mC0EBDF136A2968A7187D73F079B37FA79CF3D726,
	TableUIExample_OnChangeTextValue_m9F3863D4C8B0408AFBFE73567781702C1193CBBF,
	TableUIExample_OnAddNewRowClick_m338F19CC09329ED6FC5D94EB3A4C346E5D6BBB4F,
	TableUIExample_OnAddNewColumnClick_m627102EC50EAB214AEDEA80306E366A0000E58A1,
	TableUIExample_OnRemoveLastColumn_m5A5E4E04EDC9764083B32725CEF002642CB8E18E,
	TableUIExample_OnRemoveLastRow_m70C5718BF00FBF3D58EEAC662897569D159A2536,
	TableUIExample__ctor_m0D49A5BCB167C398D1A3229E726FEAB189326733,
};
static const int32_t s_InvokerIndices[546] = 
{
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	7662,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	7662,
	5147,
	5041,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4001,
	5041,
	4001,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	4001,
	5041,
	4001,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	7662,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	7662,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
	4054,
	5147,
	4969,
	5041,
	5147,
	5041,
	5147,
	5147,
	5147,
	5147,
	5147,
	5147,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	546,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
